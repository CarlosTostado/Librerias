/* 
 * Archivo:     EM3242.c
 * Autor:       Ing. Carlos Tostado
 * Fecha:       18 de noviembre de 2015
 * Empresa:     i + I + D
 * Version:     1.0
 * 
 * Version:     1.1
 *  1.- Se Agrego funcion para lectura de la mediana y su orden ascendente.
 *  2.- se agrego un delay_10us para su uso en PIC16F.
 *  3.- Se agrego funcion para lectura de promedio movil para su uso en MNA
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "em3242.h"
#include "adc.h"
#include "funciones.h"
#include "assert.h"

FILENUM(4);

extern flags_t bandera;
static uint8_t error_encoder;
static uint8_t  Arreglo_ADC[TAMANIO];

//Funcion que acopla la lectura obtenida a un rango de 0 - 255
uint8_t Valor_Digital(void)
{
   //Variables
    uint16_t Lectura = 0;
    uint8_t Valor_A = 0;
    uint16_t relacion = 0;
    
    Lectura = Lectura_Promedio();                                                //Guarda el valor digital promedio
    
    if (Lectura <= 90)                                                          //Verifica que haya encoder conectado
        bandera.estado_errores = ERROR_ENCODER;
    else
        bandera.estado_errores = NO_ERROR;

   if (ERROR_ENCODER != bandera.estado_errores)                                 //Bandera que monitorea un error de encoder b_no_sen = 0 (Error), b_no_sen = 1 (Ok)
   {
        if (Lectura > VOUT_MAX)                                                 //Si la lectura es mayor a .905VDD, se establece un limite al valor
            Lectura = VOUT_MAX;

        if(Lectura != 0)                                                        //Comprueba que el valor es diferente de 0
            Lectura -= VOUT_MIN;                                                //Resta el valor para obtener el valor digital                                           
//            Lectura -= VOUT_MIN2;                                                         
        relacion = (Lectura*5)/16;                                              //Multiplicacion para convcertir la relacion 1023 a 255

        Valor_A = (uint8_t)relacion;                                            //Cast a la variable relacion, para obtener valor angular

        //if (1 == CARATULA)                                                      //Comprueba que caratula es la que sera utilizada para realizar el 
        Valor_A += AJUSTE_CERO;                                             //adjuste a cero correspondiente
    }
    else
        Valor_A = 0;                                                            //En caso de error de encoder valor angular es 0

    return Valor_A;
}

//Función que obtiene la posición del tanque en porcentaje (Carátula INGUSA)
uint8_t Porcentaje_Tanque(uint8_t v_angular)
{
    float Y = 0, Xa = 0, Xb = 0;
    uint8_t Resultado, Ya = 0, Yb = 0, X = 0;

    if (v_angular >=7 && v_angular <= 19)                                      //Interpolación de 0 - 5
        return 5;
    else if (v_angular >= 20 && v_angular <= 48)                                //Interpolación de 5 - 10
    {
        Xa = 20;
        Xb = 48;
        X = v_angular;
        Ya = 5;
        Yb = 10;
    }
    else if (v_angular >= 49 && v_angular <= 64)                                //Interpolación de 10 - 15
    {
        Xa = 49;
        Xb = 64;
        X = v_angular;
        Ya = 10;
        Yb = 15;
    }
    else if (v_angular >= 65 && v_angular <= 75)                                //Interpolación de 15 - 20
    {
        Xa = 65;
        Xb = 75;
        X = v_angular;
        Ya = 15;
        Yb = 20;
    }
    else if (v_angular >= 76 && v_angular <= 82)                                //Interpolación de 20 - 25
    {
        Xa = 76;
        Xb = 82;
        X = v_angular;
        Ya = 20;
        Yb = 25;
    }
    else if (v_angular >= 83 && v_angular <= 94)                                //Interpolación de 25 - 30
    {
        Xa = 83;
        Xb = 94;
        X = v_angular;
        Ya = 25;
        Yb = 30;
    }
    else if (v_angular >= 95 && v_angular <= 103)                               //Interpolación de 30 - 35
    {
        Xa = 95;
        Xb = 103;
        X = v_angular;
        Ya = 30;
        Yb = 35;
    }
    else if (v_angular >= 104 && v_angular <= 112)                              //Interpolación de 35 - 40
    {
        Xa = 104;
        Xb = 112;
        X = v_angular;
        Ya = 35;
        Yb = 40;
    }
    else if (v_angular >= 113 && v_angular <= 119)                              //Interpolación de 40 - 45
    {
        Xa = 113;
        Xb = 119;
        X = v_angular;
        Ya = 40;
        Yb = 45;
    }
    else if (v_angular >= 120 && v_angular <= 128)                              //Interpolación de 45 - 50
    {
        Xa = 120;
        Xb = 128;
        X = v_angular;
        Ya = 45;
        Yb = 50;
    }
    else if (v_angular >= 129 && v_angular <= 135)                              //Interpolación de 50 - 55
    {
        Xa = 129;
        Xb = 135;
        X = v_angular;
        Ya = 50;
        Yb = 55;
    }
    else if (v_angular >= 136 && v_angular <= 143)                              //Interpolación de 55 - 60
    {
        Xa = 136;
        Xb = 143;
        X = v_angular;
        Ya = 55;
        Yb = 60;
    }
    else if (v_angular >= 144 && v_angular <= 151)                              //Interpolación de 60 - 65
    {
        Xa = 144;
        Xb = 151;
        X = v_angular;
        Ya = 60;
        Yb = 65;
    }
    else if (v_angular >= 152 && v_angular <= 160)                              //Interpolación de 65 - 70
    {
        Xa = 152;
        Xb = 160;
        X = v_angular;
        Ya = 65;
        Yb = 70;
    }
    else if (v_angular >= 161 && v_angular <= 169)                              //Interpolación de 70 - 75
    {
        Xa = 161;
        Xb = 169;
        X = v_angular;
        Ya = 70;
        Yb = 75;
    }
    else if (v_angular >= 170 && v_angular <= 178)                              //Interpolación de 75 - 80
    {
        Xa = 170;
        Xb = 178;
        X = v_angular;
        Ya = 75;
        Yb = 80;
    }
    else if (v_angular >= 179 && v_angular <= 192)                              //Interpolación de 80 - 85
    {
        Xa = 179;
        Xb = 192;
        X = v_angular;
        Ya = 80;
        Yb = 85;
    }
    else if (v_angular >= 193 && v_angular <= 208)                              //Interpolación de 85 - 90
    {
        Xa = 193;
        Xb = 208;
        X = v_angular;
        Ya = 85;
        Yb = 90;
    }
    else if (v_angular >= 209 && v_angular <= 235)                              //Interpolación de 90 - 95
    {
        Xa = 209;
        Xb = 235;
        X = v_angular;
        Ya = 90;
        Yb = 95;
    }
    else if (v_angular >= 236 && v_angular <= 254)                              //Interpolación de 95 - 99
        return 95;
    else
        return 0;

    Y = (Ya)+((X - Xa)*((Yb - Ya) / (Xb - Xa)));                                //Realiza interpolación
    Y += 0.5;                                                                   //Redondeo
    Resultado = (uint8_t) Y;                                                    //Obtiene el resultado en enteros

    return Resultado;
}

//Funci?n que obtiene la posici?n del tanque en porcentaje (Car?tula INGUSA)
uint8_t Por_Tanq_Ingusa_5(uint8_t v_angular)
{
    float Y = 0, Xa = 0, Xb = 0;
    uint8_t Resultado, Ya = 0, Yb = 0, X = 0;
    
    if(v_angular <= AJUSTE_CERO || v_angular >= 250)                            //Condici?n para asegurar cero cuando se desborda valor degital de 0 a 255 (hasta 240)
        return 0;
    
    else if (v_angular >= 6 && v_angular <= 19)                                 //Interpolaci?n de  5
    {
        Xa = 6;
        Xb = 19;
        X = v_angular;
        Ya = 1;
        Yb = 5;
    }        
    
    else if (v_angular >= 20 && v_angular <= 48)                                //Interpolaci?n de 5 - 10
    {
        Xa = 20;
        Xb = 48;
        X = v_angular;
        Ya = 5;
        Yb = 10;
    }
    else if (v_angular >= 49 && v_angular <= 64)                                //Interpolaci?n de 10 - 15
    {
        Xa = 49;
        Xb = 64;
        X = v_angular;
        Ya = 10;
        Yb = 15;
    }
    else if (v_angular >= 65 && v_angular <= 77)                                //Interpolaci?n de 15 - 20
    {
        Xa = 65;
        Xb = 77;
        X = v_angular;
        Ya = 15;
        Yb = 20;
    }
    else if (v_angular >= 78 && v_angular <= 85)                                //Interpolaci?n de 20 - 25
    {
        Xa = 78;
        Xb = 85;
        X = v_angular;
        Ya = 20;
        Yb = 25;
    }
    else if (v_angular >= 86 && v_angular <= 95)                                //Interpolaci?n de 25 - 30
    {
        Xa = 86;
        Xb = 95;
        X = v_angular;
        Ya = 25;
        Yb = 30;
    }
    else if (v_angular >= 96 && v_angular <= 103)                               //Interpolaci?n de 30 - 35
    {
        Xa = 96;
        Xb = 103;
        X = v_angular;
        Ya = 30;
        Yb = 35;
    }
    else if (v_angular >= 104 && v_angular <= 112)                              //Interpolaci?n de 35 - 40
    {
        Xa = 104;
        Xb = 112;
        X = v_angular;
        Ya = 35;
        Yb = 40;
    }
    else if (v_angular >= 113 && v_angular <= 119)                              //Interpolaci?n de 40 - 45
    {
        Xa = 113;
        Xb = 119;
        X = v_angular;
        Ya = 40;
        Yb = 45;
    }
    else if (v_angular >= 120 && v_angular <= 128)                              //Interpolaci?n de 45 - 50
    {
        Xa = 120;
        Xb = 128;
        X = v_angular;
        Ya = 45;
        Yb = 50;
    }
    else if (v_angular >= 129 && v_angular <= 135)                              //Interpolaci?n de 50 - 55
    {
        Xa = 129;
        Xb = 135;
        X = v_angular;
        Ya = 50;
        Yb = 55;
    }
    else if (v_angular >= 136 && v_angular <= 143)                              //Interpolaci?n de 55 - 60
    {
        Xa = 136;
        Xb = 143;
        X = v_angular;
        Ya = 55;
        Yb = 60;
    }
    else if (v_angular >= 144 && v_angular <= 151)                              //Interpolaci?n de 60 - 65
    {
        Xa = 144;
        Xb = 151;
        X = v_angular;
        Ya = 60;
        Yb = 65;
    }
    else if (v_angular >= 152 && v_angular <= 160)                              //Interpolaci?n de 65 - 70
    {
        Xa = 152;
        Xb = 160;
        X = v_angular;
        Ya = 65;
        Yb = 70;
    }
    else if (v_angular >= 161 && v_angular <= 169)                              //Interpolaci?n de 70 - 75
    {
        Xa = 161;
        Xb = 169;
        X = v_angular;
        Ya = 70;
        Yb = 75;
    }
    else if (v_angular >= 170 && v_angular <= 178)                              //Interpolaci?n de 75 - 80
    {
        Xa = 170;
        Xb = 178;
        X = v_angular;
        Ya = 75;
        Yb = 80;
    }
    else if (v_angular >= 179 && v_angular <= 192)                              //Interpolaci?n de 80 - 85
    {
        Xa = 179;
        Xb = 192;
        X = v_angular;
        Ya = 80;
        Yb = 85;
    }
    else if (v_angular >= 193 && v_angular <= 208)                              //Interpolaci?n de 85 - 90
    {
        Xa = 193;
        Xb = 208;
        X = v_angular;
        Ya = 85;
        Yb = 90;
    }
    else if (v_angular >= 209 && v_angular <= 235)                              //Interpolaci?n de 90 - 95
    {
        Xa = 209;
        Xb = 235;
        X = v_angular;
        Ya = 90;
        Yb = 95;
    }
    else if (v_angular >= 236 && v_angular <= 249)                              //Interpolaci?n de 95 - 99
        return 95;

    Y = (Ya)+((X - Xa)*((Yb - Ya) / (Xb - Xa)));                                //Realiza interpolaci?n
    Y += 0.5;                                                                   //Redondeo
    Resultado = (uint8_t) Y;                                                    //Obtiene el resultado en enteros

    return Resultado;
}

//Función que obtiene la posición del tanque en porcentaje (Carátula ROCHESTER 10)
uint8_t Por_Tanq_Rochester_10(uint8_t v_angular)
{
	float y = 0, xa = 0, xb = 0;
    uint8_t resultado, ya = 0, yb = 0, x = 0;

    if (v_angular >= 1 && v_angular <= 25)                                     //Interpolación de 0 - 10
        return 10;
    else if (v_angular >= 26 && v_angular <= 48)                                //Interpolación de 10 - 15
    {
        xa = 26;
        xb = 48;
        x = v_angular;
        ya = 10;
        yb = 15;
    }
    else if (v_angular >= 49 && v_angular <= 64)                                //Interpolación de 15 - 20
    {
        xa = 49;
        xb = 64;
        x = v_angular;
        ya = 15;
        yb = 20;
    }
    else if (v_angular >= 65 && v_angular <= 77)                               //Interpolación de 20 - 25
    {
        xa = 65;
        xb = 77;
        x = v_angular;
        ya = 20;
        yb = 25;
    }
    else if (v_angular >= 78 && v_angular <= 88)                              //Interpolación de 25 - 30
    {
        xa = 78;
        xb = 88;
        x = v_angular;
        ya = 25;
        yb = 30;
    }
    else if (v_angular >= 89 && v_angular <= 98)                              //Interpolación de 30 - 35
    {
        xa = 89;
        xb = 98;
        x = v_angular;
        ya = 30;
        yb = 35;
    }
    else if (v_angular >= 99 && v_angular <= 108)                              //Interpolación de 35 - 40
    {
        xa = 99;
        xb = 108;
        x = v_angular;
        ya = 35;
        yb = 40;
    }
    else if (v_angular >= 109 && v_angular <= 118)                              //Interpolación de 40 - 45
    {
        xa = 109;
        xb = 118;
        x = v_angular;
        ya = 40;
        yb = 45;
    }
    else if (v_angular >= 119 && v_angular <= 128)                              //Interpolación de 45 - 50
    {
        xa = 119;
        xb = 128;
        x = v_angular;
        ya = 45;
        yb = 50;
    }    
    else if (v_angular >= 129 && v_angular <= 136)                              //Interpolación de 50 - 55
    {
        xa = 129;
        xb = 136;
        x = v_angular;
        ya = 50;
        yb = 55;
    }    
    else if (v_angular >= 137 && v_angular <= 147)                              //Interpolación de 55 - 60
    {
        xa = 137;
        xb = 147;
        x = v_angular;
        ya = 55;
        yb = 60;
    }    
    else if (v_angular >= 148 && v_angular <= 156)                              //Interpolación de 60 - 65
    {
        xa = 148;
        xb = 156;
        x = v_angular;
        ya = 60;
        yb = 65;
    }    
    else if (v_angular >= 157 && v_angular <= 167)                              //Interpolación de 35 - 40
    {
        xa = 157;
        xb = 167;
        x = v_angular;
        ya = 65;
        yb = 70;
    }    
    else if (v_angular >= 168 && v_angular <= 179)                              //Interpolación de 70 - 75
    {
        xa = 168;
        xb = 179;
        x = v_angular;
        ya = 70;
        yb = 75;
    }    
    else if (v_angular >= 180 && v_angular <= 191)                              //Interpolación de 75 - 80
    {
        xa = 180;
        xb = 191;
        x = v_angular;
        ya = 75;
        yb = 80;
    }    
    else if (v_angular >= 192 && v_angular <= 207)                              //Interpolación de 80 - 85
    {
        xa = 192;
        xb = 207;
        x = v_angular;
        ya = 80;
        yb = 85;
    }
    else if (v_angular >= 208 && v_angular <= 226)                              //Interpolación de 85 - 90
    {
        xa = 208;
        xb = 226;
        x = v_angular;
        ya = 85;
        yb = 90;
    }
    else if (v_angular >= 227 && v_angular <= 254)                              //Valor de la caratula fuera de rango
        return 90;
    else 
        return 0;

    y = (ya)+((x - xa)*((yb - ya) / (xb - xa)));                                //Realiza interpolación
    y += 0.5;                                                                   //Redondeo
    resultado = (uint8_t) y;                                              //Obtiene el resultado en enteros

    return resultado;
}

//Funcion que obtine un promedio de lecturas
static uint16_t Lectura_Promedio(void)
{
    uint8_t i = 0;
    uint32_t Prom = 0;
    
    i = 0;
    do
    {
        Prom = adc_read() + Prom;
//        Prom = Lectura_Mediana() + Prom;
        i++;
    }while(i <= (TAMANIO_PROM - 1));

    return (uint16_t) (Prom / TAMANIO_PROM);

}

#if 0

//Funcio que devuelce el valor de un promedio movil
static unsigned int Lectura_Promedio_Movil(void)
{
    unsigned char i = 0;
    unsigned int Prom = 0;
    unsigned int Promedio = 0;
    static unsigned char m = 0;
    static unsigned char b_vuelta = 0;
    static unsigned int Muestras[No_MUESTRAS];
    
    
//    Muestras[m] = Leer_EM3242();                                                //Lee el valor angular del encoder
//    Muestras[m] = Lectura_Promedio();                                                 //Lee el valor angular del encoder
    Muestras[m] = Lectura_Mediana();                                                //Lee el valor angular del encoder

    if (!b_no_sen)                                                              //Si hay encoder procede
    {
        if (!b_vuelta)                                                          //Verifica que sea la primera vuelta
        {
            m++;                                                                //Llena el arreglo la primera vez
            Prom = 0;                                                           //Mandamos un 0 mientras se llena el arreglo

            if (m == No_MUESTRAS)                                               //Mientras no se haya llenado el arraglo no hacemos promedio
            {
                for (i = 0; i < No_MUESTRAS; i++)                                    //Obtiene las muestras
                    Prom += Muestras[i];                                         //Acumula mas muestras
                b_vuelta = 1;                                                   //Levanta bandera de primer vuelta
                m = 0;                                                          //Reiniciliza el indice
            }
        }
        else
        {
            m++;                                                                //incrementa el indice
            for (i = 0; i < No_MUESTRAS; i++)                                        //Obtiene las muestras
                Prom += Muestras[i];
            if (m == No_MUESTRAS)                                               //Si se desborda el indice lo reinicialiaza
                m = 0;                                                          //Reinicia el índice
        }
    }
    else
    {
        Prom = 0;
        m = 0;
        b_vuelta = 0;
        
        for(i = 0; i < No_MUESTRAS; i++)
            Muestras[i] = 0;
        
        return 0;
    }
                                                              
    if(!b_vuelta)
        Promedio = Muestras[8];
    else
        Promedio = Prom / No_MUESTRAS;                                              //Obtiene el promedio de las muestras

    return Promedio;
}
#endif
//Funcion para obtener la mediana de un conjunto de muestras
static unsigned int Lectura_Mediana(void)
{
    unsigned char k = 0;
    
    for (k = 0; k < TAMANIO; k++) {                                              
        Arreglo_ADC[k] = adc_read();                                         //LLenamos el arreglo para la media con lecturas de ADC
    } 
    
    Ordena_Ascendente();                                                        //Funcion para ordenar de manera ascendente los datos
    
    return Arreglo_ADC[(TAMANIO-1)/2];                                          //Retorna la muestra que se encuentra a la mitad de la mediana
    
}

//Funcion que ordena de manera ascencente un conjunto de datos
static void Ordena_Ascendente(void)
{
    unsigned char pasadas = 0;
    unsigned char J = 0;
    unsigned int Auxiliar = 0;
    
    //Ordamiento de la burbuja
    //Ciclo con el que manejas el numero de pasos
    for (pasadas = 1; pasadas < TAMANIO; pasadas++)
    {
        //Ciclo con el que comparas el numero de comparaciones por pasadas 
        for (J = 0; J < TAMANIO - 1; J++)
        {
            //Compara los elementos adyacentes y los intercambia si el primer
            //elemento es mayor que el segundo
            if (Arreglo_ADC[J] > Arreglo_ADC[J+1])
            {
                Auxiliar = Arreglo_ADC[J];
                Arreglo_ADC[J] = Arreglo_ADC[J+1];
                Arreglo_ADC[J+1] = Auxiliar;
            }
        }
    }
}



