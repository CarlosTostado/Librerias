/* 
 * File:   EM3242.h
 * Author: Desarrollo
 *
 * Created on 18 de noviembre de 2015, 12:31 PM
 */

#ifndef EM3242_H
#define	EM3242_H

/*******************************HEADER FILES***********************************/

#include <xc.h>
#include <stdint.h>
#include "adc.h"
#include "timers.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************DEFINES***************************************/

#define VOUT_MIN             106                                                //Lectura minima para el sensor EM3242
#define VOUT_MIN2             37                                                //Lectura minima para el sensor EM3242
#define VOUT_MAX             925                                                //Lectura maxima para el sensor EM3242
#define AJUSTE_CERO          5                                                  //Ajuste a cero para caratula Ingusa 5 - 95%    
#define No_MUESTRAS          30                                                 //Mestras para el promedio movil 
#define No_MUESTRAS_1RA      6                                                  //N?mero de muestras para promediar en primera vuelta
#define TAMANIO              21                                                 //Siempre debe ser IMPAR
#define TAMANIO_PROM         20 
    
/******************************STRUCTS/ENUMS***********************************/

/* NA */

/*******************************PROTOTYPES*************************************/

//Funciones publicaa
uint8_t Valor_Digital(void);
uint8_t Porcentaje_Tanque(uint8_t v_angular);
uint8_t Por_Tanq_Ingusa_5(uint8_t v_angular);
uint8_t Por_Tanq_Rochester_10(uint8_t v_angular);

//Funciones locales
static  unsigned int Leer_EM3242(void);
static uint16_t Lectura_Promedio(void);
static void  Ordena_Ascendente(void);
static uint16_t  Lectura_Mediana(void);
static uint16_t  Promedio_Movil(void);

#ifdef __cplusplus
}
#endif

#endif	/* EM3242_H */

