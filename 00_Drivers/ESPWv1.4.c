/*******************************************************************************
* File:     ESPW.c
* Author:   Roberto D�vora Ochoa
* Date:     10/01/2018
* Versi�n:  1.3 
*******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "ESPWv1.4.h"
#include "eusart.h"

#define _XTAL_FREQ 32000000

//Handlers para UART
static void (*ptrWriteUART)(const uint8_t *);
static uint8_t (*ptrReadUART)(void);
static uint8_t *ptrRegistro;
static uint8_t EnDisInterruptMask;
static uint8_t *ptrSleep;
static uint8_t sleepMask;

//Buffers
static uint8_t ESPW_BufferTX_Commands[SIZE_BUFFER_TX_COMMAND] = {0};            //Buffer de Transmisi�n de comandos
static uint8_t ESPW_BufferRX_Commands[SIZE_BUFFER_RX_COMMAND] = {0};            //Buffer de recepci�n de comandos
static uint8_t ESPW_BufferTX_Data[SIZE_BUFFER_TX_DATA] = {0};                   //Buffer para transmisi�n de datos
static uint8_t ESPW_BufferRX_Data[SIZE_BUFFER_RX_DATA] = {0};                   //Buffer de recepci�n de datos
static uint8_t lenght[4] = {0};                                                 //Buffer para enviar la longitud del msg al m�dulo
#if SSID_PWD_FIXED
static uint8_t _SSID[15] = {0};
static uint8_t _PASSWD[15] = {0};
#endif

//Variables de estados
static volatile estados_ESPW_t ESPW_state = ESPW_IDLE;
static estados_ESPW_t ESPW_oldState = ESPW_IDLE;

//Variables para enviar y recibir via UART
static volatile uint16_t commandHead = 0;                                        //Indice para escribir en bufferRX de comandos
static volatile uint16_t commandTail = 0;
static volatile int8_t commandBytes = SIZE_BUFFER_RX_COMMAND;
static volatile uint8_t rcvDatabytes = 0;
static uint16_t dataHead = 0;                                                    //Indice para escribir en bufferTX de datos
static uint16_t dataTail = 0;
static uint8_t dataTailTemp = 0;
static int8_t dataBytesTemp = 0;
static int8_t dataBytes = SIZE_BUFFER_TX_COMMAND;
static uint32_t timeout = 0;                                                    //Variable de timeout
static volatile int8_t rcvLenght = 0;                                           //Longitud en bytes de datos recibidos
static volatile int8_t tempRcvLenght = 0;
static uint8_t msgLenght[NUMERO_DE_ENVIOS_EN_COLA][2] = {{0},{0}};              //Longitud en bytes de datos a enviar e ID de env�o
static uint8_t msgLenghtHead = 0;
static uint8_t msgLenghtTail = 0;
static volatile uint8_t indexUnits = 0;
static volatile uint8_t indexTens = 0;
static volatile uint8_t indexHundreds = 0;
static uint8_t intentos = 0;

//Banderas
static volatile ESPW_flags_t Banderas = {0};
static volatile uint8_t lineReceived = 0;                                       //Contador de l�neas recibidas

//TODO 1 ESPW_v1.3: Registrar las funcionen de lectura y escritura para UART
void ESPW_UART_Handler(void (*ptrWriteFunction)(const uint8_t), uint8_t (*ptrReadFunction)(void)/*, uint8_t *RegistroRX, uint8_t BitRX*/)
{
    ptrWriteUART = ptrWriteFunction;
    ptrReadUART = ptrReadFunction; 
//    ptrRegistro = RegistroRX;
//    EnDisInterruptMask = BitRX;
    
    return;
}

//TODO 1 ESPW_v1.3: Registrar el puerto y bit que se conectar� al pin Enable del m�dulo ESP
void ESPW_sleepBitRegister (uint8_t *Puerto, uint8_t Bit)
{
    ptrSleep = Puerto;
    sleepMask = Bit;
    
    *Puerto |= sleepMask;
    
    return;
}

void ESPW_setUART_Speed(uint8_t *baudrate)
{    
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+UART_DEF=");
    ESPW_concatenar(ESPW_BufferTX_Commands,baudrate,SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,",8,1,0,0\r\n",SIZE_BUFFER_TX_COMMAND);
    ESPW_WriteString(ESPW_BufferTX_Commands);
    
    ESPW_waitFor(OK);
    return;
}

//TODO 5 (Opcional) ESPWv1.3: Cambiar la MAC Address del m�dulo
void ESPW_setMAC_Address(uint8_t *MAC)
{
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPSTAMAC=\"");
    ESPW_concatenar(ESPW_BufferTX_Commands,MAC,SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,"\"\r\n",SIZE_BUFFER_TX_COMMAND);    
    ESPW_WriteString(ESPW_BufferTX_Commands);
    
    ESPW_waitFor(OK);
    
    return;
}

//TODO 5 (Solo Medidores de Nivel de tanque estacionario) ESPWv1.3: Registrar el n�mero de medidor (MAC) para el medidor
void ESPW_setNoMedidor(uint32_t No_medidor)
{     
    if (No_medidor<=0xFFFFFF)
    {
        uint8_t auxiliar[9]={0};
        
        ultoa(auxiliar,No_medidor,16);

        if(No_medidor>0xFFFFF)
        {
            auxiliar[7]=auxiliar[5];
            auxiliar[6]=auxiliar[4];
            auxiliar[5]=':';
            auxiliar[4]=auxiliar[3];
            auxiliar[3]=auxiliar[2];
            auxiliar[2]=':';
        }
        else if ((No_medidor<=0xFFFFF)&&(No_medidor>0xFFFF))
        {
            auxiliar[7]=auxiliar[4];
            auxiliar[6]=auxiliar[3];
            auxiliar[5]=':';
            auxiliar[4]=auxiliar[2];
            auxiliar[3]=auxiliar[1];
            auxiliar[2]=':';
            auxiliar[1]=auxiliar[0];
            auxiliar[0]='0';
        }
        else if ((No_medidor<=0xFFFF)&&(No_medidor>0xFFF))
        {
            auxiliar[7]=auxiliar[3];
            auxiliar[6]=auxiliar[2];
            auxiliar[5]=':';
            auxiliar[4]=auxiliar[1];
            auxiliar[3]=auxiliar[0];
            auxiliar[2]=':';
            auxiliar[1]='0';
            auxiliar[0]='0';
        }
        else if ((No_medidor<=0xFFF)&&(No_medidor>0xFF))
        {
            auxiliar[7]=auxiliar[2];
            auxiliar[6]=auxiliar[1];
            auxiliar[5]=':';
            auxiliar[4]=auxiliar[0];
            auxiliar[3]='0';
            auxiliar[2]=':';
            auxiliar[1]='0';
            auxiliar[0]='0';
        }
        else if ((No_medidor<=0xFF)&&(No_medidor>0xF))
        {
            auxiliar[7]=auxiliar[1];
            auxiliar[6]=auxiliar[0];
            auxiliar[5]=':';
            auxiliar[4]='0';
            auxiliar[3]='0';
            auxiliar[2]=':';
            auxiliar[1]='0';
            auxiliar[0]='0';
        }
        else        
        {
            auxiliar[7]=auxiliar[0];
            auxiliar[6]='0';
            auxiliar[5]=':';
            auxiliar[4]='0';
            auxiliar[3]='0';
            auxiliar[2]=':';
            auxiliar[1]='0';
            auxiliar[0]='0';
        }

    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPSTAMAC=\"");
    ESPW_concatenar(ESPW_BufferTX_Commands,"54:4A:4D:",SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,auxiliar,33);
    ESPW_concatenar(ESPW_BufferTX_Commands,"\"\r\n",SIZE_BUFFER_TX_COMMAND);
    ESPW_WriteString(ESPW_BufferTX_Commands);
    
    ESPW_waitFor(OK);    
    }
    return;
}

//Funci�n que reinicia el m�dulo para llevarlo a un estado inicial
static void ESPW_Reset (void)
{       
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+RST\r\n");
    ESPW_WriteString(ESPW_BufferTX_Commands);      
    
    ESPW_waitFor(READY);                                                        //Espera a recibir un "ready"
    ESPW_waitFor(CONECTADO_A_RED_WIFI);                                         //Espera a recibir un "WIFI CONNECT"
    if(ESPW_waitFor(IP_OBTENIDA))                                               //Espera a recibir un "WIFI GOT IP"
    {
        Banderas.networkStatus = 1;
    }
    else
    {
        Banderas.networkStatus = 0;
    }
    
    return;
}

void ESPW_Restore (void)
{       
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+RESTORE\r\n");
    ESPW_WriteString(ESPW_BufferTX_Commands);      
    
    ESPW_waitFor(READY);                                                        //Espera a recibir un "ready"
    ESPW_waitFor(CONECTADO_A_RED_WIFI);                                         //Espera a recibir un "WIFI CONNECT"
    if(ESPW_waitFor(IP_OBTENIDA))                                               //Espera a recibir un "WIFI GOT IP"
    {
        Banderas.networkStatus = 1;
    }
    else
    {
        Banderas.networkStatus = 0;
    }
    
    return;
}

// Funci�n que comprueba que el m�dulo responde correctamente
static void ESPW_test(void)
{        
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT\r\n");
    ESPW_WriteString(ESPW_BufferTX_Commands);
    ESPW_waitFor(OK);           
    
    return;
}

#if SSID_PWD_FIXED
// TODO 3 ESPW_v1.3: Configurar el funcionamiento del m�dulo
void ESPW_wifiInit(uint8_t WiFiConfig, uint8_t *SSID, uint8_t *PWD, uint8_t cliente_servidor)
{    
    __delay_ms(200);
    ESPW_test();
    __delay_ms(BUSY_TIME);
    ESPW_Reset();  
    __delay_ms(BUSY_TIME);
    ESPW_WiFiConfig(WiFiConfig);           
    __delay_ms(BUSY_TIME);
    ESPW_ClientServerMode(cliente_servidor);               
    
    if((WiFiConfig != ACCESSPOINT_MODE)&& (Banderas.networkStatus != 1))
    {        
        ESPW_ConectarARedWiFi(SSID,PWD);
    }    
    return;
}
#else
// TODO 3 ESPW_v1.3: Configurar el funcionamiento del m�dulo
void ESPW_wifiInit(uint8_t WiFiConfig, uint8_t cliente_servidor)
{    
    __delay_ms(200);
    ESPW_test();
    __delay_ms(BUSY_TIME);
    ESPW_Reset();  
    __delay_ms(BUSY_TIME);
    ESPW_WiFiConfig(WiFiConfig);                     
    __delay_ms(BUSY_TIME);
    ESPW_ClientServerMode(cliente_servidor);             
    __delay_ms(BUSY_TIME);        
    
    return;
}
#endif

// Funci�n que configura el m�dulo como cliente o servidor
static void ESPW_ClientServerMode (uint8_t config)
{
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    if(config == SERVER_MODE)
    {
        Banderas.MultipleConections = 1;
        ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPMUX=1\r\n");
    }
    if(config == CLIENT_MODE)
    {
        Banderas.MultipleConections = 0;
        ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPMUX=0\r\n");
    }    
    
    if (Banderas.taskOn == 0)
    {
        ESPW_WriteString(ESPW_BufferTX_Commands);
        ESPW_waitFor(OK);
    }
    else
    {
        ESPW_setState(ESPW_TRANSMISION);
    }
    
    return;
}

//Funci�n que configura al m�dulo para conectarse a una red WiFi o emitir una red WiFi
void ESPW_WiFiConfig(uint8_t config)
{    
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    switch(config)
    {
        case STA_MODE:
            ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CWMODE=1\r\n");
            break;
            
        case ACCESSPOINT_MODE:
            ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CWMODE=2\r\n");
            ESPW_WriteString(ESPW_BufferTX_Commands);    
            ESPW_waitFor(OK);
            ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPAP=\"192.168.137.1\",\"192.168.137.1\",\"255.255.255.0\"\r\n");
            break;
            
        case DUAL_MODE:
            ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CWMODE=3\r\n");
            ESPW_WriteString(ESPW_BufferTX_Commands);    
            ESPW_waitFor(OK);
            ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPAP=\"192.168.137.1\",\"192.168.137.1\",\"255.255.255.0\"\r\n");
            break;
        default:
            ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CWMODE=1\r\n");         //Por default en modo estacionario
            break;
    }
    if(Banderas.taskOn == 0)
    {
        ESPW_WriteString(ESPW_BufferTX_Commands);    
        ESPW_waitFor(OK);        
    }
    else
    {
        ESPW_setState(ESPW_TRANSMISION);
    }
    
    return;
}

//TODO 4(opcional) ESPW_v1.3: Configurar los parametros de la red (No visible) que emitir� el m�dulo
void ESPW_configAccessPoint(uint8_t *SSID, uint8_t *PWD, uint8_t *channel, uint8_t EncryptionMethod)
{
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CWSAP=\"");
    ESPW_concatenar(ESPW_BufferTX_Commands,SSID,SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,"\",\"",SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,PWD,SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,"\",",SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,channel,SIZE_BUFFER_TX_COMMAND);   
    switch(EncryptionMethod)
    {
        case OPEN:
            ESPW_concatenar(ESPW_BufferTX_Commands,",0,2,0\r\n",SIZE_BUFFER_TX_COMMAND);
            break;
            
        case WPA_PSK:
            ESPW_concatenar(ESPW_BufferTX_Commands,",2,2,0\r\n",SIZE_BUFFER_TX_COMMAND);
            break;
            
        case WPA2_PSK:
            ESPW_concatenar(ESPW_BufferTX_Commands,",3,2,0\r\n",SIZE_BUFFER_TX_COMMAND);
            break;
            
        case WPA_WPA2_PSK:
            ESPW_concatenar(ESPW_BufferTX_Commands,",4,2,0\r\n",SIZE_BUFFER_TX_COMMAND);
            break;
            
        default:
            ESPW_concatenar(ESPW_BufferTX_Commands,",4,2,0\r\n",SIZE_BUFFER_TX_COMMAND);
            break;
    }
    
    if(Banderas.taskOn == 0)
    {
        ESPW_WriteString(ESPW_BufferTX_Commands);
        ESPW_waitFor(OK);
    }
    else
    {
        ESPW_setState(ESPW_TRANSMISION);
    }
    
    Banderas.taskOn = 1;
    
    return;    
}

#if SSID_PWD_FIXED
//Funci�n que conecta el m�dulo a una red WiFi
void ESPW_ConectarARedWiFi(uint8_t *SSID,uint8_t *PSWD)
{    
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CWJAP=\"");
    ESPW_concatenar(ESPW_BufferTX_Commands,SSID,SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,"\",\"",SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,PSWD,SIZE_BUFFER_TX_COMMAND);
    ESPW_concatenar(ESPW_BufferTX_Commands,"\"\r\n",SIZE_BUFFER_TX_COMMAND);
    ESPW_state = ESPW_TRANSMISION;             
}
#endif

void ESPW_EstablishTCPConection(uint8_t *NoConection, uint8_t *IP, uint8_t *Port)
{
    if(Banderas.networkStatus == 1)
    {
        ESPW_clearBuffer(ESPW_BufferTX_Commands);
        ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPSTART=");
        if(Banderas.MultipleConections == 1)
        {
            ESPW_concatenar(ESPW_BufferTX_Commands,NoConection,SIZE_BUFFER_TX_COMMAND);
            ESPW_concatenar(ESPW_BufferTX_Commands,",",SIZE_BUFFER_TX_COMMAND);
        }
        ESPW_concatenar(ESPW_BufferTX_Commands,"\"TCP\",\"",SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,IP,SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,"\",",SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,Port,SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,"\r\n",SIZE_BUFFER_TX_COMMAND);        
        ESPW_setState(ESPW_TRANSMISION);
    }
    
    return;
}

void ESPW_EstablishUDPConection(uint8_t *NoConection,uint8_t *IP, uint8_t *SendPort, uint8_t *RecvPort)
{
    if(Banderas.networkStatus == 1)
    {                    
        ESPW_clearBuffer(ESPW_BufferTX_Commands);
        ESPW_putToBuffer(ESPW_BufferTX_Commands,"AT+CIPSTART=");
        if(Banderas.MultipleConections == 1)
        {
            ESPW_concatenar(ESPW_BufferTX_Commands,NoConection,SIZE_BUFFER_TX_COMMAND);
            ESPW_concatenar(ESPW_BufferTX_Commands,",",SIZE_BUFFER_TX_COMMAND);
        }
        ESPW_concatenar(ESPW_BufferTX_Commands,"\"UDP\",\"",SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,IP,SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,"\",",SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,SendPort,SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,",",SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,RecvPort,SIZE_BUFFER_TX_COMMAND);
        ESPW_concatenar(ESPW_BufferTX_Commands,",0\r\n",SIZE_BUFFER_TX_COMMAND);
        ESPW_setState(ESPW_TRANSMISION);
    }    
    
    return;
}

//Funci�n que analiza la respuesta del m�dulo ESP32 (debe incluirse en la interrupci�n de recepci�n)
void ESPW_ReadAnswer (void)
{       
    volatile uint8_t Aux = 0;
    Aux = (*ptrReadUART)();
    
    if((Aux == ':')&&Banderas.rcvingData==0)
    {        
        // <editor-fold defaultstate="collapsed" desc="Obtenci�n de los �ndices">
        indexUnits = (int8_t) commandHead - 1;
        indexTens = (int8_t) commandHead - 2;
        indexHundreds = (int8_t) commandHead - 3;

        if (indexUnits < 0) {
            indexUnits += SIZE_BUFFER_RX_COMMAND;
        }
        if (indexTens < 0) {
            indexTens += SIZE_BUFFER_RX_COMMAND;
        }
        if (indexHundreds < 0) {
            indexHundreds += SIZE_BUFFER_RX_COMMAND;
        }
        // </editor-fold>
        
        if((UNIDADES>48)&&(UNIDADES<58)&&(CENTENAS!='"'))
        {
            ESPW_clearBuffer(ESPW_BufferRX_Data);
            rcvLenght = UNIDADES - 48;
            if ((DECENAS >48) && (DECENAS < 58)) 
            {
                rcvLenght += (DECENAS - 48)*10;
            }
            if ((CENTENAS >48) && (CENTENAS < 58) && (DECENAS != ',')) 
            {
                rcvLenght += (CENTENAS - 48)*100;
            }                                        
            tempRcvLenght = rcvLenght;
            rcvDatabytes = 0;
            Banderas.rcvingData = 1;
            commandHead++;                   
            commandTail = commandHead;
            commandBytes = SIZE_BUFFER_RX_COMMAND;
        }
    }
    else if (Banderas.rcvingData)
    {        
        ESPW_BufferRX_Data[rcvDatabytes++] = Aux;        
        tempRcvLenght--;
        if(tempRcvLenght <= 0)
        {                        
            Banderas.rcvingData = 0;
            Banderas.gotDataFlag = 1; 
            Nop();
        }
    }
    else
    {
        if(Aux != '\n') 
        {            
            ESPW_BufferRX_Commands[commandHead++] = Aux;         
            if(commandHead>=SIZE_BUFFER_RX_COMMAND)
            {
                commandHead = 0;
            }
            commandBytes--;
        }
        else
        {                                        
            lineReceived++;               
        }
    }
    return;
}

void ESPW_goToSleep(void)
{
    Banderas.connectionStatus = 0;
    Banderas.networkStatus = 0;
    Banderas.smartconfig = 0;
    
    commandHead = 0;
    commandTail = 0;
    
    // Limpeza de buffers
    ESPW_clearBuffer(ESPW_BufferRX_Commands);
    ESPW_clearBuffer(ESPW_BufferTX_Commands);
    
    __delay_ms(700);
    
    ESPW_setState(ESPW_IDLE);
    *ptrSleep &= (~sleepMask);
    
    return;
}

void ESPW_wakeUp(void)
{
    *ptrSleep |= sleepMask;
    ESPW_state = ESPW_RESPUESTA;
    
    return;
}

//Funci�n que establece la longitud en bytes del mensaje a enviar
void ESPW_setMsgLenght(uint8_t len, uint8_t ID)
{    
    if(Banderas.MultipleConections == 0)
    {
        msgLenght[msgLenghtHead++][0] = len;
    }
    else
    {
        msgLenght[msgLenghtHead][0] = len;
        msgLenght[msgLenghtHead++][1] = ID;
    }
    if(msgLenghtHead>=NUMERO_DE_ENVIOS_EN_COLA)
    {
        msgLenghtHead=0;
    }
    Banderas.envioPendiente++;
    
    return;
}

//Funci�n que establece el estado actual de la tarea ESPW_task()
void ESPW_setState(uint8_t state)
{
//#if !SSID_PWD_FIXED
//    if((ESPW_oldState != ESPW_STARTSMART)&&(ESPW_oldState != ESPW_STOPSMART)
//            &&(ESPW_state != ESPW_STARTSMART)&&(ESPW_state != ESPW_STOPSMART))
//#endif
    if(/*Banderas.networkStatus && */ESPW_state != state)
    {
        ESPW_oldState = ESPW_state;
        ESPW_state = state;        
    }
    return;
}

//Funci�n que devuelve el estado actual de la tarea ESPW_task()
uint8_t ESPW_getState(void)
{
    return (ESPW_state);
}

//Funci�n que establece el valor de la bandera de recepci�n de datos
void ESPW_setRX_Status(uint8_t status)
{
    Banderas.gotDataFlag = status;
    return;
}

//Funci�n que devuelve el valor de la bandera de recepci�n de datos
uint8_t ESPW_getRX_Status(void)
{
    return(Banderas.gotDataFlag);
}

//Funci�n que retorna el valor del buffer de recepci�n de datos en una posici�n espec�fica
uint8_t ESPW_getBufferAtPosition(uint8_t position)
{
    return(ESPW_BufferRX_Data[position]);
}

//Funci�n que escribe en el buffer de transmisi�n de datos
void ESPW_setBuffer(uint8_t data)
{
    ESPW_BufferTX_Data[dataHead++]=data;
    if(dataHead>=SIZE_BUFFER_TX_DATA)
    {
        dataHead = 0;
    }
    dataBytes--;
    return;
}

//Funci�n que regresa el estado de la conexi�n con el Servidor/Dispositivo
uint8_t ESPW_getConnectionStatus(void)
{
    return Banderas.connectionStatus;
}

//Funci�n que regresa el estado de la conexi�n con la red WiFi
uint8_t ESPW_getNetworkStatus(void)
{
    return Banderas.networkStatus;
}

uint8_t ESPW_getTX_Status(void)
{
    if (Banderas.envioPendiente == 0 && Banderas.envioTerminado)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void ESPW_setTX_Status(uint8_t set_clear)
{
    Banderas.envioTerminado = set_clear;
    return;
}

int8_t ESPW_getRcvdBytes(void)
{
    return rcvLenght;
}

void ESPW_setError(uint8_t set_clear)
{   
    Banderas.fatal_error = set_clear;
    return;
}

uint8_t ESPW_getError(void)
{
    return Banderas.fatal_error;
}

uint8_t ESPW_getNetworkID(void)
{
    return Banderas.conexion_MNI;
}
//Funci�n que compara un string con el buffer de recepci�n de comandos
static uint8_t ESPW_compare(const uint8_t *string) 
{   
    volatile uint8_t i=commandTail;
    volatile uint8_t k=0;
    
    while (*string)
    {        
        if(ESPW_BufferRX_Commands[i++] != *string++)
        {            
            return 0;            
        }
        if(i>=SIZE_BUFFER_RX_COMMAND)
        {
            i=0;
        }
        ++k;
    }    
        
    commandBytes += k;
    commandTail = i;
    
    return 1;
}

//Funci�n que escribe un string o buffer dentro de otro buffer, empezando siempre desde la posici�n 0
static void ESPW_putToBuffer(uint8_t *Destino,uint8_t *String)
{    
    while(*Destino=*String)
    {        
        String++;
        Destino++;       
    }
    
    return;
}

//Funci�n que limpia un buffer que contiene un string
static void ESPW_clearBuffer(uint8_t *Buffer)
{
    while( *Buffer )
    {
        *Buffer=0;
        Buffer++;
    }
    
    return;
}

//Funci�n que agrega un string u otro buffer, al final del buffer destino, mientras que haya espacio
static void ESPW_concatenar(uint8_t *Destino, uint8_t *string, uint8_t tamannioHacia)
{
    tamannioHacia-=2;                                                           //Direcci�n del �ltimo byte vac�a para que se respeste el formato de string
    while(*Destino && tamannioHacia)
    {                
        Destino++;
        tamannioHacia--;
    }
    
    while((*Destino++ = *string++)&&(tamannioHacia--))
    {             
        ;
    }
    
    return;
}

//Funci�n que escribe un buffer a trav�s de UART
static void ESPW_WriteRawBuffer(void)
{
    uint8_t Aux = msgLenght[msgLenghtTail][0];    
    dataTailTemp = dataTail;
    dataBytesTemp = dataBytes;
    while(Aux--)
    {
        (*ptrWriteUART)(ESPW_BufferTX_Data[dataTailTemp++]);
        if(dataTailTemp>=SIZE_BUFFER_TX_DATA)
        {
            dataTailTemp = 0;
        }
        dataBytesTemp++;
    }    
    return;
}

//Funci�n que escribe un string o un buffer que contenga un string a trav�s de UART
static void ESPW_WriteString(const uint8_t *ptrTXdata)
{
    while(*ptrTXdata)
    {
        (*ptrWriteUART)(*ptrTXdata++);        
    }
}

static void ESPW_recorreBuffer(void)
{
    if(commandTail != commandHead)
    {
        while(ESPW_BufferRX_Commands[commandTail++] != '\r')
        {                    
            if(commandTail>=SIZE_BUFFER_RX_COMMAND) 
            { 
                commandTail=0; 
            } 
            commandBytes++; 
        }     
        if(commandTail>=SIZE_BUFFER_RX_COMMAND) 
        { 
            commandTail=0; 
        } 
        commandBytes++; 
    } 
    
    return; 
} 

//Funci�n que espera que se active una bandera, antes de que se llegue a un timeout
static uint8_t ESPW_waitFor(const uint8_t *string)
{
    uint8_t respuestaBuscada = 0;    
    while (respuestaBuscada==0)
    {
        if(lineReceived)
        {
            lineReceived--;
            if (ESPW_compare(ERROR))
            {
                __delay_ms(BUSY_TIME);
                ESPW_WriteString(ESPW_BufferTX_Commands);            
            }               
            if(ESPW_compare(string))
            {
                respuestaBuscada=1;
            }
            else
            {
                ESPW_recorreBuffer();
            }
        }
        if (timeout>=INIT_TIMEOUT)
        {                        
            timeout = 0;
            return SIN_RESPUESTA;            
        }
        timeout++;        
    }    
    timeout = 0;
    
    return RESPUESTA_OK;
}

//Tarea que maneja al m�dulo ESP32 cuando se usa el WiFi
void ESPW_task(void)
{    
    switch (ESPW_state)
    {
        case ESPW_IDLE:            
            break;
            
        case ESPW_RESPUESTA:     
            
            // <editor-fold defaultstate="collapsed" desc="ESPERA RESPUESTA">                        
            if (lineReceived) 
            {
                lineReceived--;
                timeout = 0;
                if (ESPW_compare(CONEXION_0_ESTABLECIDA))
                {
                    Banderas.connectionStatus++;           
                }                
                else if (ESPW_compare(CONEXION_1_ESTABLECIDA))
                {
                    Banderas.connectionStatus++;           
                }                
                else if (ESPW_compare(CONEXION_ESTABLECIDA))
                {
                    Banderas.connectionStatus++;           
                }                
                else if (ESPW_compare(CONEXION_2_ESTABLECIDA))
                {
                    Banderas.connectionStatus++;           
                }                
                else if(ESPW_compare(ENVIO_CORRECTO)) 
                {
                    Banderas.envioPendiente--;
                    msgLenghtTail++;                    
                    dataTail = dataTailTemp;
                    dataBytes = dataBytesTemp;
                    if(msgLenghtTail>= NUMERO_DE_ENVIOS_EN_COLA)
                    {
                        msgLenghtTail = 0;
                    }
                    
                    if(Banderas.envioPendiente != 0)
                    {                        
                        Banderas.envioSiguiente = 1;
                        ESPW_setState(ESPW_SEND);
                    }
                    else
                    {
                        Banderas.envioTerminado = 1;                        
                        ESPW_clearBuffer(ESPW_BufferTX_Commands);                    
                        ESPW_setState(ESPW_IDLE);
                    }                    
                }
                else if (ESPW_compare(ENVIO_FALLIDO))
                {
                    ESPW_setState(ESPW_SEND);
                }
                else if (ESPW_compare(ERROR)) 
                {
                    Banderas.error = 1;
                    ESPW_state = ESPW_RETRANSMISION;
                } 
                else if(ESPW_compare(CONEXION_0_CERRADA))
                {
                    Banderas.connectionStatus--;
                }
                else if(ESPW_compare(CONEXION_1_CERRADA))
                {
                    Banderas.connectionStatus--;
                }
                else if(ESPW_compare(CONEXION_2_CERRADA))
                {
                    Banderas.connectionStatus--;
                }
                else if(ESPW_compare(IP_OBTENIDA) && Banderas.smartconfig == 0)
                {
                    Banderas.networkStatus = 1;
                }
                else if (ESPW_compare(OK)) 
                {
                    if (Banderas.error == 1) 
                    {
                        ESPW_state = ESPW_RETRANSMISION;
                        Banderas.error = 0;
                        intentos++;
                        if (intentos >= MAX_INTENTOS)
                        {
                            ESPW_clearBuffer(ESPW_BufferTX_Commands);
                            ESPW_setState(ESPW_IDLE);
                            intentos = 0;
                            Banderas.fatal_error =1 ;
                        }
                    }
                    else if (ESPW_oldState == ESPW_SEND) 
                    {
                        __delay_ms(BUSY_TIME);
                        ESPW_WriteRawBuffer();
                    }                                         
#if !SSID_PWD_FIXED                    
                    else if (ESPW_oldState == ESPW_STARTSMART)
                    {
                        ESPW_state = ESPW_STOPSMART;
                    }
#endif
                    else 
                    {
                        ESPW_clearBuffer(ESPW_BufferTX_Commands);
                        ESPW_setState(ESPW_IDLE);
                    }
                }          
                else
                {
                   ESPW_recorreBuffer();       
                }
            }
            timeout++;
            if (timeout>=TASK_TIMEOUT)
            {                        
                ESPW_state = ESPW_RETRANSMISION;
                timeout = 0;
            }
            // </editor-fold>

            break;
            
        case ESPW_TRANSMISION:
            
            // <editor-fold defaultstate="collapsed" desc="TRANSMISION DE COMANDOS">
            __delay_ms(BUSY_TIME);
            ESPW_WriteString(ESPW_BufferTX_Commands);            
            ESPW_state = ESPW_RESPUESTA; 
            // </editor-fold>
            
            break;
            
        case ESPW_RETRANSMISION:

            // <editor-fold defaultstate="collapsed" desc="RETRANSMISI�N DE COMANDOS">
            __delay_ms(BUSY_TIME);
            if (Banderas.error) 
            {
                ESPW_WriteString("AT\r\n");
            } 
            else 
            {
                ESPW_WriteString(ESPW_BufferTX_Commands);
            }

            ESPW_state = ESPW_RESPUESTA;
            // </editor-fold>

            break;
            
        case ESPW_SEND:
            
            // <editor-fold defaultstate="collapsed" desc="ENV�O DE DATOS">
            if(1 <= Banderas.envioPendiente || 1 == Banderas.envioSiguiente)
            {
                uint8_t Aux = msgLenght[msgLenghtTail][0];                
                
                ESPW_putToBuffer(ESPW_BufferTX_Commands, "AT+CIPSEND=");
                if(Banderas.MultipleConections == 1)
                {
                    uint8_t AuxString[3] = "";
                    AuxString[0]= msgLenght[msgLenghtTail][1] + '0';
                    AuxString[1]= ',';
                    ESPW_concatenar(ESPW_BufferTX_Commands, AuxString, SIZE_BUFFER_TX_COMMAND);
                }
                if(Aux>99)
                {                
                    lenght[0] = (Aux / 100) + '0';
                    Aux %=100;
                    lenght[1] = (Aux / 10) + '0';
                    lenght[2] = (Aux % 10) + '0';
                }
                else if (Aux > 9) 
                {
                    lenght[0] = (Aux / 10) + '0';
                    lenght[1] = (Aux % 10) + '0';
                    lenght[2] = 0;
                } 
                else 
                {
                    lenght[0] = (Aux) + '0';
                    lenght[1] = 0;
                    lenght[2] = 0;
                }
                ESPW_concatenar(ESPW_BufferTX_Commands, lenght, SIZE_BUFFER_TX_COMMAND);
                ESPW_concatenar(ESPW_BufferTX_Commands, "\r\n", SIZE_BUFFER_TX_COMMAND);            
                ESPW_setState(ESPW_TRANSMISION);                                       
            }            
            else
            {
                Banderas.envioPendiente++;
                ESPW_setState(ESPW_oldState);
            }
            
            break;
            // </editor-fold>
       
#if !SSID_PWD_FIXED            
        case ESPW_STARTSMART:
            
            // <editor-fold defaultstate="collapsed" desc="START SMART CONFIG">
            __delay_ms(BUSY_TIME);            
            ESPW_WriteString("AT+RST\r\n");
            ESPW_waitFor(READY);
            ESPW_WriteString("AT+CWSTARTSMART=3\r\n");                        
            ESPW_oldState = ESPW_state;
            ESPW_state = ESPW_RESPUESTA;
            Banderas.networkStatus = 0;
            Banderas.connectionStatus = 0;
            Banderas.conexion_MNI = 0;
            Banderas.smartconfig = 1;
            // </editor-fold>
            
            break;
            
        case ESPW_STOPSMART:
            
            // <editor-fold defaultstate="collapsed" desc="STOP SMART CONFIG">
            if (lineReceived) 
            {
                lineReceived--;
                if (ESPW_compare(SMART_CONECTED)) 
                {                    
                    __delay_ms(BUSY_TIME);
                    ESPW_WriteString("AT+CWSTOPSMART\r\n");
                    ESPW_waitFor(OK);
                    Banderas.networkStatus = 1;
                    Banderas.smartconfig = 0;
                    timeout = 0;
                    ESPW_oldState = ESPW_IDLE;
                    ESPW_state = ESPW_IDLE;                    
                }                 
                else if (ESPW_compare(CONEXION_CON_MNI))
                {
                    Banderas.conexion_MNI = 1;
                }
                else if (ESPW_compare(SMART_INITIED))
                {
                    Banderas.initTimeout = 1;
                }                
                else 
                {
                    ESPW_recorreBuffer();
                }                                
            }
            if(Banderas.initTimeout)
            {
                timeout++;
            }
            if(timeout>=TASK_TIMEOUT)
            {
                __delay_ms(BUSY_TIME);
                ESPW_WriteString("AT+CWSTOPSMART\r\n");                
                ESPW_state = ESPW_STARTSMART;                
                Banderas.initTimeout = 0;
                timeout = 0;
            }                                          
            // </editor-fold>
            
            break;                    
#endif
        default:
            break;
    }
}   