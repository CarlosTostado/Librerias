/*
* File:            RA8875.c
* Author:          Ing. Carlos Tostado
* Date:            28 de Julio 2017
* PIC:             PIC18F4xK22
* Versi�n:         1.0
* 
* Libreria  
* 
* Version  1.x (Julio 27, 2017)
* Autor: Ing. Carlos Tostado
* 
* 
*/

#include "RA8875.h"

//Variables locales para el funcionamiento de las funciones
static RA8875_Shape figuraHelper = {0};
static uint8_t b_textSize = 0;

//Punteros a funcion para el uso del Driver SPI
static void (*writeSpiPtr)(uint8_t);
static uint8_t (*readSpiPtr)(void);
static uint8_t *chipSelect;
static uint8_t bitMask;

/*************************** Funciones de registro ****************************/ 

void RA8875_SPI_Register(void (*functionWritePtr)(uint8_t), uint8_t (*functionReadPtr)(void))
{
    writeSpiPtr = functionWritePtr;
    readSpiPtr  = functionReadPtr;
    
    return;
}

void RA8875_SPI_PIN_Register(volatile unsigned char *ptrPort, unsigned char Byte)
{
    chipSelect = ptrPort;
    bitMask = Byte;
    return;
    
}

void RA8875_Constructor (RA8875_t *ptrConstructor)
{
    ptrConstructor->enviarDatos = RA8875_Write;
    ptrConstructor->init = init_RA8875;
    ptrConstructor->enciendeDisplay = powerOnDisplay;
    ptrConstructor->apagaDisplay = powerOffDisplay;
    ptrConstructor->limpiaDisplay = clearWindow;
    ptrConstructor->ajustaBrillo = setBrightness;
    ptrConstructor->colocarCursor = setCursor;
    ptrConstructor->colocarCursorGrafico = setGraphicsCursor;
    ptrConstructor->colocarTexto = setText_LCD;
    ptrConstructor->colocarCaracter = setChar_LCD;
    ptrConstructor->colocarTexto_Color = setTextColor;
    ptrConstructor->colocarTexto_Transparent = setTextTransparent;
    ptrConstructor->colocarTexto_Ampliacion = setTextEnlargenemnt;
    ptrConstructor->colocarTexto_ColorFondo = setTextBackground;
    ptrConstructor->colocarFondo = setBackground;
    ptrConstructor->colocarModo_LCD = setModeLCD;
    ptrConstructor->resetSoftware = setSoftwareReset;
    
    ptrConstructor->dibujaRectangulo = drawRectangle;
    ptrConstructor->dibujaLinea = drawLine;
    ptrConstructor->dibujaCirculo = drawCircle;
    ptrConstructor->dibujaTriangulo = drawTriangle;
    ptrConstructor->dibujaPixel = drawPixel;
    ptrConstructor->convertir_RGB888_To_RGB565 = convertToRGB565;

    ptrConstructor->crearNuevo_Simbolo_o_Fuente = createNew_Font_Or_Symbol;
    ptrConstructor->mostrar_SimbolosEspeciales = showEspecial_Symbols;
    
    return;
}

/**************************** Funciones locales  ******************************/ 

static inline void escribirRegistro(uint8_t tipo, uint8_t info)
{
    *chipSelect &= (~bitMask);                                                  // CS en 0
    NOP();
    (*writeSpiPtr)(tipo);                                                       // Escribe por SPI
    (*writeSpiPtr)(info);                                                       // Escribe por SPI
    *chipSelect |= (bitMask);                                                   // CS en 1
    NOP();
}

static inline void init_RA8875_PLL(void)
{
    //Pantalla de 800x480 system clock (45MHz) o 80
    RA8875_Write(REGISTER_PLLC1, 0x08);                                         //Parametro de entrada 8                                          
    Wait1ms(1);
    
    RA8875_Write(REGISTER_PLLC2, 0x02);                                          //PLL dividido en 2
    Wait1ms(1);
   
    return;
} 

static inline void init_Horizontal_Screen(void)
{
    // System Configuration Register
    RA8875_Write(REGISTER_PCS, 0x81);                                           // PCLK Falling Edge, PCLK Period 2 (45 / 2 = )                                          
    Wait1ms(1);
    
    // LCD Horizontal Display Width Register
    RA8875_Write(REGISTER_HDW, 0x63);                                           // Tama�o de panel horizontal 800 pixeles                                         
    
    // Horizontal Non-Display Period Fine Tuning Option Register
    RA8875_Write(REGISTER_HNDFTR, 0x00);                                        // DE High Active, HNDFT  0                                        

    // LCD Horizontal Non-Display Period Register
    RA8875_Write(REGISTER_HNDR, 0x03);                                 
    
    // HSYNC Start Position RegisteR
    RA8875_Write(REGISTER_HSTR, 0x03);  

    // HSYNC Pulse Width Register
    RA8875_Write(REGISTER_HPWR, 0x0B);  
    
    return;
}

static inline void init_Vertical_Screen(void)
{
    RA8875_Write(REGISTER_VDH0, 0xDF);                                          // LCD Vertical Display Height Register
    RA8875_Write(REGISTER_VDH1, 0x01);                                          // LCD Vertical Display Height Register0   

    RA8875_Write(REGISTER_VND0, 0x1F);                                          // LCD Vertical Non-Display Period Register 
    RA8875_Write(REGISTER_VND1, 0x00);                                          // LCD Vertical Non-Display Period Register 1    
    
    RA8875_Write(REGISTER_VST0, 0x16);                                          // VSYNC Start Position[7:0]    
    RA8875_Write(REGISTER_VST1, 0x00);                                          // VSTR1 //VSYNC Start Position[8]

    RA8875_Write(REGISTER_VPW, 0x01);                                           // VSYNC Polarity ,VSYNC Pulse Width[6:0]  
}

static void configActiveWindow(uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2)
{
    uint8_t temp = 0;
    
    RA8875_Write(REGISTER_HSAW0,(x1 & 0xFF));                                   // Horizontal Start Point of Active Window [7:0]                                    
    temp = (x1 & 0x300 )>>8;
    RA8875_Write(REGISTER_HSAW1,temp);                                          // Horizontal Start Point of Active Window [9:8]
    
    RA8875_Write(REGISTER_HEAW0,(x2 & 0xFF));                                   // Horizontal End Point of Active Window [7:0]                                     
    temp = (x2 & 0x300 )>>8;
    RA8875_Write(REGISTER_HEAW1,temp);                                          // Horizontal End Point of Active Window [9:8]
    
    RA8875_Write(REGISTER_VSAW0,(y1 & 0xFF));                                   // Vertical Start Point of Active Window [7:0]    
    temp = (y1 & 0x300 )>>8;
    RA8875_Write(REGISTER_VSAW1,temp);                                          // Vertical Start Point of Active Window [8]
   
    RA8875_Write(REGISTER_VEAW0,(y2 & 0xFF));                                   // Vertical End Point of Active Window 0 (VEAW0)   
    temp = (y2 & 0x300 )>>8;
    RA8875_Write(REGISTER_VEAW1,temp);                                          // Vertical End Point of Active Window 1 (VEAW1)                                     
    
    return;
}

/**************************** Manejo del driver  ******************************/ 

void RA8875_Write (uint8_t Register, uint8_t Data)
{
    escribirRegistro(COMAND_WRITE,Register);
    escribirRegistro(DATA_WRITE,Data);
    
    return;
}

uint8_t readState_RA8875(void)
{
    uint8_t temp = 0;
    
    *chipSelect &= (~bitMask);                                                  // CS 0
    (*writeSpiPtr)(STATUS_READ);
    temp = (*readSpiPtr)();

    *chipSelect |= (bitMask);                                                   // CS 1

    return temp;
}

uint8_t readRegister_RA8875 (uint8_t Registro)
{
    uint8_t temp = 0;
            
    *chipSelect &= (~bitMask);                                                  // CS 0
    (*writeSpiPtr)(COMAND_WRITE);
    (*writeSpiPtr)(Registro);
    *chipSelect |= (bitMask);                                                   // CS 1
    
    __delay_ms(1);
    
    *chipSelect &= (~bitMask);                                                  // CS 0
    (*writeSpiPtr)(DATA_READ);
    temp = (*readSpiPtr)();
    *chipSelect |= (bitMask);                                                   // CS 1
    
    return temp;
} 

//RA8875_Funcion que pregunta el estado del proceso, es necesario ingresar el registro y la bandera que indica el estado
uint8_t isProcessDone(uint8_t Register, uint8_t statusFlag)
{
    //Es recomendable utilizar un timeOut al utilizar esta funcion
    uint8_t temp = 0;
    
    // Lee el estado actual del registro y lo compara con la bandera
    temp = readRegister_RA8875(Register);
    if (0 == (temp & statusFlag))
    {
        return 1;
    }
    else
        return 0;
}

/**************** Funciones para manejo de la interfaz ************************/

//RA8875_Funcion para inicializar las funciones del driver 
void init_RA8875 (void)
{
    uint8_t varTemp = 0;
    
    //Configura la direcci�n de los pines a utilizar
    configReset_PIN();
    configWait_PIN();
    
    //Secuencia de inicio para driver
    RA8875_RESET = 0;
    Wait1ms(10);
    RA8875_RESET = 1;
    Wait1ms(10);
    
    //Configuraci�n inicial PLL
    init_RA8875_PLL();
    
    //Configuraci�n de la interfaz y profundidad de color
    RA8875_Write (REGISTER_SYS,SYSR_16BPP | SYSR_8Bit);
    Wait1ms(1);
    
    //Configuraci�n Horizontal de la pantalla
    init_Horizontal_Screen();
    
    //Configuraci�n Vertical de la pantalla
    init_Vertical_Screen();
    
    //Extra General Purpose IO Register (GPIOX)
    RA8875_Write(REGISTER_GPIOX, 0x01);
    
    //Encendemos display
    RA8875_Write(REGISTER_PWRR, PWRR_LCD_NORMAL | PWRR_LCD_ON);                 // Power and Display Control Register
    
    //Graphic Mode - Cursor Not Visible - Normal Display - Cursor don�t auto-increase
    RA8875_Write(REGISTER_MWCR0, 0x01);                           
    
    //Configuraci�n del display
    RA8875_Write(REGISTER_DPCR, DPCR_OneLayer);
   
    //Memory Write Control Register 1 (MWCR0)
    RA8875_Write(REGISTER_MWCR1, 0x00);

    //Configurar ventana activa en X (0,799) y Y (0,479)
    configActiveWindow(0,799,0,479);                                            
   
    //Limpiamos la ventana completa
    clearWindow();
   
    //Configurar PWM
    RA8875_Write(REGISTER_P1CR, P1CR_PWM_ENABLE | 0x0A);                        // Div1024 y PWM Enable                     
    
    //Ajusta el brillo de la pantalla
    setBrightness(0xFE);
    Wait1ms(500);
    
    //Encendemos display
    powerOnDisplay();
    
    return;
}

//RA8875_Limpia la memoria, borrando la ventana completa
void clearWindow(void)
{   
    RA8875_Write(REGISTER_MCLR, MCLR_MEMORY_CLEAR | MCLR_FULL_WINDOW);          // Memory Clear Control Register
    
    while (0 == isProcessDone(REGISTER_MCLR, STATUS_MCLR));
    
    return; 
}

//RA8875_Enciende el display 
void powerOnDisplay (void)
{
    // Power and Display Control Register
    RA8875_Write(REGISTER_PWRR, PWRR_LCD_NORMAL | PWRR_LCD_ON);                 // Enciende display  
    return;
}

//RA8875_Apaga display el display 
void powerOffDisplay (void)
{
    // Power and Display Control Register
    RA8875_Write(REGISTER_PWRR, PWRR_LCD_NORMAL | PWRR_LCD_OFF);                 // Apaga display  
    return;    
}

//RA8875_Ajusta el brillo de la pantalla, valores de entrada de 0 - 255
void setBrightness (uint8_t brightness)
{
    //Salida PWM, varia el brillo de la pantalla
    escribirRegistro(COMAND_WRITE, REGISTER_P1DCR);    
    escribirRegistro(DATA_WRITE,brightness); 
}

//RA8875_Ingresa la posicion del cursor en pixeles
void setCursor (uint16_t x, uint16_t y)
{
    uint8_t temp = 0;
    
    /* Ingresa X */
    RA8875_Write(REGISTER_F_CURXL, (x & 0x00FF));
    temp = (x & 0x0300) >> 8;
    RA8875_Write(REGISTER_F_CURXH, temp);        

    /* Ingresa Y */
    RA8875_Write(REGISTER_F_CURYL, (y & 0x00FF));
    temp = (y & 0x0300) >> 8;
    RA8875_Write(REGISTER_F_CURYH, temp);
    
    return;
}

//RA8875_Ingresa la posicion del cursor en pixeles
void setGraphicsCursor (uint16_t x, uint16_t y)
{
    uint8_t temp = 0;
    
    /* Ingresa X */
    RA8875_Write(REGISTER_CURH0, (x & 0x00FF));
    temp = (x & 0x0300) >> 8;
    RA8875_Write(REGISTER_CURH1, temp);        

    /* Ingresa Y */
    RA8875_Write(REGISTER_CURV0, (y & 0x00FF));
    temp = (y & 0x0300) >> 8;
    RA8875_Write(REGISTER_CURV1, temp);
    
    
    return;
}

//RA8875_Funcion para escribir texto en la pantalla, MODO TEXT
void setText_LCD (const uint8_t *ptrBuffer)
{
    escribirRegistro(COMAND_WRITE, REGISTER_MRWC);
    
    while (*ptrBuffer)
    {
        escribirRegistro(DATA_WRITE, *ptrBuffer);
        ptrBuffer++;
        
        if (1 <= b_textSize){
            Wait1ms(1);
        }
    }
    
    return;
}

//RA8875_Funcion para escribir un caracter en la pantalla, MODO TEXT
void setChar_LCD (const uint8_t caracter)
{
    escribirRegistro(COMAND_WRITE, REGISTER_MRWC);
    escribirRegistro(DATA_WRITE, caracter);
    
    if (1 <= b_textSize){
        Wait1ms(1);
    }
    
    return;
}

//RA8875_Funcion para signar color a texto, foreground = Color de la letra
void setTextColor (uint16_t foregroundColor, uint16_t backgroundColor)
{
    uint8_t temporal = 0;
    /* Set Foreground Color RGB565 */
    temporal = (foregroundColor & 0xF800) >> 11;
    RA8875_Write(0x63, temporal);                                               //Red 
    temporal = (foregroundColor & 0x07E0) >> 5;
    RA8875_Write(0x64, temporal);                                               //Green
    temporal = (foregroundColor & 0x001F);
    RA8875_Write(0x65, temporal);                                               //Blue
    
    /* SetBackground Color RGB565 */
    temporal = (backgroundColor & 0xF800) >> 11;
    RA8875_Write(0x60, temporal);                                               //Red 
    temporal = (backgroundColor & 0x07E0) >> 5;
    RA8875_Write(0x61, temporal);                                               //Green
    temporal = (backgroundColor & 0x001F);
    RA8875_Write(0x62, temporal);                                               //Blue
    
    //En el registro 22 puedes habilitar o deshabilitar el color de fondo
}

//RA8875_Funcion para signar color a texto
void setTextTransparent (uint16_t foregroundColor)
{
    uint8_t temporal = 0;
    /* Set Foreground Color RGB565 */
    temporal = (foregroundColor & 0xF800) >> 11;
    RA8875_Write(0x63, temporal);                                               //Red 
    temporal = (foregroundColor & 0x07E0) >> 5;
    RA8875_Write(0x64, temporal);                                               //Green
    temporal = (foregroundColor & 0x001F);
    RA8875_Write(0x65, temporal);                                               //Blue
    
    temporal = readRegister_RA8875(REGISTER_FNCR1);
    RA8875_Write(REGISTER_FNCR1,temporal | FNCR1_FONT_TRASNPARENCY); 
    
}

//RA8875_Funcion para signar color a fondo de la letra
void setTextBackground (uint16_t backgroundColor)
{
   uint8_t temporal = 0;
   
    /* SetBackground Color RGB565 */
   temporal = (backgroundColor & 0xF800) >> 11;
   RA8875_Write(0x60, temporal);                                                //Red 
   temporal = (backgroundColor & 0x07E0) >> 5;
   RA8875_Write(0x61, temporal);                                                //Green
   temporal = (backgroundColor & 0x001F);
   RA8875_Write(0x62, temporal);                                                //Blue
   
   
    temporal = readRegister_RA8875(REGISTER_FNCR1);
    RA8875_Write(REGISTER_FNCR1,temporal & (~FNCR1_FONT_TRASNPARENCY));
}

//RA8875_Funcion para incrementar el tama�o de texto, rango 0 - 3
void setTextEnlargenemnt (uint8_t size)
{
    uint8_t temporal = 0;
    
    //Poner una restricci�n al tama�o 
    if (3 <= size)
    {
        size = 3;
    }
    
    //Rutina para agregar un retardo a la funci�n de escribir texto
    if (size > 1)
    {
        b_textSize = 1;
    }
    else
    {
        b_textSize = 0;
    }
    
    temporal = readRegister_RA8875(REGISTER_FNCR1);
    
    temporal &= 0xF0;                                                           //Limpia bits 0 - 3
    temporal |= (size << 2);                                                    //Asign horizontal Enlargement 
    temporal |= size;                                                           //Asign horizontal Enlargement 
    RA8875_Write(REGISTER_FNCR1,temporal); 

    
}

//RA8875_Funcion para poner el fondo de la pantalla en MODO_GRAPHICS
void setBackground (const uint16_t color)
{
    figuraHelper.x1 = 0;
    figuraHelper.y1 = 0;
    figuraHelper.x2 = 799;
    figuraHelper.y2 = 479;
    figuraHelper.Color = color;
    figuraHelper.fill = 1;
    drawRectangle(&figuraHelper);
}

void setModeLCD (const uint8_t mode)
{
    if (mode & 1)
    {
        RA8875_Write(REGISTER_MWCR0, MWCR0_TXTMODE | MWCR0_CURSOR_NONVISIBLE | MWCR0_CURSOR_NOTBLINK | MWCR0_MEMORY_AUTOINCREASE); 
    }
    else 
    {
        RA8875_Write(REGISTER_MWCR0, MWCR0_GFXMODE | MWCR0_MEMORY_AUTOINCREASE);         
    }
}

//RA8875_Funcion que genera un reset por software
void setSoftwareReset(void)
{
    uint8_t temporal = 0;
    
    temporal = readRegister_RA8875(REGISTER_PWRR);
    RA8875_Write(REGISTER_PWRR, (temporal | PWRR_SOFT_RESET));
    
    __delay_ms(1);
    
    RA8875_Write(REGISTER_PWRR, (temporal & (~PWRR_SOFT_RESET)));
    
    return;
}

/*************************** Funciones Auxiliares *****************************/

//RA8875_Dibuja un rectangulo en una localidad especifica
void drawRectangle (RA8875_Shape *ptrRectangulo)
{
    uint8_t temporal = 0; 
    
    /* Set X1 */
    temporal = (ptrRectangulo->x1 & 0xFF);
    RA8875_Write(REGISTER_DLHSR0,  temporal);
    temporal = (ptrRectangulo->x1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLHSR1, temporal);
    
    /* Set Y1 */
    temporal = (ptrRectangulo->y1 & 0xFF);
    RA8875_Write(REGISTER_DLVSR0, temporal);
    temporal = (ptrRectangulo->y1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLVSR1, temporal);
  
    /* Set X2 */
    temporal = (ptrRectangulo->x2 & 0xFF);
    RA8875_Write(REGISTER_DLHER0, temporal );
    temporal = (ptrRectangulo->x2 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLHER1, temporal);
    
    /* Set Y2 */
    temporal = (ptrRectangulo->y2 & 0xFF);
    RA8875_Write(REGISTER_DLVER0, temporal);
    temporal = (ptrRectangulo->y2 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLVER1, temporal);    
    
    /* Set Color RGB565 */
    temporal = (ptrRectangulo->Color & 0xF800) >> 11;
    RA8875_Write(0x63, temporal);                                               //Red 
    temporal = (ptrRectangulo->Color & 0x07E0) >> 5;
    RA8875_Write(0x64, temporal);                                               //Green
    temporal = (ptrRectangulo->Color & 0x001F);
    RA8875_Write(0x65, temporal);                                               //Blue

    
  /* Draw! RA8875_DCR Fill*/
    if (1 == ptrRectangulo->fill)
    {
        RA8875_Write(REGISTER_DCR,DCR_DrawSquare | DCR_Fill | DCR_StartDrawing);
    }
    else
    {
        RA8875_Write(REGISTER_DCR,DCR_DrawSquare | DCR_NonFill | DCR_StartDrawing); 
    }
  
  while (0 == isProcessDone(REGISTER_DCR, STATUS_DCR_LST));

  return;
}

//RA8875_Dibuja una linea en una localidad especifica
void drawLine (RA8875_Shape *ptrLine)
{
    uint8_t temporal = 0; 
    
    /* Set X1 */
    temporal = (ptrLine->x1 & 0xFF);
    RA8875_Write(REGISTER_DLHSR0,  temporal);
    temporal = (ptrLine->x1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLHSR1, temporal);
    
    /* Set Y1 */
    temporal = (ptrLine->y1 & 0xFF);
    RA8875_Write(REGISTER_DLVSR0, temporal);
    temporal = (ptrLine->y1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLVSR1, temporal);
  
    /* Set X2 */
    temporal = (ptrLine->x2 & 0xFF);
    RA8875_Write(REGISTER_DLHER0, temporal );
    temporal = (ptrLine->x2 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLHER1, temporal);
    
    /* Set Y2 */
    temporal = (ptrLine->y2 & 0xFF);
    RA8875_Write(REGISTER_DLVER0, temporal);
    temporal = (ptrLine->y2 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLVER1, temporal);    
    
    /* Set Color RGB565 */
    temporal = (ptrLine->Color & 0xF800) >> 11;
    RA8875_Write(0x63, temporal);                                               //Red 
    temporal = (ptrLine->Color & 0x07E0) >> 5;
    RA8875_Write(0x64, temporal);                                               //Green
    temporal = (ptrLine->Color & 0x001F);
    RA8875_Write(0x65, temporal);                                               //Blue

    
  /* Draw! RA8875_DCR Fill*/
    if (1 == ptrLine->fill)
    {
        RA8875_Write(REGISTER_DCR,DCR_DrawLine | DCR_Fill | DCR_StartDrawing);
    }
    else
    {
        RA8875_Write(REGISTER_DCR,DCR_DrawLine | DCR_NonFill | DCR_StartDrawing); 
    }
  
    //Espera que el comando termine el proceso
    while (0 == isProcessDone(REGISTER_DCR, STATUS_DCR_LST));
    return;
}

//RA8875_Dibuja un cirulo en una localidad especifica
void drawCircle (RA8875_Shape *ptrCircle)
{
    uint8_t temporal = 0; 
    
    /* Set X1 */
    temporal = (ptrCircle->x1 & 0xFF);
    RA8875_Write(REGISTER_DCHR0,  temporal);
    temporal = (ptrCircle->x1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DCHR1, temporal);
    
    /* Set Y1 */
    temporal = (ptrCircle->y1 & 0xFF);
    RA8875_Write(REGISTER_DCVR0, temporal);
    temporal = (ptrCircle->y1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DCVR1, temporal);
  
    /* Set Radio */
    RA8875_Write(REGISTER_DCRR, ptrCircle->radio );   
    
    /* Set Color RGB565 */
    temporal = (ptrCircle->Color & 0xF800) >> 11;
    RA8875_Write(0x63, temporal);                                               //Red 
    temporal = (ptrCircle->Color & 0x07E0) >> 5;
    RA8875_Write(0x64, temporal);                                               //Green
    temporal = (ptrCircle->Color & 0x001F);
    RA8875_Write(0x65, temporal);                                               //Blue

    
  /* Draw! RA8875_DCR Fill*/
    if (1 == ptrCircle->fill)
    {
        RA8875_Write(REGISTER_DCR,DCR_StartCircleDrawing | DCR_Fill );
    }
    else
    {
        RA8875_Write(REGISTER_DCR,DCR_StartCircleDrawing | DCR_NonFill ); 
    }
  
    //Espera que el comando termine el proceso
    while (0 == isProcessDone(REGISTER_DCR, STATUS_DCR_CIRCLE));
  return;   
}

//RA8875_Dibuja un trinagulo en una localidad especifica
void drawTriangle (RA8875_Shape *ptrTriangle)
{
    uint8_t temporal = 0; 
    
    /* Set X1 */
    temporal = (ptrTriangle->x1 & 0xFF);
    RA8875_Write(REGISTER_DLHSR0,  temporal);
    temporal = (ptrTriangle->x1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLHSR1, temporal);
    
    /* Set Y1 */
    temporal = (ptrTriangle->y1 & 0xFF);
    RA8875_Write(REGISTER_DLVSR0, temporal);
    temporal = (ptrTriangle->y1 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLVSR1, temporal);
  
    /* Set X2 */
    temporal = (ptrTriangle->x2 & 0xFF);
    RA8875_Write(REGISTER_DLHER0, temporal );
    temporal = (ptrTriangle->x2 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLHER1, temporal);
    
    /* Set Y2 */
    temporal = (ptrTriangle->y2 & 0xFF);
    RA8875_Write(REGISTER_DLVER0, temporal);
    temporal = (ptrTriangle->y2 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DLVER1, temporal);    
    
    /* Set X3 */
    temporal = (ptrTriangle->x3 & 0xFF);
    RA8875_Write(REGISTER_DTPH0, temporal );
    temporal = (ptrTriangle->x3 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DTPH1, temporal);
    
    /* Set Y2 */
    temporal = (ptrTriangle->y3 & 0xFF);
    RA8875_Write(REGISTER_DTPV0, temporal);
    temporal = (ptrTriangle->y3 & 0x0300) >> 8;
    RA8875_Write(REGISTER_DTPV1, temporal);   
    
    /* Set Color RGB565 */
    temporal = (ptrTriangle->Color & 0xF800) >> 11;
    RA8875_Write(0x63, temporal);                                               //Red 
    temporal = (ptrTriangle->Color & 0x07E0) >> 5;
    RA8875_Write(0x64, temporal);                                               //Green
    temporal = (ptrTriangle->Color & 0x001F);
    RA8875_Write(0x65, temporal);                                               //Blue

    
  /* Draw! RA8875_DCR Fill*/
    if (1 == ptrTriangle->fill)
    {
        RA8875_Write(REGISTER_DCR,DCR_DrawTriangle | DCR_Fill | DCR_StartDrawing);
    }
    else
    {
        RA8875_Write(REGISTER_DCR,DCR_DrawTriangle | DCR_NonFill | DCR_StartDrawing); 
    }
  
    //Espera que el comando termine el proceso
    while (0 == isProcessDone(REGISTER_DCR, STATUS_DCR_LST));
    return;    
    
}

//RA8875_Dibuja un pixel en una localidad especifica
void drawPixel (uint16_t x, uint16_t y, uint16_t color)
{
    uint8_t temporal = 0; 
    
    /* Set X1 */
    temporal = (x & 0xFF);
    RA8875_Write(0x46,  temporal);
    temporal = (x & 0x0300) >> 8;
    RA8875_Write(0x47, temporal);
    
    /* Set Y1 */
    temporal = (y & 0xFF);
    RA8875_Write(0x48, temporal);
    temporal = (y & 0x0300) >> 8;
    RA8875_Write(0x49, temporal);
    
    /* Set Color RGB565 */  
    escribirRegistro(COMAND_WRITE,REGISTER_MRWC);
    temporal = color >> 8;
    escribirRegistro(DATA_WRITE, temporal);
    escribirRegistro(DATA_WRITE, (color & 0x00FF));
    
  return;      
}

//RA8875_Funcion para convertir los colores RGB888 a formato RGB565
uint16_t convertToRGB565 (RGB_Color_t *ptrRGB)
{
    uint8_t temp1 = 0;
    uint8_t temp2 = 0;
    
    temp1 = (ptrRGB->Red & 0xF8) | (ptrRGB->Green >> 5);
    
    temp2 = ((ptrRGB->Green & 0x1C) << 3) | (ptrRGB->Blue >> 3);
    
    return ((temp1 << 8) | temp2); 
}

//RA8875_Crea una nueva fuente o simbolo, Revisar archivo en excel para el dibujo de simbolos
void createNew_Font_Or_Symbol (uint8_t address, uint8_t *ptrBuffer)
{
    //Secuencia obtenida de la pagina 113 del datasheet v1.9
    
    uint8_t auxiliar = 0;

    //Colocar la pantalla en modo grafico
    setModeLCD(MODE_GRAPHICS);
    
    //Selecciona la fuente CGRAM
    auxiliar = readRegister_RA8875(REGISTER_FNCR0);
    RA8875_Write(REGISTER_FNCR0, (FNCR0_CGROM_FONT | FNCR0_EXTERNAL_FONT | auxiliar));
    
    //Antes de seleccionar el espacio hay que coloar MWCR1 bit[3-2] = 0b01
    auxiliar = readRegister_RA8875(REGISTER_MWCR1);
    RA8875_Write(REGISTER_MWCR1, (auxiliar | MWCR1_CGRAM_DESTINATION));
    
    //Selecciona el espacio de la memoria CGRAM donde se escribira
    RA8875_Write(REGISTER_CGSR, address);
    
    //Escribe en la memoria
    escribirRegistro(COMAND_WRITE,REGISTER_MRWC);
    
    for (auxiliar = 0; auxiliar < 16; auxiliar++)
    {
        escribirRegistro(DATA_WRITE,ptrBuffer[auxiliar]);
    }
    
    return;
}

//RA8875_Muestra en pantalla los simbolos personalizados
void showEspecial_Symbols(uint8_t address)
{
    //Secuencia obtenida de la pagina 113 del datasheet v1.9
    
    uint8_t auxiliar = 0;
    
    //Selecciona el modo texto
    setModeLCD(MODE_TEXT);
    
    //Selecciona la fuente CGRAM
    auxiliar = readRegister_RA8875(REGISTER_FNCR0);
    RA8875_Write(REGISTER_FNCR0, (FNCR0_CGRAM_FONT | FNCR0_EXTERNAL_FONT | auxiliar));
    
    //Coloca en 0 los bits 3-2 del registro MWCR1
    auxiliar = readRegister_RA8875(REGISTER_MWCR1);
    RA8875_Write(REGISTER_MWCR1, (auxiliar & 0b11110011));
    
    //Escribe el simbolo o la fuente
    RA8875_Write(REGISTER_MRWC, address);
    
    return; 
    
}

/********************** Block Transfer Engine Function ************************/

void bte_writeData (const BTE_t *ptrBTE, const uint16_t *ptrData)
{
    uint16_t aux = 0;
    uint8_t temporal = 0;
    
/* *****************************************************************************
 * 
 * The Write BTE increases the speed of transferring data from MCU interface 
 * to the DDRAM. The Write BTE with ROP fills a specified area of the DDRAM 
 * with data supplied by the MCU. The Write BTE requires the MCU to provide data.
 * 
 *                    -----              ----------
 *                   |     |            |  RA8875  |
 *                   | MCU |   ----->   |          |
 *                   |     |            |   DDRAM  |
 *                    -----              ----------
 * 
 * ****************************************************************************/
    
 /* ****************************************************************************
  * 
  * The suggested programming steps and registers setting are listed below as reference.
  * 1. Setting destination position � REG[58h], [59h], [5Ah], [5Bh]
  * 2. Setting BTE width register � REG[5Ch], [5Dh]
  * 3. Setting BTE height register � REG[5Eh], [5Fh]
  * 4. Setting register Destination = source � REG[51h] = Ch
  * 5. Enable BTE function � REG[50h] Bit7 = 1
  * 6. Check STSR Bit7
  * 7. Write next image data
  * 8. Repeat step 6, 7 until image data = block image data. Or Check STSR Bit6
  * 
  * 
  * ***************************************************************************/   
    
    /* Setting destination position x */
    temporal = (ptrBTE->destination_x & 0xFF);
    RA8875_Write(REGISTER_HDBE0,  temporal);
    temporal = (ptrBTE->destination_x & 0x0300) >> 8;
    RA8875_Write(REGISTER_HDBE1, temporal);
    
    /* Setting destination position Y1 */
    temporal = (ptrBTE->destination_y & 0xFF);
    RA8875_Write(REGISTER_VDBE0, temporal);
    temporal = (ptrBTE->destination_y & 0x0300) >> 8;
    RA8875_Write(REGISTER_VDBE1, temporal);    
    
    /* Setting BTE width register 100*/ 
    temporal = (ptrBTE->width & 0xFF);
    RA8875_Write(REGISTER_BEWR0, temporal);
    temporal = (ptrBTE->width & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEWR1, temporal);

    /* Setting BTE height register 76*/
    temporal = (ptrBTE->height & 0xFF);
    RA8875_Write(REGISTER_BEHR0, temporal);
    temporal = (ptrBTE->height & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEHR1, temporal);

    /* Setting register Destination = source */
    RA8875_Write(REGISTER_BECR1, 0xC0);
            
    /* Enable BTE function Bit 7*/
     RA8875_Write(REGISTER_BECR0, BECR0_ENABLE_BTE);
     
    escribirRegistro(COMAND_WRITE,REGISTER_MRWC);
    
    for (aux = 0; aux < 7679; aux++)
    {
        /* Write next image data */
        

        temporal = (ptrData[aux] & 0xFF00) >> 8;
        escribirRegistro(DATA_WRITE,temporal); 
        temporal = (ptrData[aux] & 0x00FF);
        escribirRegistro(DATA_WRITE,temporal);
//        while(0 == isProcessDone(REGISTER_BECR0, STATUS_BTE_BUSY));

    }
    /* Check STSR Bit7 */
    while(0 == isProcessDone(REGISTER_BECR0, STATUS_BTE_BUSY));

    return;
}

void bte_movePositiveDirection (const BTE_t *ptrBTE )
{
    uint8_t temporal = 0;
    
 /* ****************************************************************************
  * The suggested programming steps and registers setting are listed below as reference.
  * 
  * 1. Setting source layer and address � REG[54h], [55h], [56h], [57h]
  * 2. Setting destination layer and address � REG[58h], [59h], [5Ah], [5Bh]
  * 3. Setting BTE width and height � REG[5Ch], [5Dh], [5Eh], [5Fh]
  * 4. Setting BTE operation and ROP function � REG[51h] Bit[3:0] = 2h
  * 5. Enable BTE function � REG[50h] Bit7 = 1
  * 6. Check STSR REG Bit6 � check 2D final
  * 
  * ***************************************************************************/  
    
    /* Setting source layer and address */
    temporal = (ptrBTE->source_x & 0xFF);
    RA8875_Write(REGISTER_HSBE0,  temporal);
    temporal = (ptrBTE->source_x & 0x0300) >> 8;
    RA8875_Write(REGISTER_HSBE1, temporal);
    
    temporal = (ptrBTE->source_y & 0xFF);
    RA8875_Write(REGISTER_VSBE0, temporal);
    temporal = (ptrBTE->source_y & 0x0300) >> 8;
    RA8875_Write(REGISTER_VSBE1, temporal);
    
    /* Setting destination layer and address */
    temporal = (ptrBTE->destination_x & 0xFF);
    RA8875_Write(REGISTER_HDBE0,  temporal);
    temporal = (ptrBTE->destination_x & 0x0300) >> 8;
    RA8875_Write(REGISTER_HDBE1, temporal);
    
    /* Setting destination position Y1 */
    temporal = (ptrBTE->destination_y & 0xFF);
    RA8875_Write(REGISTER_VDBE0, temporal);
    temporal = (ptrBTE->destination_y & 0x0300) >> 8;
    RA8875_Write(REGISTER_VDBE1, temporal);    
      
    /* Setting BTE width register 100*/ 
    temporal = (ptrBTE->width & 0xFF);
    RA8875_Write(REGISTER_BEWR0, temporal);
    temporal = (ptrBTE->width & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEWR1, temporal);

    /* Setting BTE height register 76*/
    temporal = (ptrBTE->height & 0xFF);
    RA8875_Write(REGISTER_BEHR0, temporal);
    temporal = (ptrBTE->height & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEHR1, temporal);
    
    /* Setting BTE operation and ROP function � REG[51h] Bit[3:0] = 2h */
    RA8875_Write(REGISTER_BECR1, 0xC2);
    
    /* Enable BTE function Bit 7*/
    RA8875_Write(REGISTER_BECR0, BECR0_ENABLE_BTE);
    
//    temporal = readState_RA8875() & STSR_BTE_BUSY;
    
    while ((readState_RA8875() & STSR_BTE_BUSY));
        
    return;
}

void bte_moveNegativeDirection (const BTE_t *ptrBTE)
{
    uint8_t temporal = 0;
        
    /* Setting source layer and address */
    temporal = (ptrBTE->source_x & 0xFF);
    RA8875_Write(REGISTER_HSBE0,  temporal);
    temporal = (ptrBTE->source_x & 0x0300) >> 8;
    RA8875_Write(REGISTER_HSBE1, temporal);
    
    temporal = (ptrBTE->source_y & 0xFF);
    RA8875_Write(REGISTER_VSBE0, temporal);
    temporal = (ptrBTE->source_y & 0x0300) >> 8;
    RA8875_Write(REGISTER_VSBE1, temporal);
    
    /* Setting destination layer and address */
    temporal = (ptrBTE->destination_x & 0xFF);
    RA8875_Write(REGISTER_HDBE0,  temporal);
    temporal = (ptrBTE->destination_x & 0x0300) >> 8;
    RA8875_Write(REGISTER_HDBE1, temporal);
    
    /* Setting destination position Y1 */
    temporal = (ptrBTE->destination_y & 0xFF);
    RA8875_Write(REGISTER_VDBE0, temporal);
    temporal = (ptrBTE->destination_y & 0x0300) >> 8;
    RA8875_Write(REGISTER_VDBE1, temporal);    
      
    /* Setting BTE width register 100*/ 
    temporal = (ptrBTE->width & 0xFF);
    RA8875_Write(REGISTER_BEWR0, temporal);
    temporal = (ptrBTE->width & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEWR1, temporal);

    /* Setting BTE height register 76*/
    temporal = (ptrBTE->height & 0xFF);
    RA8875_Write(REGISTER_BEHR0, temporal);
    temporal = (ptrBTE->height & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEHR1, temporal);
    
    /* Setting BTE operation and ROP function � REG[51h] Bit[3:0] = 3h */
    RA8875_Write(REGISTER_BECR1, 0xC3);
    
    /* Enable BTE function Bit 7*/
    RA8875_Write(REGISTER_BECR0, BECR0_ENABLE_BTE);
    
//    temporal = readState_RA8875() & STSR_BTE_BUSY;
    
    while ((readState_RA8875() & STSR_BTE_BUSY));
        
    return;
    
    
}

void bte_transparentWrite (const BTE_t *ptrBTE, uint16_t Color, const uint16_t *ptrData)
{
    uint8_t temporal = 0;
    uint16_t aux = 0;
  
    /* *************************************************************************
     * The suggested programming steps and registers setting are listed below as reference.
     * 
     * 1. Setting destination position � REG[58h], [59h], [5Ah], [5Bh]
     * 2. Setting BTE width register � REG[5Ch], [5Dh]
     * 3. Setting BTE height register � REG[5Eh], [5Fh]
     * 4. Setting Transparency Color ?Background Color � REG[63h], [64h], [65h]
     * 5. Setting BTE operation code and ROP Code � REG[51h] = C4h
     * 6. Enable BTE function � REG[50h] Bit7 = 1
     * 7. Write next image data
     * 8. Check STSR Bit7
     * 9. Repeat step 7, 8 until image data = block image data. Or Check STSR Bit6 
     * 
     * ************************************************************************/  

    /* Setting destination layer and address */
    temporal = (ptrBTE->destination_x & 0xFF);
    RA8875_Write(REGISTER_HDBE0,  temporal);
    temporal = (ptrBTE->destination_x & 0x0300) >> 8;
    RA8875_Write(REGISTER_HDBE1, temporal);
    
    /* Setting destination position Y1 */
    temporal = (ptrBTE->destination_y & 0xFF);
    RA8875_Write(REGISTER_VDBE0, temporal);
    temporal = (ptrBTE->destination_y & 0x0300) >> 8;
    RA8875_Write(REGISTER_VDBE1, temporal);       
    
    /* Setting BTE width register 100*/ 
    temporal = (ptrBTE->width & 0xFF);
    RA8875_Write(REGISTER_BEWR0, temporal);
    temporal = (ptrBTE->width & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEWR1, temporal);

    /* Setting BTE height register 76*/
    temporal = (ptrBTE->height & 0xFF);
    RA8875_Write(REGISTER_BEHR0, temporal);
    temporal = (ptrBTE->height & 0x0300) >> 8;
    RA8875_Write(REGISTER_BEHR1, temporal);    
    
    /* Set Color RGB565 */
    temporal = (Color & 0xF800) >> 11;
    RA8875_Write(0x63, temporal);                                               //Red 
    temporal = (Color & 0x07E0) >> 5;
    RA8875_Write(0x64, temporal);                                               //Green
    temporal = (Color & 0x001F);
    RA8875_Write(0x65, temporal);                                               //Blue
    
    /* Setting BTE operation and ROP function � REG[51h] = C4h */
    RA8875_Write(REGISTER_BECR1, 0xC4);
    
    /* Enable BTE function Bit 7*/
    RA8875_Write(REGISTER_BECR0, BECR0_ENABLE_BTE);
    
    escribirRegistro(COMAND_WRITE,REGISTER_MRWC);
    
    for (aux = 0; aux < 7679; aux++)
    {
        /* Write next image data */
        temporal = (ptrData[aux] & 0xFF00) >> 8;
        escribirRegistro(DATA_WRITE,temporal); 
        temporal = (ptrData[aux] & 0x00FF);
        escribirRegistro(DATA_WRITE,temporal);

    }
    /* Check STSR Bit7 */
    while(0 == isProcessDone(REGISTER_BECR0, STATUS_BTE_BUSY));
    
    return;
    
}