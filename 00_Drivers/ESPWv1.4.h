/*******************************************************************************
 * Nombre de archivo:   ESPWv1.3.h
 * Autor: Ing. Roberto D�vora Ochoa
 * Comentarios: 
 * - Esta librer�a funciona solo en m�quinas de estados.
 * - La funci�n de lectura EUSART debe ser capaz de funcionar por interrupci�n
 *    y por polling.  
 * - Si la m�quina de estados empieza a fallar, se debe dar m�s tiempo entre 
 *   cada env�o que se haga.
 * - Debe haber un tiempo entre la conexi�n con la red WiFi y la conexi�n con el
 *   servidor u otro dispositivo (TCP/UDP). Este tiempo no puede ser dado con 
 *   un delay puesto que la m�quina de estados debe seguir funcionando.
 * - Debe haber un tiempo entre la conexi�n con el servidor u otro dispositivo
 *   (TCP/UDP) y el primer env�o. Este tiempo no puede ser dado con un delay 
 *   puesto que la m�quina de estados debe seguir funcionando.
 * - Los macros usados para los timeouts se refieren a cantidades de TCYs, estos
 *   se deben modificar de acuerdo al Fosc con el que se est� trabajando. Por 
 *   regla general el INIT_TIMEOUT ser� mayor que el TASK_TIMEOUT.
 * - Cuando se use el modo SMARTCONFIG se requerir� de una applicaci�n para 
 *   Android, la cual proporciana el SSID y password al m�dulo ESP. Se usar� la 
 *   applicaci�n para conectar el m�dulo a un router WiFi.
 * Versi�n: 1.3
 * 
 * 
 */

#ifndef ESPW_H
#define	ESPW_H

// <editor-fold defaultstate="collapsed" desc="Condicionante para C++">
#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */
// </editor-fold>     

// <editor-fold defaultstate="collapsed" desc="Preprocesador">
#include <xc.h>
#include <stdint.h>
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="MACROS">
    
//Habilitar o deshabilitar c�digo
#define _ON                         1
#define _OFF                        0

//Macros relacionados con el tiempo
#define BUSY_TIME                   2               //Tiempo de espera para que el m�dulo pueda procesar lo que se le env�a
#define INIT_TIMEOUT                3000000         //Timeout de espera de respuesta durante inicializaci�n 
#define TASK_TIMEOUT                3000000         //Timeout de espera de respuesta durante la ejecuci�n de la tarea

//Macros para tama�os de buffers
#define SIZE_BUFFER_TX_COMMAND      70              //Este buffer no puede ser menor a 50
#define SIZE_BUFFER_RX_COMMAND      300             //Este buffer no puede ser menor a 50 normalmente o 60 cuando se use el smart config
#define SIZE_BUFFER_RX_DATA         40
#define SIZE_BUFFER_TX_DATA         40
    
#define NUMERO_DE_ENVIOS_EN_COLA    4               //Est� directamente relacionado con el tama�o del buffer TX de datos
    
//Strings para comparar
#define OK                          "OK\r"
#define READY                       "ready\r"
#define ERROR                       "ERROR\r"
#define CONEXION_ESTABLECIDA        "CONNECT\r"
#define CONEXION_0_ESTABLECIDA      "0,CONNECT\r"
#define CONEXION_1_ESTABLECIDA      "1,CONNECT\r"
#define CONEXION_2_ESTABLECIDA      "2,CONNECT\r"
#define CONECTADO_A_RED_WIFI        "WIFI CONNECTED\r"
#define IP_OBTENIDA                 "WIFI GOT IP\r"
#define ENVIO_CORRECTO              "SEND OK\r"
#define ENVIO_FALLIDO               "SEND FAIL\r"
#define CONEXION_0_CERRADA          "0,CLOSED\r"
#define CONEXION_1_CERRADA          "1,CLOSED\r"
#define CONEXION_2_CERRADA          "2,CLOSED\r"
#define SMART_CONECTED              "smartconfig connected wifi\r"
#define SMART_INITIED               "Smart get wifi info\r"
#define CONEXION_CON_MNI            "ssidESP_ESCLAVO\r"
#define MAX_INTENTOS                20

//Macros para la funci�n como cliente o servidor
#define CLIENT_MODE                 0               
#define SERVER_MODE                 1
    
//Habilitaci�n del smartconfig
#define SSID_PWD_FIXED              _OFF            //Si es _ON, el m�todo de conexi�n es proporcionando (en el c�digo) el SSID y contrase�a. Si es _OFF se utiliza una APP para proporcionar el SSID y contrase�a.

//Macros para el modo de operaci�n del m�dulo
#define STA_MODE                    1               //Modo estacionario (se conecta a una red WiFi)
#define ACCESSPOINT_MODE            2               //Modo access point (emite una red WiFi)
#define DUAL_MODE                   3               //Modo estacionario y access point al mismo tiempo

//Macros para el m�todo de encriptaci�n del access point
#define OPEN                        0               //Sin contrase�a
#define WPA_PSK                     2               //Contrase�a alfanum�rica
#define WPA2_PSK                    3               //Contrase�a alfanum�rica y cifrado AES
#define WPA_WPA2_PSK                4               //Combinaci�n de los dos m�todos anteriores

//Macros para el manejo de la respuesta de los commandos
#define SIN_RESPUESTA               0
#define RESPUESTA_OK                1

//Macros para determinar la cantidad de bytes recibidos (datos)
#define UNIDADES                    ESPW_BufferRX_Commands[indexUnits]       //unidades de bytes recibidos
#define DECENAS                     ESPW_BufferRX_Commands[indexTens]       //decenas de bytes recibidos
#define CENTENAS                    ESPW_BufferRX_Commands[indexHundreds]       //centenas de bytes recibidos
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Enums | Estructuras | Uniones">

//Estados de la tarea ESPW_task()
typedef enum 
{
    ESPW_IDLE = 0,
    ESPW_RESPUESTA,
    ESPW_TRANSMISION,
    ESPW_RETRANSMISION,
    ESPW_SEND,    
#if !SSID_PWD_FIXED
    ESPW_STARTSMART,
    ESPW_STOPSMART,
#endif
}estados_ESPW_t;

//Banderas
typedef struct
{
    //Primer byte de banderas
    
    uint8_t networkStatus           :1;                                         //Bandera de red recordada
    uint8_t smartconfig             :1;                                         //Bandera de bloqueo para que el smartconfig funcione correctamente
    uint8_t gotDataFlag             :1;                                         //Bandera de recepci�n de datos
    uint8_t initTimeout             :1;                                         //Bandera de inicio de timeout para el smartconfig    
    uint8_t envioPendiente          :4;                                         //Contador de env�os en cola
    
    //Segundo byte de banderas
    
    uint8_t envioSiguiente          :1;                                         //Bandera de siguiente env�o
    uint8_t error                   :1;                                         //Bandera de comando erroneo o mal entregado    
    uint8_t envioTerminado          :1;                                         //Bandera que indica cuando se ha terminado un env�o
    uint8_t rcvingData              :1;                                         //Bandera para guardar los bytes recibidos en el buffer en el buffer de datos
    uint8_t taskOn                  :1;                                         //Bandera que cambia el comportamiento de algunas funciones para que trabajen correctamente en m�quinas de estados.
    uint8_t MultipleConections      :1;                                         //Bandera que indica el numero de conexiones aceptadas.
    uint8_t connectionStatus        :2;                                         //Bandera de conexi�n UDP/TCP 
    
    //Tercer byte de banderas
    uint8_t fatal_error             :1;                                         //Bandera que indica que hubo un error fatal y hay que reiniciar el m�dulo
    uint8_t conexion_MNI            :1;                                         //Bandera que indica que se conect� a un hotspot proveido por un MNI
    
}ESPW_flags_t;

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Prototipos de Funciones"> 

/*******************************************************************************
* @Description
* Funcion que registra al controlador USART, en especifico  las funciones de
* lectura y escritura 
* @Preconditions
* Haber configurado los parametros para controlador USART del MCU
* @Param
* void (*functionWritePtr)(uint8_t)   -Puntero a funci�n de escritura
* 
* uint8_t (*functionReadPtr)(void)    -Puntero a funci�n de lectura
* @Returns
* Ninguno
* @Example
* #include <xc.h>
* #include <"eusart.h">
*  
* ESPW_UART_Handler(EUSART1_WriteInterrupt, EUSART_Read1);
* *****************************************************************************/
void ESPW_UART_Handler(void (*ptrWriteFunction)(const uint8_t), uint8_t (*ptrReadFunction)(void)/*, uint8_t *RegistroRX, uint8_t BitRX*/);

/*******************************************************************************
* @Description
* Funcion que registra el pin donde se controlar� el la habilitaci�n del M�dulo.
* 
* @Preconditions
* Ninguna
* 
* @Param
* Ninguno
* 
* @Returns
* Ninguno
 * 
* @Example
* #include <xc.h>
* #include <"eusart.h">
*  
* ESPW_sleepBitRegister (&PORTB, 0b00001000);
* *****************************************************************************/
void ESPW_sleepBitRegister (uint8_t *Puerto, uint8_t Bit);

/*******************************************************************************
 *@Description
 * Funci�n que ajusta el baudrate de la comunicaci�n UART. Esta configuraci�n
 * es guardada por defecto.
 * 
 *@Preconditions
 * Haber registrado las funciones de lectura y escritura de UART
 *
 *@Param
 * baudrate     - Velocidad de baudrate al que se quiere establecer comunicaci�n
 *
 *@Returns      
 * Ninguno
 *
 *@Example
 * ESPW_setUART_Speed("9600");
 * 
 *@Comments
 * El m�dulo ESP32 se puede ajustar a un baudrate custom en un rango de
 * 80 - 5000000. Sin embargo, se recomienda elegir un baudrate comunmente usado
 * as� como 9600 o 115200.
 *
 ******************************************************************************/
void ESPW_setUART_Speed(uint8_t *baudrate);

/*******************************************************************************
 *@Description
 * Funci�n que le asigna una MAC address personalizada al m�dulo, esta MAC es la
 * usada en el modo estacionario.
 *
 *@Preconditions
 * Haber registrado las funciones de lectura y escritura de UART
 *
 *@Param
 * MAC          - Este debe se un string que contenga la direcci�n MAC en el 
 *                formato correspondiente.
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_setMAC_Address("00:00:00:12:34:56");
 *
 *@Comments
 * Las MAC 00:00:00:00:00:00 y FF:FF:FF:FF:FF:FF son inv�lidas. No se debe asig-
 * nar la misma MAC al modo estacionario y al modo AP. El bit 0 de la MAC NO 
 * puede ser 1. For example, a MAC address can be "1a:?" but not "15:?".
 *
 ******************************************************************************/
void ESPW_setMAC_Address(uint8_t *MAC);

/*******************************************************************************
 *@Description
 * Funci�n que asigna el n�mero de medidor, �nicamente utilizada en los Medidores
 * de Nivel (MNI, MNA, MNT).
 *
 *@Preconditions
 * Haber registrado las funciones de lectura y escritura de UART
 *
 *@Param
 * No_medidor       - Es un n�mero entero positivo no mayor a 0xFFFFFF
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_setNoMedidor(15);  
 *
 ******************************************************************************/
void ESPW_setNoMedidor(uint32_t No_medidor);

/*******************************************************************************
 *@Description
 * Funci�n que resetea el m�dulo ESP
 * 
 *@Preconditions
 * Haber usado la funci�n ESPW_test() para comprobar la comunicaci�n con el m�dulo
 * 
 *@Param
 * Ninguno
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_Reset();
 *
 ******************************************************************************/
static void ESPW_Reset (void);

void ESPW_Restore (void);

/*******************************************************************************
 *@Description
 * Funci�n que comprueba la respuesta del m�dulo ESP32 a trav�s de UART
 *
 *@Preconditions
 * Haber registrado las funciones de lectura y escritura UART
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_tes();
 *
 ******************************************************************************/
static void ESPW_test(void);

#if SSID_PWD_FIXED
/*******************************************************************************
 *@Description
 * Funci�n que inicializa el m�dulo ESP32 para su uso en WiFi
 *
 *@Preconditions
 * Uso de la funci�n ESPW_test() para comprobar que el m�dulo responde
 *
 *@Param
 * WiFiConfig        -Establece la operaci�n como router (ACCESSPOINT), como 
 *                       dispositivo que se conecta a una red (STA) o ambos al 
 *                       mismo tiempo(DUAL).
 * SSID                 -Nombre de la red a la que se va a conectar
 * PWD                  -Contrase�a de la red a la que se va a conectar
 * cliente_servidor     -Elige entre funcionar como cliente o servidor
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_wifiInit(STA_MODE,"Pyansa","password",CLIENT_MODE);
 * 
 *@Comments
 * -Si se escoje el modo ACCESSPOINT o el modo DUAL se tendr� que configurar el 
 *  m�dulo usando la funci�n ESPW_configAccessPoint();
 * -Si se requiere que se conecte a diferentes redes WiFi, sin necesidad de re-
 *  programar el m�dulo cambie el macro SSID_PWD_FIXED a _OFF.
 * -Una vez conectado a una red WiFi, �sta se recordar�. Si se requiere conectar
 *  a otra red, ser� necesario apagar la red anterior e introducir los datos de
 *  la nueva red.
 *
 ******************************************************************************/
void ESPW_wifiInit(uint8_t WiFiConfig, uint8_t *SSID, uint8_t *PWD, uint8_t cliente_servidor);
#else
/*******************************************************************************
 *@Description
 * Funci�n que inicializa el m�dulo ESP32 para su uso en WiFi
 *
 *@Preconditions
 * Registro de las funciones de lectura y escritura UART
 *
 *@Param
 * WiFiConfig        -Establece la operaci�n como router (ACCESSPOINT), como 
 *                       dispositivo que se conecta a una red (STA) o ambos al 
 *                       mismo tiempo(DUAL). 
 * cliente_servidor     -Elige entre funcionar como cliente o servidor
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_wifiInit(STA_MODE,CLIENT_MODE);
 * 
 *@Comments
 * -Si se escoje el modo ACCESSPOINT o el modo DUAL se tendr� que configurar el 
 *  m�dulo usando la funci�n ESPW_configAccessPoint();
 * -Si se requiere que se conecte a una red WiFi predeterminada, cambie el macro
 *  SSID_PWD_FIXED a _ON.
 * -Una vez conectado a una red WiFi, �sta se recordar�. Si se requiere conectar
 *  a otra red, ser� necesario apagar la red anterior e introducir los datos de
 *  la nueva red.
 *
 ******************************************************************************/
void ESPW_wifiInit(uint8_t WiFiConfig, uint8_t cliente_servidor);
#endif

/*******************************************************************************
 *@Description
 * Funci�n que configura al m�dulo ESP32 como cliente o servidor, es decir, que
 * acepte multiples conexiones (servidor) o una sola (cliente)
 * 
 *@Preconditions
 * Haber registrado las funciones de lectura y escritura UART
 *
 *@Param
 * config       -macro de configuraci�n (CLIENT_MODE o SERVER_MODE)
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_ClientServerMode (SERVER_MODE);
 *
 ******************************************************************************/
static void ESPW_ClientServerMode (uint8_t config);

/*******************************************************************************
 *@Description
 * Funci�n que configura el m�dulo ESP32 para funcionar como ACCESSPOINT, STA o
 * DUAL
 *
 *@Preconditions
 * Haber registrado las funciones de lectura y escritura UART
 *
 *@Param
 * config       -macro de configuraci�n (STA_MODE, ACCESSPOINT_MODE o DUAL_MODE)
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_WiFiConfig(DUAL_MODE);
 *
 ******************************************************************************/
void ESPW_WiFiConfig(uint8_t config);

/*******************************************************************************
 *@Description
 * Funci�n que configura la red que emite el m�dulo ESP32 en modo ACCESSPOINT o
 * DUAL
 *
 *@Preconditions
 * Haber inicializado el m�dulo en modo ACCESSPOINT O DUAL
 *
 *@Param
 * SSID             -Nombre de la red que va a emitir el m�dulo
 * PWD              -Contrase�a para que otro dispositivo se conecte a dicha red 
 * channel          -Canal en el que el m�dulo va a emitir la red
 * EncryptoinMethod -M�todo de encriptaci�n a usar en la red
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_configAccessPoint("Pyansa","password","11",WPA_WPA2_PSK);
 *
 ******************************************************************************/
void ESPW_configAccessPoint(uint8_t *SSID, uint8_t *PWD, uint8_t *channel, uint8_t EncryptionMethod);

#if SSID_PWD_FIXED
/*******************************************************************************
 *@Description
 * Funci�n que conecta el m�dulo ESP32 a una red WiFi conocida
 *
 *@Preconditions
 * Haber inicializado el m�dulo como STA o DUAL
 *
 *@Param
 * SSID     -Nombre de la red a la que se va a conectar
 * PSWD     -Contrase�a de la red a la que se va a conectar
 *
 *@Returns
 * 1        -Conexi�n exitosa
 * 0        -Conexi�n fallida
 *
 *@Example
 * ESPW_ConectarARedWiFi("Pyansa","password");
 *
 ******************************************************************************/
void ESPW_ConectarARedWiFi(uint8_t *SSID,uint8_t *PSWD);
#endif

/*******************************************************************************
 *@Description
 * Funci�n que establece una conexi�n TCP/IP ya sea directamente con una IP o
 * con un DNS
 *
 *@Preconditions
 * Haberse conectado a una red, que el dispositivo/DNS est� disponible a la red
 * y caso de ser necesario conexi�n a internet
 *
 *@Param
 * IP       -IP o servicio DNS con el que se quiere conectar
 * Port     -Puerto que se usar� para la conexi�n
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_EstablishTCPConection("www.google.com","80");
 * 
 * *@Comments
 * Esta funci�n tambi�n puede ser utilizada despu�s de la inicializaci�n, en 
 * caso de que la conexi�n sea cerrada 
 *
 ******************************************************************************/
void ESPW_EstablishTCPConection(uint8_t *NoConection, uint8_t *IP, uint8_t *Port);

/*******************************************************************************
 *@Description
 * Funci�n que establece una conexi�n UDP ya sea directamente con una IP o con 
 * un DNS
 *
 *@Preconditions
 * Haberse conectado a una red, que el dispositivo/DNS est� disponible a la red
 * y caso de ser necesario conexi�n a internet
 *
 *@Param
 * IP           -IP o servicio DNS con el que se quiere conectar
 * SendPort     -Puerto al que se enviar�n los datos
 * RecvPort     -Puerto que por el que se recibir�n los datos
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_EstablishUDPConection("www.google.com","80");
 *
 * *@Comments
 * Esta funci�n tambi�n puede ser utilizada despu�s de la inicializaci�n, en
 * caso de que la conexi�n sea cerrada.
 *
 ******************************************************************************/
void ESPW_EstablishUDPConection(uint8_t *NoConection, uint8_t *IP, uint8_t *SendPort, uint8_t *RecvPort);

/*******************************************************************************
 *@Description
 * Funci�n que se encarga de analizar la respuesta del m�dulo, �sta se ejecuta 
 * durante la interrupci�n para no perder ning�n byte
 *
 *@Preconditions
 * Haber habilitado la interrupci�n e incluir la funci�n en el vector de 
 * interrupci�n correspondiente
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * 
 * void Interrupt ISR (void)
 * {
 *   if (PIR1bits.RC1IF == 1) 
 *   {
 *      ESPW_ReadAnswer();
 *   }
 * }
 *
 ******************************************************************************/
void ESPW_ReadAnswer(void);

/*******************************************************************************
 *@Description
 * Funci�n que pone al m�dulo en modo de bajo consumo (sleep) por tiempo 
 * indeterminado.
 *
 *@Preconditions
 * Haber registrado el puerto y bit con el que se controlar� el pin Enable del
 * m�dulo ESP32.
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_goToSleep();
 *
 ******************************************************************************/
void ESPW_goToSleep(void);

/*******************************************************************************
 *@Description
 * Funci�n que pone al m�dulo en modo de operaci�n normal (lo despierta).
 *
 *@Preconditions
 * Haber registrado el puerto y bit con el que se controlar� el pin Enable del
 * m�dulo ESP32.
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * 
 * ESPW_wakeUp();
 *
 ******************************************************************************/
void ESPW_wakeUp(void);

/*******************************************************************************
 *@Description
 * Funci�n que establece la longitud del mensaje a enviar
 *
 *@Preconditions
 * La tarea ESPW_task() debe estar incluida en el ciclo infito(while(1)) del 
 * archivo main
 *
 *@Param
 * len      -Longitud en bytes del mensaje a enviar
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_setMsgLenght(20);
 * 
 *@Comments
 * La longitud no debe ser mayor a 99
 *
 ******************************************************************************/
void ESPW_setMsgLenght(uint8_t len, uint8_t ID);

/*******************************************************************************
 *@Description
 * Funci�n que maneja la tarea ESPW_task() asignandole un estado
 *
 *@Preconditions
 * La tarea ESPW_task() debe estar incluida en el ciclo infito(while(1)) del 
 * archivo main
 *
 *@Param
 * state    -Estado a asignar
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_setState(ESPW_SEND);
 * 
 *@Comments
 * La librer�a est� pensada para que el usuario establezca solo el estado 
 * ESPW_SEND
 *
 ******************************************************************************/
void ESPW_setState(uint8_t state);

/*******************************************************************************
 *@Description
 * Funci�n que obtiene el estado actual de la tarea ESPW_task()
 *
 *@Preconditions
 * La tarea ESPW_task() debe estar incluida en el ciclo infito(while(1)) del 
 * archivo main
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * El estado actual de la tarea (vea la secci�n de enums)
 *
 *@Example
 * uint8_t Aux = 0;
 * Aux = ESPW_getState();
 *
 ******************************************************************************/
uint8_t ESPW_getState(void);

/*******************************************************************************
 *@Description
 * Funci�n que establece el valor de la bandera de recepci�n de datos
 *
 *@Preconditions
 * Tarea ESPW_task en el ciclo infito del archivo main y funci�n ESPW_ReadAnswer
 * en el vector de interrupci�n necesario
 *
 *@Param
 * 1    -Activa la bandera de recepci�n de datos
 * 0    -Desactiva la bandera de recepci�n de datos
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_setRX_Status(0);
 * 
 *@Comments
 * Por lo general el usuario s�lo necesitar� desactivar la bandera
 *
 ******************************************************************************/
void ESPW_setRX_Status(uint8_t status);

/*******************************************************************************
 *@Description
 * Funci�n que obtiene el valor de la bandera de recepci�n de datos
 *
 *@Preconditions
 * Tarea ESPW_task en el ciclo infito del archivo main y funci�n ESPW_ReadAnswer
 * en el vector de interrupci�n necesario
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * 1        -Bandera activa
 * 0        -Bandera inactiva
 *
 *@Example
 * 
 * if(ESPW_getRX_Status())
 * {
 *     ESPW_setRX_Status(0);
 *     //Your code
 * }
 *
 ******************************************************************************/
uint8_t ESPW_getRX_Status(void);

/*******************************************************************************
 *@Description
 * Funci�n que devuelve un byte espec�fico del buffer de recepci�n de datos
 *
 *@Preconditions
 * Haber recibido datos
 *
 *@Param
 * position     -Posici�n del dato que se desea obtener
 *
 *@Returns
 * Un byte recibido
 *
 *@Example
 * if(ESPW_getRX_Status())
 * {
 *     uint8_t Aux = 0;
 *     ESPW_setRX_Status(0);
 * 
 * 
 *     Aux = ESPW_getBufferAtPosition(0);
 * }
 *
 ******************************************************************************/
uint8_t ESPW_getBufferAtPosition(uint8_t position);

/*******************************************************************************
 *@Description
 * Funci�n que escribe un byte en el buffer de transmisi�n de datos
 *
 *@Preconditions
 * Inicializaci�n del m�dulo, conexi�n con un dispotivo y tarea ESPW_task() en 
 * el ciclo infinito del archivo main
 *
 *@Params
 * data         -byte de informaci�n a escrbir 
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * ESPW_setBufferAtPosition(15);
 * 
 *
 ******************************************************************************/
void ESPW_setBuffer(uint8_t data);

/*******************************************************************************
 *@Description
 * Funci�n que devuelve el estado de la conexi�n con el Servidor/Dispositivo
 *
 *@Preconditions
 * Ninguna
 *
 *@Params
 * Ninguno
 *
 *@Returns
 * - Una bandera (uint8_t) que es 1 cuando la conexi�n se encuentra estableci-
 * da, y es 0 cuando la conexi�n no se encuentra establecida.
 *
 *@Example
 * uint8_t Aux;
 * Aux = ESPW_getConnectionStatus();
 *
 ******************************************************************************/
uint8_t ESPW_getConnectionStatus(void);

/*******************************************************************************
 *@Description
 * Funci�n que devuelve el estado de la conexi�n a la red WiFi
 *
 *@Preconditions
 * Ninguna
 *
 *@Params
 * Ninguno
 *
 *@Returns
 * - Una bandera (uint8_t) que es 1 cuando la conexi�n se encuentra estableci-
 * da, y es 0 cuando la conexi�n no se encuentra establecida.
 *
 *@Example
 * uint8_t Aux;
 * Aux = ESPW_getNetworkStatus();
 *
 ******************************************************************************/
uint8_t ESPW_getNetworkStatus(void);

/*******************************************************************************
 *@Description
 * Funci�n que retorna un 1 cuando se hubieron completados todos los env�os 
 * puestos en cola.
 *
 *@Preconditions
 * Ninguno
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * uint8_t      - Bandera que indica el fin de los env�os en puestos en cola
 *
 *@Example
 * if (ESPW_getTX_Status()) {
 *     ESPW_setTX_Status(0);            
 *     ESPW_goToSleep();                        
 * }
 * 
 *@Comments
 * Se necesita usar la funci�n ESPW_setTX_Status para bajar esta la bandera de
 * fin de env�o, una vez que esta llega a 1.
 *
 ******************************************************************************/
uint8_t ESPW_getTX_Status(void);

/*******************************************************************************
 *@Description
 * Funci�n que escribe un valor (1 o 0) a la bandera de fin de env�os.
 *
 *@Preconditions
 * Ninguno
 *
 *@Param
 * status       - Es el estado en que se desea colocar la bandera, generalmente
 *                se usar� 0.
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * if (ESPW_getTX_Status()) {
 *     ESPW_setTX_Status(0);            
 *     ESPW_goToSleep();                        
 * }
 *
 ******************************************************************************/
void ESPW_setTX_Status(uint8_t status);

/*******************************************************************************
 *@Description
 * Funci�n que retorna el n�mero de bytes recibidos en una transmisi�n entrante
 *
 *@Preconditions
 * Ninguno
 *
 *@Param 
 *
 *@Returns
 * uint8_t      -N�mero de bytes que se recibieron en una transmisi�n entrante
 *
 *@Example
 * for(uint8_t x = 0; x<ESPW_getRcvBytes();x++){
 *      buffer[x] = ESPW_getBufferAtPosition(x);
 * }
 *
 ******************************************************************************/
int8_t ESPW_getRcvdBytes(void);

void ESPW_setError(uint8_t set_clear);

uint8_t ESPW_getError(void);

uint8_t ESPW_getNetworkID(void);

//Funciones que el usuario no deber�a modificar ni usar
static uint8_t ESPW_compare(const uint8_t *string);
static void ESPW_putToBuffer(uint8_t *Destino,uint8_t *String);
static void ESPW_clearBuffer(uint8_t *Buffer);
static void ESPW_concatenar(uint8_t *Hacia, uint8_t *Desde, uint8_t tamannioHacia);
static void ESPW_WriteRawBuffer(void);
static void ESPW_WriteString(const uint8_t *ptrTXdata);
static void ESPW_recorreBuffer(void);
static uint8_t ESPW_waitFor(const uint8_t *string);

/*******************************************************************************
 *@Description
 * Tarea que env�a comandos, env�a datos, recibe datos y en general maneja al 
 * m�dulo ESP32 para un �ptimo funcionamiento
 *
 *@Preconditions
 * Haber inicializado el m�dulo como cliente y haberlo establecido una conexi�n
 * con alg�n dispositivo
 *
 *@Param
 * Ninguno
 *
 *@Returns
 * Ninguno
 *
 *@Example
 * #include "Include/ESPW.h"
 * 
 * void main (void)
 * {
 *      while (1)
 *      {
 *       ESPW_task();
 *      }
 * }
 *
 ******************************************************************************/
void ESPW_task(void);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Condicionante para C++">
#ifdef	__cplusplus
}
#endif /* __cplusplus */
// </editor-fold>

#endif	/* XC_HEADER_TEMPLATE_H */

