/* 
* File:            RA8875.h
* Author:          Ing. Carlos Tostado
* Date:            28 de Julio 2017
* PIC:             PIC18F4xK22
* Versi�n:         1.0
 * 
*/

#ifndef RA8875_H
#define	RA8875_H

/* *********************** FORMATO DE PULSOS **********************************
 *
 * Se envian 16 bit por paquete, los primeros 8 solo obedecen al tipo de dato 
 * a ser transmitido, los 8 siguientes sera la informaci�n a transmitir
 * 
 *                 Tipo de comando     Informacion
 *                ---- 8 bits -----   ---- 8 bits ----
 *                RS RW 0 0 0 0 0 0   1 0 1 0 1 0 1 0 
 *                
 *                RS = 0 DATA            RW = 0 WRITE
 *                RS = 1 COMANDO/ESTADO  RW = 1 READ
 *  
 * ****************************************************************************/

#include <xc.h>
#include <stdint.h>
#include <stdio.h>
#include "config.h"
#include "timers.h"
#include "eusart.h"

//Macros para el envio de datos
#define COMAND_WRITE               0x80
#define STATUS_READ                0xC0
#define DATA_WRITE                 0x00
#define DATA_READ                  0x40

//Configuraci�n Pines de control
#define configReset_PIN()          TRISEbits.RE1 = 0
#define configWait_PIN()           

//Pines de control
#define RA8875_RESET               PORTEbits.RE1               
#define RA8875_WAIT                PORTAbits.RA7
#define RA8875_INT

// Colores (RGB565)
#define	RA8875_BLACK               0x0000
#define	RA8875_BLUE                0x001F
#define	RA8875_RED                 0xF800
#define	RA8875_GREEN               0x07E0
#define RA8875_CYAN                0x07FF
#define RA8875_MAGENTA             0xF81F
#define RA8875_YELLOW              0xFFE0  
#define RA8875_WHITE               0xFFFF
#define RA8875_PURPLE              0x6019

//Macros para los registros System and Configuration (0x01,0x02,0x04,0x10 - 0x1F)
#define REGISTER_PWRR              0x01                                         // Power and Display Control Register
#define REGISTER_MRWC              0x02                                         // Memory Read/Write Command
#define REGISTER_PCS               0x04                                         // Pixel Clock Setting Register 
#define REGISTER_SYS               0x10                                         // System Configuration Register
#define REGISTER_HDW               0x14                                         // LCD Horizontal Display Width Register
#define REGISTER_HNDFTR            0x15                                         // Horizontal Non-Display Period Fine Tuning Option Register
#define REGISTER_HNDR              0x16                                         // LCD Horizontal Non-Display Period Register
#define REGISTER_HSTR              0X17                                         // HSYNC Start Position RegisteR
#define REGISTER_HPWR              0x18                                         // HSYNC Pulse Width Register
#define REGISTER_VDH0              0x19                                         // LCD Vertical Display Height Register
#define REGISTER_VDH1              0x1A                                         // LCD Vertical Display Height Register0 
#define REGISTER_VND0              0X1B                                         // LCD Vertical Non-Display Period Register          
#define REGISTER_VND1              0x1C                                         // LCD Vertical Non-Display Period Register
#define REGISTER_VST0              0x1D                                         // VSYNC Start Position Register 0
#define REGISTER_VST1              0x1E                                         // VSYNC Start Position Register 1
#define REGISTER_VPW               0x1F                                         // VSYNC Pulse Width Register

//Macros para los registros de control LCD Display (0x20 - 0x29)
#define REGISTER_DPCR              0x20                                         // Display Configuration Register (DPCR)
#define REGISTER_FNCR0             0x21                                         //  Font Control Register 0 (FNCR0) 
#define REGISTER_FNCR1             0x22
#define REGISTER_CGSR              0x23                                         // CGRAM Select Register (CGSR) 
#define REGISTER_F_CURXL           0x2A                                         // Font Write Cursor Horizontal Position Register 0
#define REGISTER_F_CURXH           0x2B                                         // Font Write Cursor Horizontal Position Register 1 
#define REGISTER_F_CURYL           0x2C                                         // Font Write Cursor Vertical Position Register 0
#define REGISTER_F_CURYH           0x2D                                         // Font Write Cursor Vertical Position Register 1 (F_CURYH)

//Macros para los registros Active Window & Scroll Window (0x30 - 0x3F)
#define REGISTER_HSAW0             0x30                                         // Horizontal Start Point 0 of Active Window
#define REGISTER_HSAW1             0x31                                         // Horizontal Start Point 1 of Active Window
#define REGISTER_VSAW0             0x32                                         // Vertical Start Point 0 of Active Window
#define REGISTER_VSAW1             0x33                                         // Vertical Start Point 1 of Active Window
#define REGISTER_HEAW0             0x34                                         // Horizontal End Point 0 of Active Window
#define REGISTER_HEAW1             0x35                                         // Horizontal End Point 1 of Active Window
#define REGISTER_VEAW0             0x36                                         // Vertical End Point of Active Window 0
#define REGISTER_VEAW1             0x37                                         // Vertical End Point of Active Window 1

//Macros para los registros Cursor Setting Registers ()
#define REGISTER_MWCR0             0x40                                         // Memory Write Control Register 0
#define REGISTER_MWCR1             0x41                                         // Memory Write Control Register 1 
#define REGISTER_CURH0             0x46
#define REGISTER_CURH1             0x47
#define REGISTER_CURV0             0x48
#define REGISTER_CURV1             0x49

// Macros para los registros Block Transfer Engine(BTE)
#define REGISTER_BECR0             0x50                                         // BTE Function Control Register 0
#define REGISTER_BECR1             0x51                                         // BTE Function Control Register1 
#define REGISTER_LTPR0             0x52                                         // Layer Transparency Register0
#define REGISTER_LTPR1             0x53                                         // Layer Transparency Register1 (LTPR1)
#define REGISTER_HSBE0             0x54                                         // Horizontal Source Point 0 of BTE
#define REGISTER_HSBE1             0x55                                         // Horizontal Source Point 1 of BTE
#define REGISTER_VSBE0             0x56                                         // Vertical Source Point 0 of BTE (VSBE0)
#define REGISTER_VSBE1             0x57                                         // Vertical Source Point 1 of BTE (VSBE0)
#define REGISTER_HDBE0             0x58                                         // Horizontal Destination Point 0 of BTE 
#define REGISTER_HDBE1             0x59                                         // Horizontal Destination Point 1 of BTE 
#define REGISTER_VDBE0             0x5A                                         // Vertical Destination Point 0 of BTE
#define REGISTER_VDBE1             0x5B                                         // Vertical Destination Point 1 of BTE
#define REGISTER_BEWR0             0x5C                                         // BTE Width Register 0 (BEWR0) 
#define REGISTER_BEWR1             0x5D                                         // BTE Width Register 1 (BEWR0) 
#define REGISTER_BEHR0             0x5E                                         // BTE Height Register 0 (BEHR0) 
#define REGISTER_BEHR1             0x5F                                         // BTE Height Register 1 (BEHR0) 


//Macros para los registros PWM Control Registers (0x8A - 0x8E)
#define REGISTER_P1CR              0x8A                                         // PWM1 Control Register
#define REGISTER_P1DCR             0x8B                                         // PWM1 Duty Cycle Register
#define REGISTER_MCLR              0x8E                                         // Memory Clear Control Register


//Macros para configurar registros PLL (0x88 - 0x89)
#define REGISTER_PLLC1             0x88
#define REGISTER_PLLC2             0x89

//Macros para el control de registros de dibujo (0x90 - 0xAC)
#define REGISTER_DCR               0x90
#define REGISTER_DLHSR0            0x91                                         // Draw Line/Square Horizontal Start Address Register0 (DLHSR0) [7:0]
#define REGISTER_DLHSR1            0x92                                         // Draw Line/Square Horizontal Start Address Register1 (DLHSR1) [9:8]
#define REGISTER_DLVSR0            0x93                                         // Draw Line/Square Vertical Start Address Register0 (DLVSR0) [7:0]
#define REGISTER_DLVSR1            0x94                                         // Draw Line/Square Vertical Start Address Register1 (DLVSR1) [9:8]
#define REGISTER_DLHER0            0x95                                         // Draw Line/Square Horizontal End Address Register0 (DLHER0)
#define REGISTER_DLHER1            0x96                                         // Draw Line/Square Horizontal End Address Register1 (DLHER1)
#define REGISTER_DLVER0            0x97                                         // Draw Line/Square Vertical End Address Register0 (DLVER0)
#define REGISTER_DLVER1            0x98                                         // Draw Line/Square Vertical End Address Register1 (DLVER1)
#define REGISTER_DCHR0             0x99                                         // Draw Circle Center Horizontal Address Register0 (DCHR0)
#define REGISTER_DCHR1             0x9A                                         // Draw Circle Center Horizontal Address Register1 (DCHR1) [9:8]
#define REGISTER_DCVR0             0x9B                                         // Draw Circle Center Vertical Address Register0 (DCVR0) [7:0]
#define REGISTER_DCVR1             0x9C                                         // Draw Circle Center Vertical Address Register1 (DCVR1) [8]
#define REGISTER_DCRR              0x9D                                         // Draw Circle Radius Register (DCRR)

#define REGISTER_DTPH0             0xA9                                         // Draw Triangle Point 2 Horizontal Address Register0 (DTPH0)
#define REGISTER_DTPH1             0xAA                                         // Draw Triangle Point 2 Horizontal Address Register1 (DTPH1)
#define REGISTER_DTPV0             0xAB                                         // Draw Triangle Point 2 Vertical Address Register0 (DTPV0)
#define REGISTER_DTPV1             0xAC                                         // Draw Triangle Point 2 Vertical Address Register1 (DTPV1)

// Macros para configurar registros de control KEY & IO  (0xC0 - 0xC7)
#define REGISTER_GPIOX             0xC7

/******************************************************************************/

//bits para configuracion de los registros
#define PWRR_LCD_ON                0x80
#define PWRR_LCD_OFF               0x00
#define PWRR_LCD_SLEEP             0x02
#define PWRR_LCD_NORMAL            0x00
#define PWRR_SOFT_RESET            0x01

#define MWCR0_GFXMODE              0x00
#define MWCR0_TXTMODE              0x80
#define MWCR0_CURSOR_VISIBLE       0x40
#define MWCR0_CURSOR_NONVISIBLE    0x00
#define MWCR0_CURSOR_BLINK         0x20
#define MWCR0_CURSOR_NOTBLINK      0x00
#define MWCR0_MEMORY_AUTOINCREASE  0x00
#define MWCR0_MEMORY_NONINCREASE   0x02

#define MWCR1_EN_GRAPHICS_CUR      0x80
#define MWCR1_DIS_GRAPHICS_CUR     0x00
#define MWCR1_LAYER_DESTINATION    0x00
#define MWCR1_CGRAM_DESTINATION    0x04
#define MWCR1_CURSOR_DESTINATION   0x08

#define FNCR0_CGRAM_FONT           0x80
#define FNCR0_CGROM_FONT           0x00
#define FNCR0_EXTERNAL_FONT        0x20
#define FNCR0_INTERNAL_FONT        0x00

#define FNCR1_FONT_TRASNPARENCY    0X40 

#define MCLR_MEMORY_CLEAR          0x80
#define MCLR_FULL_WINDOW           0x00
#define MCLR_ACTIVE_WINDOW         0x40

#define P1CR_PWM_ENABLE            0x80
#define P1CR_PWM_DISABLE           0x00
#define P1CR_CLKOUT                0x10
#define P1CR_PWMOUT                0x00

#define SYSR_8BPP                  0x00
#define SYSR_16BPP                 0x08
#define SYSR_8Bit                  0x00
#define SYSR_16Bit                 0x02

#define DPCR_OneLayer              0x00
#define DPCR_TwoLayer              0x80

#define DCR_StartDrawing           0x80
#define DCR_StopDrawing            0x00
#define DCR_StartCircleDrawing     0x40
#define DCR_StopCircleDrawing      0x00
#define DCR_Fill                   0x20
#define DCR_NonFill                0x00
#define DCR_DrawSquare             0x10
#define DCR_DrawLine               0x00
#define DCR_DrawTriangle           0x01

#define BECR0_ENABLE_BTE           0x80

/******************************************************************************/

//Macros para realizar los Status de los procesos
#define STATUS_DCR_LST             0x80                                         //Line/Circle/Square Status
#define STATUS_DCR_CIRCLE          0x40                                         //ine/Circle/Square Status
#define STATUS_MCLR                0x80
#define STATUS_BTE_BUSY            0x80
#define STSR_BTE_BUSY              0x40

#define MODE_GRAPHICS              0x00
#define MODE_TEXT                  0x01

/******************************************************************************/

//Estructuras y uniones

typedef struct 
{
    uint8_t  radio;                                                             // Radio del circulo
    uint8_t  fill                   :  1;                                       // Bandera que indica si sera relleno o no
    uint16_t x1                     ;                                           // Posicion horizontal 1  (Line, Square, Tiriangle)
    uint16_t y1                     ;                                           // Posicion vertical   1  (Line, Square, Tiriangle)
    uint16_t x2                     ;                                           // Posicion horizontal 2  (Line, Square, Tiriangle)
    uint16_t y2                     ;                                           // Posicion verticla   2  (Line, Square, Tiriangle)
    uint16_t x3                     ;                                           // Posicion horizontal 3  (Tiriangle)
    uint16_t y3                     ;                                           // Posicion vertical   3  (Tiriangle)
    uint16_t Color;                                                             // Color del objeto RGB565
}RA8875_Shape;

typedef struct 
{
    uint16_t destination_x;                                                     //Horizontal position of the destination
    uint16_t destination_y;                                                     //Vertical position of the destination
    uint16_t source_x;                                                          //Horizontal position of the source
    uint16_t source_y;                                                          //Vertical position of the source    
    uint16_t width;                                                             //set the width of the picture
    uint16_t height;                                                            //set the height of the picture
}BTE_t;

typedef union 
{
    uint32_t colorRGB;
    
    struct 
    {
        uint8_t Blue;
        uint8_t Green;
        uint8_t Red;
    };
    
}RGB_Color_t;

typedef struct 
{
    void (*enviarDatos)(uint8_t, uint8_t);                                      // void RA8875_Write (uint8_t Register, uint8_t Data);
    void (*init)(void);                                                         // void init_RA8875 (void);
    void (*enciendeDisplay)(void);                                              // void powerOnDisplay (void);
    void (*apagaDisplay) (void);                                                // void powerOffDisplay (void);
    void (*limpiaDisplay)(void);                                                // void clearWindow (void);
    void (*ajustaBrillo) (uint8_t);                                             // void setBrightness (uint8_t brightness);
    void (*colocarCursor)(uint16_t, uint16_t);                                  // void setCursor (uint16_t x, uint16_t y);
    void (*colocarCursorGrafico)(uint16_t, uint16_t);                           // void setGraphicsCursor (uint16_t x, uint16_t y);
    void (*colocarTexto)(const uint8_t*);                                       // void setText_LCD (const uint8_t *ptrBuffer);
    void (*colocarCaracter)(const uint8_t);                                     // void setChar_LCD (const uint8_t caracter);
    void (*colocarTexto_Color)(uint16_t, uint16_t);                             // void setTextColor (uint16_t foregroundColor, uint16_t backgroundColor);
    void (*colocarTexto_Transparent)(uint16_t);                                 // void setTextTransparent (uint16_t foregroundColor);
    void (*colocarTexto_ColorFondo)(uint16_t);                                  // void setTextBackground (uint16_t backgroundColor);
    void (*colocarTexto_Ampliacion)(uint8_t);                                   // void setTextEnlargenemnt (uint8_t size);
    void (*colocarFondo)(const uint16_t);                                       // void setBackground (const uint16_t color);
    void (*colocarModo_LCD)(const uint8_t);                                     // void setModeLCD (const uint8_t mode);
    void (*resetSoftware)(void);                                                // setSoftwareReset (void)
    
    void (*dibujaRectangulo)(RA8875_Shape*);                                    // void drawRectangle (RA8875_Shape *ptrRectangulo);
    void (*dibujaLinea)(RA8875_Shape*);                                         // void drawLine (RA8875_Shape *ptrLine);
    void (*dibujaCirculo)(RA8875_Shape*);                                       // void drawCircle (RA8875_Shape *ptrCircle);
    void (*dibujaTriangulo)(RA8875_Shape*);                                     // void drawTriangle (RA8875_Shape *ptrTriangle);
    void (*dibujaPixel)(uint16_t,uint16_t,uint16_t);                            // void drawPixel (uint16_t x, uint16_t y, uint16_t color);
    
    uint16_t (*convertir_RGB888_To_RGB565)(RGB_Color_t*);                         // uint16_t convertToRGB565 (RGB_Color *ptrRGB);
    
    void (*crearNuevo_Simbolo_o_Fuente)(uint8_t,uint8_t*);                      // void createNew_Font_Or_Symbol (uint8_t address, uint8_t *ptrBuffer);
    void (*mostrar_SimbolosEspeciales)(uint8_t);                                // void showEspecial_Symbols(uint8_t address);

}RA8875_t;

//Generar la estructura con los datos de las funciones.

#ifdef	__cplusplus
extern "C" {
#endif
    
/*************************** Funciones de registro ****************************/   
    
/*************************************************************************
* @Description
* Funcion que registra al controlador SPI, en especifico  las funciones de
* lectura y escritura 
* @Preconditions
* Haber configurado los parametros para controlador SPI del MCU
* @Param
* void (*functionWritePtr)(uint8_t)   -Puntero a funci�n de escritura
* 
* uint8_t (*functionReadPtr)(void)    -Puntero a funci�n de lectura
* @Returns
* Ninguno
* @Example
* #include <xc.h>
* #include <"spi.h">
*  
* RA8875_SPI_Register(writeSPI_Buffer,readSPI_Buffer);
* ************************************************************************/
void RA8875_SPI_Register(void (*functionWritePtr)(uint8_t), uint8_t (*functionReadPtr)(void));

/*************************************************************************
* @Description
* Funcion que registra al pin de chip select SPI 
* @Preconditions
* Ninguna
* @Param
* *ptrPort   - Nombre del Puerto
* 
* Byte       - Numero del puerto a ser utilizado
* @Returns
* Ninguno
* @Example
* #include <xc.h>
* #include <"spi.h">
*  
* RA8875_SPI_PIN_Register(&PORTD, 0b00000100); //PORTDbits.RD2
* ************************************************************************/
void RA8875_SPI_PIN_Register(volatile unsigned char *ptrPort, unsigned char Byte);

/*************************************************************************
* @Description
* Funcion que registra a la estructura RA8875, esta contendra las funciones
* que corresponden a la interfaz del driver 
* @Preconditions
* Ninguna
* @Param
* RA8875 *ptrConstructor   -Estructura que contiene los punteros a las funciones
* @Returns
* Ninguno
* @Example
* #include <xc.h>
* #include <"spi.h">
*  
 * RA8875 myLCD;
* RA8875_Constructor(&myLCD);
* ************************************************************************/
void RA8875_Constructor (RA8875_t *ptrConstructor);
    
/**************************** Funciones locales  ******************************/      

/*************************************************************************
* @Description
* Funcion local para acceder a los registros por separado
* @Preconditions
* Es necesario haber registrado los controladores
* @Param
* uint8_t tipo   - Nombre del comando a utilizar, pueden ser los siguientes macros:
*                   COMAND_WRITE               
*                   STATUS_READ                
*                   DATA_WRITE                 
*                   DATA_READ
* 
* uint8_t info   - Byte a ser enviado 
* @Returns
* Ninguno
* @Example
* #include <xc.h>
* #include <"spi.h">
*  
* escribirRegistro(COMAND_WRITE,Register);
* escribirRegistro(DATA_WRITE,Data);
* ************************************************************************/
static inline void escribirRegistro(uint8_t tipo, uint8_t info);   

/*************************************************************************
* @Description
* Funci�n para inicializar PLL
* 
* La frecuencia del oscilador debe ser mayor a 15MHz y menor a 30MHz
* The internal multiplied clock frequency FPLL = FIN * ( PLLDIVN [4:0] + 1 ) 
* must be equal to or greater than 110 MHz. 
* 
* Formula para calcular la frecuencia del sistema
* SYS_CLK = FIN * ( PLLDIVN [4:0] +1 ) / (( PLLDIVM+1 ) * ( 2^PLLDIVK [2:0] ))
* 
* @Preconditions
* Ninguna
* @Param
* Ninguno
* @Returns
* Ninguno
* @Example
* ************************************************************************/
static inline void init_RA8875_PLL(void);

/*************************************************************************
* @Description
* Funci�n para escribir las configuraciones horizontales
* @Preconditions
* Ninguna
* @Param
* Ninguno
* @Returns
* Ninguno
* @Example
* ************************************************************************/
static inline void init_Horizontal_Screen(void);

/*************************************************************************
* @Description
* Funci�n para escribir las configuraciones verticales
* @Preconditions
* Ninguna
* @Param
* Ninguno
* @Returns
* Ninguno
* @Example
* ************************************************************************/
static inline void init_Vertical_Screen(void);

/*************************************************************************
* @Description
* Funci�n para el tama�o de la pantalla activa
* @Preconditions
* Ninguna
* @Param
* Puntos de inicio y fin horizontales y verticales
* @Returns
* Ninguno
* @Example
* configActiveWindow(0,799,0,479); // Pantalla 800x480
* ************************************************************************/
static void configActiveWindow(uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2);
    
/************************ Manejo del driver  **********************************/ 

/*************************************************************************
* @Description
* Funcion que envia informaci�n a los registros del driver RA8875
* @Preconditions
* Es necesario haber registrado los controladores
* @Param
* uint8_t Register   - Nombre del registroa al cual se enviara informaci�n
*                      Es posible utilizar calquier macro que empieza con la 
*                      palabra REGISTER_  
* 
* uint8_t Data   - Byte a ser enviado 
* @Returns
* Ninguno
* @Example
* #include <xc.h>
* #include <"spi.h">
*
* RA8875_Write(REGISTER_DPCR, DPCR_OneLayer);
* ************************************************************************/
void RA8875_Write (uint8_t Register, uint8_t Data);

/*************************************************************************
* @Description
* Funcion que devuelve el valor de registro de estado del driver.
* @Preconditions
* Ninguna
* @Param
* Nunguno
* @Returns
* uint8_t   - Valor del registro en 8 bits       
* @Example
* ************************************************************************/
uint8_t readState_RA8875();

/*************************************************************************
* @Description
* Funcion que devuelve el valor que contiene un registro del driver en especifico
* @Preconditions
* Ninguna
* @Param
* uint8_t Registro  - Nombre del registro a solicitar
* @Returns
* uint8_t           - Valor del registro en 8 bits       
* @Example
* #include <xc.h>
* #include "RA8875.h"
* 
* uint8_t registroRA8875 = 0;
* registroRA8875 = readRegister_RA8875 (REGISTER_MWCR0);
* ************************************************************************/
uint8_t readRegister_RA8875 (uint8_t Registro);

/*************************************************************************
* @Description
* Funcion que revisa si un proceso ha terminado, para poder continuar con una
* nueva tarea.
* @Preconditions
* Haber comenzado un proceso en el driver.
* @Param
* Register      - Nombre del registro en el cual se esta realizando
*                 el proceso
* statusFlag    - Bandera de estado, es el bit que se estara monitoreando
* @Returns
* uint8_t       - Retorna un '0' cuando el proceso sigue ejecutandose.  
*                 Retorna un '1' cuando el proceso finalizo.                 
* @Example
* #include <xc.h>
* #include "RA8875.h"
* 
* RA8875_Shape myRectangulo;
* 
* myRectangulo.x1 = 0;
* myRectangulo.x2 = 99;
* myRectangulo.y1 = 0;
* myRectangulo.y2 = 479;
* myRectangulo.Color = RA8875_BLUE;
* myRectangulo.fill = 1;
* 
* drawRectangle(&myRectangulo);
* 
* while (0 == isProcessDone(REGISTER_DCR, STATUS_DCR_LST));
* ************************************************************************/
uint8_t isProcessDone(uint8_t Register, uint8_t statusFlag);
    
/**************** Funciones para manejo de la interfaz ************************/

void init_RA8875 (void);
void powerOnDisplay (void);
void powerOffDisplay (void);
void clearWindow (void);
void setBrightness (uint8_t brightness);
void setCursor (uint16_t x, uint16_t y);
void setGraphicsCursor (uint16_t x, uint16_t y);
void setText_LCD (const uint8_t *ptrBuffer);
void setChar_LCD (const uint8_t caracter);
void setTextColor (uint16_t foregroundColor, uint16_t backgroundColor);
void setTextTransparent (uint16_t foregroundColor);
void setTextBackground (uint16_t backgroundColor);
void setTextEnlargenemnt (uint8_t size);
void setBackground (const uint16_t color);

/*************************************************************************
* @Description
* Funcion que asigna el modo en que el display muestra informaci�n
* @Preconditions
* Ninguno
* @Param
* mode   - Nombre del comando a utilizar, pueden ser los siguientes macros:
*          MODE_TEXT               
*          MODE_GRAPHICS               
* @Returns
* Ninguno
* @Example
* #include <xc.h>
* #include <"spi.h">
*  
* setModeLCD(MODE_TEXT);
* ************************************************************************/
void setModeLCD (const uint8_t mode);

void setSoftwareReset(void);

/*************************** Funciones Auxiliares *****************************/

void drawRectangle (RA8875_Shape *ptrRectangulo);
void drawLine (RA8875_Shape *ptrLine);
void drawCircle (RA8875_Shape *ptrCircle);
void drawTriangle (RA8875_Shape *ptrTriangle);
void drawPixel (uint16_t x, uint16_t y, uint16_t color);
uint16_t convertToRGB565 (RGB_Color_t *ptrRGB);
void createNew_Font_Or_Symbol (uint8_t address, uint8_t *ptrBuffer);
void showEspecial_Symbols(uint8_t address);

/********************** Block Transfer Engine Function ************************/

/*******************************************************************************
* @Description
* Funcion para el envio de informaci�n de imagenes a traves de bloques al driver
* @Preconditions
* Haber declarado una variable de tipo BTE_t y otorgado valores iniciales.
* @Param
* *ptrBTE      - Estructura de tipo BTE_t, para esta funcion basta con indicar
*                el valor de destino "x" y "y", asi como el tama�o del bloque.
* *ptrData     - Arreglo que contiene la informaci�n de la imagen que sera 
*                mostrada en la pantalla.
* @Returns
* Ninguno        
*                                
* @Example
* #include <xc.h>
* #include "RA8875.h"
* 
* extern uint16_t arregloImagen;
* BTE_t bteConfig = {0};
* 
* bteConfig.destination_x = 86;
* bteConfig.destination_y = 150;
* bteConfig.height = 76;
* bteConfig.width = 100;
* 
* bte_writeData(&bteConfig, arregloImagen); 
* *****************************************************************************/
void bte_writeData (const BTE_t *ptrBTE, const uint16_t *ptrData);

/*******************************************************************************
* @Description
* Funcion que mueve un bloque de memoria del driver de un punto "A" a un punto
* "B", cuando se utiliza el movimiento positivo el driver empieza a dibujar la
* imagen desde la posici�n 0 hasta el ultimo byte de la memoria.
* @Preconditions
* Estar en modo grafico.
* @Param
* *ptrBTE      - Estructura de tipo BTE_t, para esta funcion es necesario 
*                indicar la posicion de inicio (source) y destino (destination),
*                asi como el tama�o del bloque.
* @Returns
* Ninguno        
*                                
* @Example
* #include <xc.h>
* #include "RA8875.h"
* 
* BTE_t bteConfig = {0};
* 
* bteConfig.source_x = 86;
* bteConfig.source_y = 150;
* bteConfig.destination_x = 526;
* bteConfig.destination_y = 150;
* bteConfig.height = 76;
* bteConfig.width = 100;
* 
* bte_movePositiveDirection(&bteConfig); 
* *****************************************************************************/
void bte_movePositiveDirection (const BTE_t *ptrBTE );

/*******************************************************************************
* @Description
* Funcion que mueve un bloque de memoria del driver de un punto "A" a un punto
* "B", cuando se utiliza el movimiento negayivo el driver empieza a dibujar la
* imagen desde lla ultima posici�n de la memoria hasta posici�n 0.
* @Preconditions
* Estar en modo grafico.
* @Param
* *ptrBTE      - Estructura de tipo BTE_t, para esta funcion es necesario 
*                indicar la posicion de inicio (source) y destino (destination),
*                asi como el tama�o del bloque.
* @Returns
* Ninguno        
*                                
* @Example
* #include <xc.h>
* #include "RA8875.h"
* 
* BTE_t bteConfig = {0};
* 
* bteConfig.source_x = 626;
* bteConfig.source_y = 227;    
* bteConfig.destination_x = 186;
* bteConfig.destination_y = 450;
* bteConfig.height = 76;
* bteConfig.width = 100;
* 
* bte_moveNegativeDirection(&bteConfig);
* *****************************************************************************/
void bte_moveNegativeDirection (const BTE_t *ptrBTE);

/*******************************************************************************
* @Description
* Funcion para el envio de informaci�n de imagenes a traves de bloques al 
* driver, esta funcion acepta como parametro de entrada un color, el cual no 
* sera dibujado en pantalla, cuando la informaci�n de la imagen sea enviada al 
* driver.
* @Preconditions
* Estar en modo grafico.
* @Param
* *ptrBTE      - Estructura de tipo BTE_t, para esta funcion es necesario 
*                indicar la posicion de inicio (source) y destino (destination),
*                asi como el tama�o del bloque.
* Color        - Color que sera utilizado como mascara para el envio de info.
* 
* *ptrData     - Arreglo que contiene la informaci�n de la imagen que sera 
*                mostrada en la pantalla.
* @Returns
* Ninguno        
*                                
* @Example
* #include <xc.h>
* #include "RA8875.h"
* #include "colors.h"
* 
* 
* extern uint16_t arregloImagen;
* BTE_t bteConfig = {0};
* 
* bteConfig.destination_x = 526;
* bteConfig.destination_y = 350;
* bteConfig.height = 76;
* bteConfig.width = 100;
* 
* bte_transparentWrite(&bteConfig, COLOR_WHITE, arregloImagen);
* *****************************************************************************/
void bte_transparentWrite (const BTE_t *ptrBTE, uint16_t Color, const uint16_t *ptrData);


#ifdef	__cplusplus
}
#endif

#endif	/* RA8875_H */

