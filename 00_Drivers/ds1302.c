
#include "ds1302.h"
#include "timers.h"

// <editor-fold defaultstate="collapsed" desc="Funciones para leer/escribir en el reloj DS1302Z">

#if DISABLE_CODE

unsigned char get_bcd(unsigned char Data) 
{
    
    unsigned char NibleH, NibleL;
    NibleH = Data / 10;
    NibleL = Data - (NibleH * 10);
    NibleH = NibleH << 4;
    return (NibleH | NibleL);

}

void set_datetime(unsigned char day, unsigned char mth, unsigned char year, unsigned char dow, unsigned char hr, unsigned char min, unsigned char sec)
{
    
    TrisSCLK = 0;
    TrisRST = 0;

    write_ds1302(0x86, get_bcd(day));
    write_ds1302(0x88, get_bcd(mth));
    write_ds1302(0x8C, get_bcd(year));
    write_ds1302(0x8A, get_bcd(dow));
    write_ds1302(0x84, get_bcd(hr));
    write_ds1302(0x82, get_bcd(min));
    write_ds1302(0x80, get_bcd(sec));

    return;
}

void Lee_reloj_ds1302(void)
{
    sec=get_sec();  
    min=get_min();
    hrs=get_hr();
    day=get_day();
    mth=get_mth();
    year=get_year();

    return;
}

unsigned char get_day() 
{
    return (rm_bcd(read_ds1302(0x87)));
}

unsigned char get_mth()
{
    return (rm_bcd(read_ds1302(0x89)));
}

unsigned char get_year()
{
    return (rm_bcd(read_ds1302(0x8D)));
}

unsigned char get_dow() 
{
    return (rm_bcd(read_ds1302(0x8B)));
}

unsigned char get_hr() 
{
    return (rm_bcd(read_ds1302(0x85)));
}

unsigned char get_min() 
{
    return (rm_bcd(read_ds1302(0x83)));
}

unsigned char get_sec() 
{
    return (rm_bcd(read_ds1302(0x81)));
}

void set_day(unsigned char dayy)
{
    TrisSCLK = 0;
    TrisRST = 0;
    write_ds1302(wDAY, get_bcd(dayy));

    return;
}

void set_mth(unsigned char mthh)
{
    TrisSCLK = 0;
    TrisRST = 0;
    write_ds1302(wMONTH, get_bcd(mthh));

    return;
}

void set_year(unsigned char yearr)
{
    TrisSCLK = 0;
    TrisRST = 0;
    write_ds1302(0x8C, get_bcd(yearr));

    return;
}

void set_hrs(unsigned char hora)
{
    TrisSCLK = 0;
    TrisRST = 0;
    write_ds1302(0x84, get_bcd(hora));

    return;
}

void set_min(unsigned char minutos)
{
    TrisSCLK = 0;
    TrisRST = 0;
    write_ds1302(0x82, get_bcd(minutos));

    return;
}

void set_sec(unsigned char segundo)
{
    TrisSCLK = 0;
    TrisRST = 0;
    write_ds1302(0x80, get_bcd(segundo));

    return;
}

unsigned char rm_bcd(unsigned char Data)
{
    unsigned char i;
    i = Data;
    Data = (i >> 4)*10;
    //Data=Data+(i<<4>>4);
    Data = Data + (i & 0b00001111);
    return (Data);
}

#endif

void init_ds1302(void) 
{
    unsigned char j;
    
    TrisSCLK = 0;                                                               //Inicializar pin de clock
    TrisIO = 0;                                                                 //Inicializar pin de datos
    TrisRST = 0;                                                                //Inicializar pin de chip select

    //Secuencia de inicializacion para DS1302
    RST = 0;    
    __delay_us(2);
    SCLK = 0;   

    write_ds1302(0x8E, 0);                                                      //Write protection para DS1302 deshabilitada
    write_ds1302(0x90, 0xA4);                                                   //Seleccion de resistencias y diodos para el Trickle-Charge Register
    write_ds1302(0x80, 0);                                                      //Al iniciar el firmware, el PIC pondr� el segundero del reloj DS1302 en 0

    return;
}

unsigned char convertDecToBCD (unsigned char hex)
{
    unsigned char bcdValue = 0;
    
    bcdValue  = (hex /10) << 4;
    bcdValue |= (hex %10);
    
    return (bcdValue);
    
}

unsigned char convertBCDToDec (unsigned char bcd)
{
    unsigned char hexValue = 0;
    
    hexValue = ((bcd & 0xF0) >> 4) * 10;
    hexValue =  hexValue + (bcd & 0x0F);
    
    return hexValue;
}

void write_byte(uint8_t Byte)
{
    unsigned char i;
    TrisIO = 0;
    for (i = 0; i <= 7; ++i) {
        IO = Byte & 0x01;
        Byte >>= 1;
        SCLK = 1;
        SCLK = 0;
    }
    return;
}

void Write_RAM(uint8_t direccion,uint8_t dato){
    
    if(direccion>=30){direccion=30;}
    
    direccion=(direccion*2)+0xC0;    
    
    write_ds1302(direccion,dato);
    
    Wait1ms(20);
    
    return;      
    
}


uint8_t Read_RAM(uint8_t direccion){
uint8_t valor;
    
    if(direccion>=30){direccion=30;}
    
    direccion=(direccion*2)+0xC1;    
    
    valor=read_ds1302(direccion);
    
    Wait1ms(20);
    
    return valor;
    
}



void write_ds1302(uint8_t Byte, uint8_t Data) 
{
    RST = 1;
    write_byte(Byte);
    write_byte(Data);
    RST = 0;
    
    return;
}

unsigned char read_ds1302(uint8_t Byte)
{
    
    unsigned char i, Data;

    TrisSCLK = 0;
    TrisRST = 0;
    RST = 1;
    write_byte(Byte);
    TrisIO = 1;
    Nop();
    Data = 0;

    for (i = 0; i <= 6; i++) {
        if (IO == 1) {
            Data += 0x80;
        }
        Data >>= 1;
        SCLK = 1;
        __delay_us(2);
        SCLK = 0;
        __delay_us(2);
    }

    RST = 0;
    return (Data);
}

void getDateTime(Calendario *TiempoActual)
{
    // Lee el tiempo actual en formato BCD
    // No es neceasario convertir ya que por medio de la union Calendario se puede
    // Acceder a los nibles por separados e imprimir en pantalla 
    TiempoActual->year      = read_ds1302(rYEAR);
    TiempoActual->month     = read_ds1302(rMONTH);
    TiempoActual->day       = read_ds1302(rDAY);
    TiempoActual->weekday   = read_ds1302(rWEEKDAY);
    TiempoActual->hour      = read_ds1302(rHOURS);
    TiempoActual->minute    = read_ds1302(rMINUTES);
    TiempoActual->second    = read_ds1302(rSECONDS);
    
    return;
}

void getDateTimeDecimal(Calendario *TiempoActual)
{
    // Lee el tiempo actual en formato BCD
    // y convierte el valor BCD a decimal
    // por lo que cada variable tendra el valor real de cada parametro en decimal
    TiempoActual->year      = convertBCDToDec(read_ds1302(rYEAR));
    TiempoActual->month     = convertBCDToDec(read_ds1302(rMONTH));
    TiempoActual->day       = convertBCDToDec(read_ds1302(rDAY));
    TiempoActual->weekday   = convertBCDToDec(read_ds1302(rWEEKDAY));
    TiempoActual->hour      = convertBCDToDec(read_ds1302(rHOURS));
    TiempoActual->minute    = convertBCDToDec(read_ds1302(rMINUTES));
    TiempoActual->second    = convertBCDToDec(read_ds1302(rSECONDS));
    
    return;
}

void setDafultDateTime()
{
    //Lee el tiempo actual en formato BCD
    TrisSCLK = 0;
    TrisRST = 0;

    // 01/01/17  01 : 00 : 00
    
    write_ds1302(0x86, convertDecToBCD(1));
    write_ds1302(0x88, convertDecToBCD(1));
    write_ds1302(0x8C, convertDecToBCD(17));
    write_ds1302(0x8A, convertDecToBCD(1));
    write_ds1302(0x84, convertDecToBCD(1));
    write_ds1302(0x82, convertDecToBCD(0));
    write_ds1302(0x80, convertDecToBCD(0));
    
    return;
}

void setDate (uint8_t dia, uint8_t mes, uint8_t anio, uint8_t dow)
{
    TrisSCLK = 0;
    TrisRST = 0;

    write_ds1302(wDAY, convertDecToBCD(dia));
    write_ds1302(wMONTH, convertDecToBCD(mes));
    write_ds1302(wYEAR, convertDecToBCD(anio));
    write_ds1302(wWEEKDAY, convertDecToBCD(dow));
    
}

void setTime (uint8_t hora, uint8_t minuto, uint8_t seg)
{
    TrisSCLK = 0;
    TrisRST = 0;

    write_ds1302(wHOURS, convertDecToBCD(hora));
    write_ds1302(wMINUTES, convertDecToBCD(minuto));
    write_ds1302(wSECONDS, convertDecToBCD(seg));
}




#if DISABLE_CODE



#endif

// </editor-fold>

