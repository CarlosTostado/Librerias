/* 
 * File:   colors.h
 * Author: Desarrollo
 *
 * Created on 28 de septiembre de 2017, 02:27 PM
 */

#ifndef COLORS_H
#define	COLORS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
/********************* Colores en formato RGB565 Basicos **********************/
    
#define	COLOR_BLACK                                 0x0000
#define	COLOR_BLUE                                  0x001F
#define	COLOR_RED                                   0xF800
#define	COLOR_GREEN                                 0x07E0
#define COLOR_CYAN                                  0x07FF
#define COLOR_MAGENTA                               0xF81F
#define COLOR_YELLOW                                0xFFE0  
#define COLOR_WHITE                                 0xFFFF
#define COLOR_PURPLE                                0x6019

#define COLOR_LIGHT_GREY                            0xD69A
#define COLOR_LIGHT_RED                             0xFDB8
#define COLOR_LIGHT_BLUE                            0xAEDC
#define COLOR_LIGHT_GREEN                           0x9FD3  
  
#define COLOR_DARK_GREY                             0xAD55      
#define COLOR_DARK_RED                              0x8800 
#define COLOR_DARK_BLUE                             0x0010
#define COLOR_DARK_GREEN                            0x0320    
    
/***************** Colores en formato RGB565 Perzonalizados *******************/


#ifdef	__cplusplus
}
#endif

#endif	/* COLORS_H */

