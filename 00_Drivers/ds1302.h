/* 
 * File:   DS1302.h
 * Author: Don Rulo
 * Created on 13 de enero de 2017, 11:26 AM
 */

#ifndef DS1302_H
#define	DS1302_H   

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// Pines de control para el reloj DS1302 
#define TrisSCLK                TRISBbits.TRISB1		// Pin de Clock de DS1302
#define TrisIO                  TRISBbits.TRISB2		// Pin de Datos de DS1302
#define TrisRST                 TRISBbits.TRISB3		// Pin de Enable de DS1302
#define SCLK                    PORTBbits.RB1                   // Pin de Clock de DS1302
#define IO                      PORTBbits.RB2                   // Pin de Datos de DS1302
#define RST                     PORTBbits.RB3                   // Pin de Enable de DS1302

#define _XTAL_FREQ              32000000
#define DISABLE_CODE            0                                               //Desactiva codigo que no es utilizado por el momento

// Constantes con la direccion de memoria para leer y escribir registros
#define wSECONDS                0x80
#define wMINUTES                0x82
#define wHOURS                  0x84
#define wDAY                    0x86
#define wMONTH                  0x88
#define wWEEKDAY                0x8A
#define wYEAR                   0x8C

#define rSECONDS                0x81
#define rMINUTES                0x83
#define rHOURS                  0x85
#define rDAY                    0x87
#define rMONTH                  0x89
#define rWEEKDAY                0x8B
#define rYEAR                   0x8D


typedef union {
    struct {  
        uint8_t year;
        uint8_t month;
        uint8_t day;
        uint8_t weekday;
        uint8_t hour;
        uint8_t minute;
        uint8_t second; 
    };
    
    struct {  
        uint8_t yearL           : 4;                                            //nible bajo de la variable a�o
        uint8_t yearH           : 4;                                            //nible alto de la variable a�o
        uint8_t monthL          : 4;                                            //nible bajo de la variable mes
        uint8_t monthH          : 4;                                            //nible alto de la variable mes
        uint8_t dayL            : 4;                                            //nible bajo de la variable dia
        uint8_t dayH            : 4;                                            //nible alto de la variable dia
        uint8_t weekdayL        : 4;                                            //nible bajo de la variable dia de la semana
        uint8_t weekdayH        : 4;                                            //nible alto de la variable dia de la semana
        uint8_t hourL           : 4;                                            //nible bajo de la variable hora
        uint8_t hourH           : 4;                                            //nible alto de la variable hora
        uint8_t minuteL         : 4;                                            //nible bajo de la variable minuto
        uint8_t minuteH         : 4;                                            //nible alto de la variable minuto
        uint8_t secondL         : 4;                                            //nible bajo de la variable segundo
        uint8_t secondH         : 4;                                            //nible alto de la variable segundo
      
   };
} Calendario;



/*Escribir/leer reloj DS1302Z*/
void init_ds1302(void);                                                         // Inicializar reloj DS1302

unsigned char convertDecToBCD (unsigned char hex);                              // Convierte hex to bcd
unsigned char convertBCDToDec (unsigned char bcd);                              // Convierte bcd to hex

void write_byte(uint8_t Byte);                                            // Escribir un byte en el reloj DS1302
void write_ds1302(uint8_t Byte, uint8_t Data);                      // Escribir un dato en un registro del reloj DS1302

unsigned char read_ds1302(uint8_t Byte);                                  // Leer un dato de un registro del reloj DS1302
void getDateTime (Calendario *TiempoActual);                                    // Funcion que obtiene la fecha y hora en formato BCD
void getDateTimeDecimal(Calendario *TiempoActual);                              // Funcion que obtiene la fecha y hora en formato Decimal

void setDafultDateTime ();                                                      // Funcion que escribe el Fecha y Hora por default
void setDate (uint8_t dia, uint8_t mes, uint8_t anio, uint8_t dow);             // Coloca la fecha
void setTime (uint8_t hora, uint8_t minuto, uint8_t seg);                       // Coloca la hora


void Write_RAM(uint8_t direccion,uint8_t dato);
uint8_t Read_RAM(uint8_t direccion);
#if DISABLE_CODE

unsigned char get_bcd(unsigned char Data);                                      //Convertir una variable unsigned char a formato BCD
unsigned char rm_bcd(unsigned char Data);                                       //Convertir de formato BCD a una variable unsigned char
unsigned char get_day();                                                        //Leer dia del reloj DS1302
unsigned char get_mth();                                                        //Leer mes del reloj DS1302
unsigned char get_year();                                                       //Leer a�o del reloj DS1302
unsigned char get_dow();                                                        //Leer dia de la semana del reloj DS1302
unsigned char get_hr();                                                         //Leer hora del reloj DS1302
unsigned char get_min();                                                        //Leer minutos reloj DS1302
unsigned char get_sec();                                                        //Leer segundos del reloj DS1302
void set_datetime(unsigned char day, unsigned char mth, unsigned char year, unsigned char dow, unsigned char hr, unsigned char min, unsigned char sec); //Modificar registros del reloj DS1302 en una sola funcion
void set_day(unsigned char dayy);                                               //Modificar dia de DS1302
void set_mth(unsigned char mthh);                                               //Modificar mes de DS1302
void set_year(unsigned char yearr);                                             //Modificar a�o de DS1302
void set_hrs(unsigned char hora);                                               //Modificar hora de DS1302
void set_min(unsigned char minutos);                                            //Modificar minutos de DS1302
void set_sec(unsigned char segundo);                                            //Modificar segundos de DS1302
void Lee_reloj_ds1302(void);                                                    //Leer los registros de DS1302 en una sola funcion
void Checar_hora_y_fecha(void);                                                 //Checar si ya transcurrio una hora y si ya se llego al tiemp oestablecido para el cambio de modo de operacion de la bomba

/******************************************************************************/

// <editor-fold defaultstate="collapsed" desc="Variables globales">
unsigned char sec=45;   //Segundos actuales del reloj DS1302
unsigned char min=0;    //Minutos actuales del reloj DS1302
unsigned char hrs=45;   //Hora actual del reloj DS1302
unsigned char day=45;   //Dia actual del reloj DS1302
unsigned char mth=45;   //Mes actual del reloj DS1302
unsigned char year=45;  //Hora actual del reloj DS1302
// </editor-fold>

#endif


#endif	/* DS1302_H */

