/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     config.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     1.0
 * 
 * Configuraciones generales del sistema para PIC18F27K40
 */

#ifndef CONFIG_H
#define	CONFIG_H

#include <xc.h>
#include <stdint.h>

/*********************************MACROS***************************************/

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para Oscilador (OSCFRQ)">

//Frecuencias de operación
#define OSCFRQ_HFFRQ_64MHZ                                  0x08                // HFINTOSC de 64 Mhz
#define OSCFRQ_HFFRQ_48MHZ                                  0x07                // HFINTOSC de 48 Mhz
#define OSCFRQ_HFFRQ_32MHZ                                  0x06                // HFINTOSC de 32 Mhz
#define OSCFRQ_HFFRQ_16MHZ                                  0x05                // HFINTOSC de 16 Mhz
#define OSCFRQ_HFFRQ_12MHZ                                  0x04                // HFINTOSC de 12 Mhz
#define OSCFRQ_HFFRQ_08MHZ                                  0x03                // HFINTOSC de 8  Mhz
#define OSCFRQ_HFFRQ_04MHZ                                  0x02                // HFINTOSC de 4  Mhz
#define OSCFRQ_HFFRQ_02MHZ                                  0x01                // HFINTOSC de 2  Mhz
#define OSCFRQ_HFFRQ_01MHZ                                  0x00                // HFINTOSC de 1  Mhz

//Habilitación manual del oscilador
#define HF_OSCILLATOR                               OSCENbits.HFEN
#define ADC_OSCILLATOR                              OSCENbits.ADOEN
#define SECONDARY_OSCILLATOR                        OSCENbits.SOSCEN

//Estado del oscilador
#define HINT_OSCILLATOR_STATE                       OSCSTATbits.HFOR            // Macro para verificar si el HFINTOSC esta listo para ser usado
#define ADC_OSCILLATOR_STATE                        OSCSTATbits.ADOR            // Macro para verificar si el oscilador ADC esta listo para ser usado
#define SECONDARY_OSCILLATOR_STATE                  OSCSTATbits.SOR             // Macro para verificar si el oscilador secundario esta listo para ser usado 

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para FVR (FVRCON)">

#define FVR_READY                                   FVRCONbits.FVRRDY           // Macro para verificar si el FVR esta listo para ser usado

#define FVRCON_TSEN_ON                              0x20                        // Indicador de temperatura habilitado
#define FVRCON_TSRNG_4VT                            0x10                        // Rango de indicador de temperatura = VDD - 4VT
#define FVRCON_TSRNG_2VT                            0x00                        // Rango de indicador de temperatura = VDD - 2VT

#define FVRCON_CDAFVR_4096                          0x0C                        // Ganancia FVR para comparadores es de 4 (4.096V)
#define FVRCON_CDAFVR_2048                          0x08                        // Ganancia FVR para comparadores es de 2 (2.048V)
#define FVRCON_CDAFVR_1024                          0x04                        // Ganancia FVR para comparadores es de 1 (1.024V)
#define FVRCON_CDAFVR_OFF                           0x00                        // FVR para comparadores deshabilitado

#define FVRCON_ADFVR_4096                           0x03                        // Ganancia FVR para ADC es de 4 (4.096V)
#define FVRCON_ADFVR_2048                           0x02                        // Ganancia FVR para ADC es de 2 (2.048V)
#define FVRCON_ADFVR_1024                           0x01                        // Ganancia FVR para ADC es de 1 (1.024V)
#define FVRCON_ADFVR_OFF                            0x00                        // FVR para ADC deshabilitado

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para PMD (PMDx)">
#define PMD_ENABLE                                  0x00
#define PMD_DISABLE                                 0xFF
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros para desactivar/activar Interrupciones">
//Configuración de interrupciones generales
#define enableGlobalInterrupts()                    INTCONbits.GIE = 1
#define enablePeripheralInterrupts()                INTCONbits.PEIE = 1
#define enablePriorityInterrupts()                  INTCONbits.IPEN = 1
#define enableExternRB0Interrupt()                  PIE0bits.INT0IE = 1
#define RB0interruptRisingEdge()                    INTCONbits.INT0EDG = 1

#define disableGlobalInterrupts()                   INTCONbits.GIE = 0
#define disablePeripheralInterrupts()               INTCONbits.PEIE = 0
#define disablePriorityInterrupts()                 INTCONbits.IPEN = 0
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de uso general">
#define ENABLE                                      0x01
#define DEFAULT                                     0x00
// </editor-fold>

/******************************************************************************/

/*****************************ENUMS/STRUCTS/UNIONS*****************************/

// NA
    
/******************************************************************************/

/*********************************FUNCIONES************************************/
    
// <editor-fold defaultstate="collapsed" desc="Funciones públicas (Uso general)">
/* ************************************************************************
 * gpio_config
 * @Description
 * Función que configura GPIO
 * @Preconditions
 * NA
 * @Param
 * NA (NOTA: Se tienen que modificar los registros directamente en la función)
 * @Returns
 * NA
 * @Example
 * gpio_config();
 * ************************************************************************/
void gpio_config(void);

/* ************************************************************************
 * oscillator_config
 * @Description
 * Función que configura el oscilador interno
 * @Preconditions
 * NA
 * @Param
 * oscfrq       - Configuraciones deseadas para registro OSCFRQ (Ver macros)
 * @Returns
 * NA
 * @Example
 * oscillator_config(OSCFRQ_HFFRQ_48MHZ);
 * ************************************************************************/
void oscillator_config (uint8_t oscfrq);

/* ************************************************************************
 * fvr_config
 * @Description
 * Función que inicializa FVR (Fixed Voltage Reference)
 * @Preconditions
 * NA
 * @Param
 * fvrcon       - COnfiguraciones deseadas para registro FVRCON (Ver macros)
 * @Returns
 * NA
 * @Example
 * fvr_config(FVRCON_ADFVR_4096);
 * ************************************************************************/
void fvr_config (uint8_t fvrcon);

/* ************************************************************************
 * pmd_config
 * @Description
 * Función habilita/deshabilita periféricos 
 * @Preconditions
 * NA
 * @Param
 * pmd          - Deshabilita o habilita registro PMD (Ver macro)
 * @Returns
 * NA
 * @Example
 * pmd_config(PMD_ENABLE);
 * ************************************************************************/
void pmd_config(uint8_t pmd);
// </editor-fold>

/******************************************************************************/ 

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

