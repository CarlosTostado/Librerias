/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     config.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     1.0
 * 
 * Configuraciones generales del sistema para PIC18F27K40
 */

#include "config.h"

void oscillator_config (uint8_t oscfrq)
{
    //Configuracion por default a 64Mhz
    OSCFRQ = 0x0F & oscfrq;
    
    //Espera que el oscilador este listo
    while (0 == HINT_OSCILLATOR_STATE); 
    
    return;
}

void fvr_config(uint8_t fvrcon)
{
    FVRCON = 0xC0 | fvrcon;
    while(0 == FVR_READY);
    
}

void gpio_config(void)
{
    //Limpia los puertos
    PORTA = 0x00;
    PORTB = 0x00;
    PORTC = 0x00;
    PORTE = 0x00;
    
    //Limpia los Latches
    LATA = 0x00;
    LATB = 0x00;
    LATC = 0x00;
    
    //Configura los pines como digitales
    ANSELA = 0b00000011;
    ANSELB = 0x00;
    ANSELC = 0x00;
    
    //Coloca los pines como salida
    TRISA = 0b00000011;
    TRISB = 0b00000001;
    TRISC = 0b00010101;
    
    //Configuración de los pine
    TRISCbits.TRISC3 = 0;
    TRISCbits.TRISC4 = 1;
    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC7 = 1;
    //Asignar pines de SPI - Recordar el estado de las interrupcion
    uint8_t state = GIE;
     
    //Apagar interrrupciones
    GIE = 0;
     
    //Secuencia de desbloque de periferico
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0x00; // unlock PPS
 
    //Asigna los recursos de SPI a los pines por default
    SSP1CLKPPSbits.SSPCLKPPS = 0x13;
    SSP1DATPPSbits.SSPDATPPS = 0x14;
    
    //Redirecciones los Pines a los recursos de SPI
    RC5PPS = 0x10;     //RC5->MSSP1:SDO1
    RC3PPS = 0x0F;     //RC3->MSSP1:SCK1
            
    // Asigna los recursos de EUSART1 a pines por default
    RX1PPSbits.RXPPS = 0x17;
    TX1PPSbits.TXPPS = 0x16;

    // Redirección de pines EUSART
    RC7PPS = 0x0A;
    RC6PPS = 0x09;
    
    // Asigna los recursos de Interrupción Externa por default
//    INT0PPSbits.INT0PPS = 0x08;
            
    //Secuencia de bloqueo del perifericp
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    
    PPSLOCKbits.PPSLOCKED = 0x01; // lock PPS
    
    //Regresa el estado de las interrupciones
    GIE = state;
    
    //Desactiva las resistencias Pull-Up
    WPUA = 0x00;
    WPUB = 0x00;
    WPUC = 0x00;
    
    //Niveles de voltaje TTL o Schmitt Trigger (0 - TTL, 1 - ST)
    INLVLA = 0xFF;  
    INLVLB = 0xFF;
    INLVLC = 0xFF;
    INLVLE = 0xFF;
    
    //Slew Rate por defualt a maximo
    SLRCONA = 0xFF;
    SLRCONB = 0xFF;
    SLRCONC = 0xFF;
    
    //Open drain desactivado (La salida puede manejar altos y bajos)
    ODCONA = 0x00;
    ODCONB = 0x00;
    ODCONC = 0x00;
}

void pmd_config(uint8_t pmd)
{  
    PMD0 = pmd;
    PMD1 = pmd;
    PMD2 = pmd;
    PMD3 = pmd;
    PMD5 = pmd;
    PMD4 = pmd;
}


