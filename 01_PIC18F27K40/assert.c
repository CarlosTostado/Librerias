/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     assert.h
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Marzo 24, 2017
 * Version:     1.0
 * 
 * Archivo:     assert.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptaci�n a est�ndar de librer�as
 */

#include "eeprom.h"
#include "assert.h"

static uint8_t LocalidadActual = 0;

void aFailed(uint16_t File_Num, uint16_t line)
{
    uint8_t Auxiliar1 = 0;
    uint8_t bandera_sobrecarga = 0;
    
    //Ocurrio un error por assertion
    write_eeprom(LOCALIDAD_ERROR,0x01);
    
    //Obtiene el numero de erroes acutales
    Auxiliar1 = read_eeprom(LOCALIDAD_NUMERO_ERRORES);
    
    //obtiene la localidad de memoria actual
    LocalidadActual = read_eeprom(LOCALIDAD_POSICION_MEMORIA);
    
    //En caso de ser la primer lectura
    if (0xFF == Auxiliar1)
    {
        LocalidadActual = 0x07;
        Auxiliar1 = 0;
    }
    else if (50 <= Auxiliar1)                                                   //Evita sobrecarga de datos
    {
        write_eeprom(LOCALIDAD_SOBRECARGA, 0x01);
        bandera_sobrecarga = 1;
    }
    
    // No guarda mas errores de los que soporta la eeprom
    if (0x00 == bandera_sobrecarga)
    {
        //Guarda el acumulado del error
        write_eeprom(LOCALIDAD_NUMERO_ERRORES,++Auxiliar1);

        //Escribe la versi�n de FW
        write_eeprom(++LocalidadActual,VERSION_FW);

        //Escribe el numero de archivo donde ocurrio el error
        write_eeprom(++LocalidadActual,(uint8_t) ( File_Num & 0xFF ));

        //Guarda la localidad alta de la linea donde ocurrio el error
        write_eeprom(++LocalidadActual, (uint8_t) (( line & 0xFF00 ) >> 8) );

        //Guarda la localidad baja donde ocurrio el error
        write_eeprom(++LocalidadActual, (uint8_t) (line & 0x00FF ));

        //Guarda la ultima localidad de memoria
        write_eeprom(LOCALIDAD_POSICION_MEMORIA,LocalidadActual);   
    }
    
    //Aplica reset por software
    RESET();
    
    return;
}
