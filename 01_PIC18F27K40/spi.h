/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     spi.ht.h
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Mayo 25, 2017
 * Version:     1.0
 * 
 * Archivo:     assert.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptación a estándar de librerías y adaptación a PIC18F27K40
 */

#ifndef SPI_H
#define SPI_H
 
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef  __cplusplus
extern "C" {
#endif

/********************************* MACROS *************************************/
   
// <editor-fold defaultstate="collapsed" desc="Macros de configuración para SPI (SSP1STAT)">
#define SSP1STAT_SMP                    0x80                                    // Información de entrada es verificada al final de información de salida
#define SSP1STAT_NULL                   0x00                                    // El registro configurado lleva las configuraciones por defecto
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para SPI (SSP1CON1)">
#define SSP1CON1_SSPM_MFOSC4SSPADD      0x0A                                    // SPI Maestro (CLK = FOSC/(4 *(SSPxADD + 1))
#define SSP1CON1_SSPM_SSCKSS            0x05                                    // SPI Maestro (CLK = SCKx pin, SSx pin deshabilitado, puede ser usado como GPIO)
#define SSP1CON1_SSPM_SSCK              0x04                                    // SPI Maestro (CLK = SCKx pin, SSx pin habilitado)
#define SSP1CON1_SSPM_MTMR2             0x03                                    // SPI Maestro (CLK = TMR2 / 2)
#define SSP1CON1_SSPM_MFOSC64           0x02                                    // SPI Maestro (CLK = FOSC/64)
#define SSP1CON1_SSPM_MFOSC16           0x01                                    // SPI Maestro (CLK = FOSC/16)
#define SSP1CON1_SSPM_MFOSC4            0x00                                    // SPI Maestro (CLK = FOSC/4)
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para SPI (MODO) @LLAMARLOS PARA CAMBIAR LOS MODOS DE CONFIGURACIÓN@">
#define MODO00()                        SSP1STATbits.CKE = 0; \
                                        SSP1CON1bits.CKP = 0

#define MODO01()                        SSP1STATbits.CKE = 0; \
                                        SSP1CON1bits.CKP = 1

#define MODO10()                        SSP1STATbits.CKE = 1; \
                                        SSP1CON1bits.CKP = 0

#define MODO11()                        SSP1STATbits.CKE = 1; \
                                        SSP1CON1bits.CKP = 1
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de uso general">
#define CS                              LATBbits.LATB4           
#define CLK                             PORTCbits.RC3
#define SDO                             PORTCbits.RC5
#define SDI                             PORTCbits.RC4
// </editor-fold>
    
/******************************************************************************/
    
/*****************************ENUMS/STRUCTS/UNIONS*****************************/

// NA
    
/******************************************************************************/

/*********************************FUNCIONES************************************/

// <editor-fold defaultstate="collapsed" desc="Funciones públicas (Uso general)">
/* ************************************************************************
 * spi_config
 * @Description
 * Función que inicializa el periférico ADC
 * @Preconditions
 * NA
 * @Param
 * ssp1stat       - Configuraciones deseadas para registro SSP1STAT (Ver macros)
 * ssp1con1       - Configuraciones deseadas para registro SSP1CON1 (Ver macros)
 * @Returns
 * NA
 * @Example
 * spi_config(SSP1STAT_NULL, SSP1CON1_SSPM_MFOSC4);
 * ************************************************************************/
void spi_config(uint8_t ssp1stat, uint8_t ssp1con1);

/* ************************************************************************
 * WriteSPI1
 * @Description
 * Función que hace escrituras SPI
 * @Preconditions
 * Inicialización de SPI
 * @Param
 * data           - Información que se quiere enviar
 * @Returns
 * NA
 * @Example
 * WriteSPI1(55);
 * ************************************************************************/
void WriteSPI1(uint8_t data);

/* ************************************************************************
 * ReadSPI_CC1101
 * @Description
 * Función que hace lecturas SPI
 * @Preconditions
 * Inicialización de SPI
 * @Param
 * NA
 * @Returns
 * Información de SPI
 * @Example
 * x = ReadSPI_CC1101();
 * ************************************************************************/
uint8_t ReadSPI_CC1101(void); // </editor-fold>

/******************************************************************************/
#ifdef  __cplusplus
}
#endif
 
#endif  /* SPI_H */

