/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     spi.ht.h
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Mayo 25, 2017
 * Version:     1.0
 * 
 * Archivo:     assert.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptaci�n a est�ndar de librer�as y adaptaci�n a PIC18F27K40
 */

#include "spi.h"
 
void spi_config(uint8_t ssp1stat, uint8_t ssp1con1)
{
    
    // SMP Middle; CKE Active to Idle; 
    SSP1STAT = 0x00 | ssp1stat;
//    SSP1STAT = 0x40;
     
    // SSPEN enabled; CKP Idle:Low, Active:High; SSPM FOSC/16;
    SSP1CON1 = 0x20 | ssp1con1;
//    SSP1CON1 = 0x20;
     
    // SSPADD 0; 
    SSP1ADD = 0x00;
}
 
void WriteSPI1(uint8_t data)
{
    // Clear the Write Collision flag, to allow writing
    if (1 == SSP1CON1bits.WCOL)
    {
        SSP1CON1bits.WCOL = 0;
    }
         
    SSP1BUF = data;
 
    while(!SSP1STATbits.BF)
    {
    }
     
    return;
}
 
uint8_t ReadSPI_CC1101(void)
{
    // Clear the Write Collision flag, to allow writing
    if (1 == SSP1CON1bits.WCOL){
        SSP1CON1bits.WCOL = 0;
    }
     
    //Write dummyData 
    SSP1BUF = 0xFF;
 
    while(!SSP1STATbits.BF)
    {
    }
     
    return (SSP1BUF);
}
