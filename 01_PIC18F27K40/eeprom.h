/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     EEPROM.h
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Enero 16, 2017
 * Version:     1.0
 * 
 * Librer�a de uso general para memoria EEPROM
 * 
 * Archivo:     EEPROM.h 
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptaci�n a est�ndar de librer�as
 */

#ifndef EEPROM_H
#define	EEPROM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>

/************************************MACROS************************************/

// <editor-fold defaultstate="collapsed" desc="Macros de uso general">
#define LOCALIDAD_MARCA_NOMBRAMIENTO             0x00                           // Espacio en EEPROM para escribir que ya se nombr� el medidor
#define LOCALIDAD_NUMMED_HBYTE                   0x01                           // Espacio en EEPROM para High Byte del # de medidor
#define LOCALIDAD_NUMMED_MBYTE                   0x02                           // Espacio en EEPROM para Mid Byte del # de medidor
#define LOCALIDAD_NUMMED_LBYTE                   0x03                           // Espacio en EEPROM para Low Byte del # de medidor
#define LOCALIDAD_SURTIDO_INI                    0x04                           // Espacio en EEPROM para Inicio de Surtido
#define LOCALIDAD_SURTIDO_FIN                    0x05                           // Espacio en EEPROM para Fin de Surtido
#define MARCA_NOMBRAMIENTO                       0x15                           // Valor de escritura que funcionar� como marca de validaci�n (21)     
// </editor-fold>

/******************************************************************************/

/*****************************ENUMS/STRUCTS/UNIONS*****************************/

// NA
    
/******************************************************************************/
    
/***********************************FUNCIONES**********************************/

// <editor-fold defaultstate="collapsed" desc="Funciones p�blicas (Uso general)">
    
/* ************************************************************************
 * EEPROM_Write
 * @Description
 * Funci�n que hace escritura de informaci�n en memoria EEPROM
 * @Preconditions
 * NA
 * @Param
 * add          - Direcci�n en memoria EEPROM para guardar informaci�n
 * datain       - Informaci�n que se quiere guardar
 * @Returns
 * NA
 * @Example
 * EEPROM_Write(5, 10);
 * ************************************************************************/
void write_eeprom (uint8_t add, uint8_t data);

/* ************************************************************************
 * EEPROM_Read
 * @Description
 * Funci�n que hace lecturas de informaci�n en memoria EEPROM
 * @Preconditions
 * NA
 * @Param
 * address      - Direci�n en memoria EEPROM para leer informaci�n
 * @Returns
 * NA
 * @Example�
 * x = EEPROM_Read(5);
 * ************************************************************************/
uint8_t read_eeprom (uint8_t add);

// </editor-fold>

/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif	/* EEPROM_H */

