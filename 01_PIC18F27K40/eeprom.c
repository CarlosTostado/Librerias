/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     EEPROM.h
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Enero 16, 2017
 * Version:     1.0
 * 
 * Librer�a de uso general para memoria EEPROM
 * 
 * Archivo:     EEPROM.h 
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptaci�n a est�ndar de librer�as
 */
#include "eeprom.h"

void write_eeprom (uint8_t add, uint8_t data)
{
    uint8_t GIEBitValue = INTCONbits.GIE;
   
    if (data != read_eeprom(add))                                               //Comprueba que no se escriba lo mismo
    {
        NVMCON1bits.NVMREG = 0b00;                                              //Selecciona eeprom
        NVMADRL = (add & 0xFF);
        NVMDAT  = data;                                                         //Carga el dato a la eeprom
        
        NVMCON1bits.WREN = 1;                                                   //Habilita la escritura
        INTCONbits.GIE = 0;                                                     // Disable interrupts
        NVMCON2 = 0x55;                                                         //Secuencia requerida
        NVMCON2 = 0xAA;
        NVMCON1bits.WR = 1;
        while(NVMCON1bits.WR){}
        
        NVMCON1bits.WREN = 0;                                                   //Desabilita la escritura
        INTCONbits.GIE = GIEBitValue;
    }
    
    return;
}

uint8_t read_eeprom (uint8_t add)
{
    NVMCON1bits.NVMREG = 0b00;                                                  //Selecciona eeprom
    NVMADRL = (add & 0xFF);                                                     //Pasa address
    NVMCON1bits.RD = 1;                                                         //Comienza la lecutra
    NOP();                                                                      // NOPs may be required for latency at high frequencies
    NOP();

    return (NVMDAT);
    
}
