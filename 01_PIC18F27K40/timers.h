/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     timers.hh
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Mayo 22, 2017
 * Version:     1.0
 * 
 * Librería para manejo de TMRs en PIC18F24K40
 * 
 * Archivo:     timers.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptación a estándar de librerías y adaptación a PIC18F27K40
 */

#ifndef TIMERS_H
#define	TIMERS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <xc.h>
#include <stdint.h>

/********************************* MACROS *************************************/
    
// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR0 (T0CON0)">

#define T0CON0_T0EN                                   0x80                     // Habilita TMR0     
#define T0CON0_T016bit                                 0x10                     // TMR0 de 16 bits
#define T0CON0_T0OUTPS_16                              0x0F                     // Postcaler 1:16
#define T0CON0_T0OUTPS_15                              0x0E                     // Postcaler 1:15
#define T0CON0_T0OUTPS_14                              0x0D                     // Postcaler 1:14
#define T0CON0_T0OUTPS_13                              0x0C                     // Postcaler 1:13
#define T0CON0_T0OUTPS_12                              0x0B                     // Postcaler 1:12
#define T0CON0_T0OUTPS_11                              0x0A                     // Postcaler 1:11
#define T0CON0_T0OUTPS_10                              0x09                     // Postcaler 1:10
#define T0CON0_T0OUTPS_9                               0x08                     // Postcaler 1: 9
#define T0CON0_T0OUTPS_8                               0x07                     // Postcaler 1: 8
#define T0CON0_T0OUTPS_7                               0x06                     // Postcaler 1: 7
#define T0CON0_T0OUTPS_6                               0x05                     // Postcaler 1: 6 
#define T0CON0_T0OUTPS_5                               0x04                     // Postcaler 1: 5
#define T0CON0_T0OUTPS_4                               0x03                     // Postcaler 1: 4
#define T0CON0_T0OUTPS_3                               0x02                     // Postcaler 1: 3
#define T0CON0_T0OUTPS_2                               0x01                     // Postcaler 1: 2
#define T0CON0_T0OUTPS_1                               0x00                     // Postcaler 1: 1

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR0 (T0CON1)">

#define T0CON1_T0CS_SOSC                           0xA0                         // TMR0 CLK = Oscilador secundario
#define T0CON1_T0CS_LFINTOSC                       0x80                         // TMR0 CLK = LFINTOSC
#define T0CON1_T0CS_HFINTOSC                       0x60                         // TMR0 CLK = HFINTOSC
#define T0CON1_T0CS_FOSC4                          0x40                         // TMR0 CLK = FOSC/4
#define T0CON1_T0CS_T0CKIPPS                       0x20                         // TMR0 CLK = Pin seleccionad por T0CKIPPS (Invertido)

#define T0CON1_T0ASYNC                             0x10                         // Entrada a TMR0 NO esta sincronizada a CLK

#define T0CON1_T0CKPS_32768                        0x0F                         // Preescaler 1:32768
#define T0CON1_T0CKPS_16384                        0x0E                         // Preescaler 1:16384
#define T0CON1_T0CKPS_8192                         0x0D                         // Preescaler 1: 8192
#define T0CON1_T0CKPS_4096                         0x0C                         // Preescaler 1: 4096
#define T0CON1_T0CKPS_2048                         0x0B                         // Preescaler 1: 2048
#define T0CON1_T0CKPS_1024                         0x0A                         // Preescaler 1: 1024
#define T0CON1_T0CKPS_512                          0x09                         // Preescaler 1:  512
#define T0CON1_T0CKPS_256                          0x08                         // Preescaler 1:  256
#define T0CON1_T0CKPS_128                          0x07                         // Preescaler 1:  128
#define T0CON1_T0CKPS_64                           0x06                         // Preescaler 1:   64
#define T0CON1_T0CKPS_32                           0x05                         // Preescaler 1:   32
#define T0CON1_T0CKPS_16                           0x04                         // Preescaler 1:   16
#define T0CON1_T0CKPS_8                            0x03                         // Preescaler 1:    8
#define T0CON1_T0CKPS_4                            0x02                         // Preescaler 1:    4
#define T0CON1_T0CKPS_2                            0x01                         // Preescaler 1:    2
#define T0CON1_T0CKPS_1                            0x00                         // Preescaler 1:    1
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR2 (T2CON0)">

#define T2CON_T2ON                                 0x80                         // Habilita TMR2

#define T2CON_CKPS_128                             0x70                         // Preescaler 1:128
#define T2CON_CKPS_64                              0x60                         // Preescaler 1: 64
#define T2CON_CKPS_32                              0x50                         // Preescaler 1: 32
#define T2CON_CKPS_16                              0x40                         // Preescaler 1: 16
#define T2CON_CKPS_8                               0x30                         // Preescaler 1:  8
#define T2CON_CKPS_4                               0x20                         // Preescaler 1:  4
#define T2CON_CKPS_2                               0x10                         // Preescaler 1:  2
#define T2CON_CKPS_1                               0x00                         // Preescaler 1:  1

#define T2CON_OUTPS_16                             0x0F                         // Postcaler 1:16
#define T2CON_OUTPS_15                             0x0E                         // Postcaler 1:15
#define T2CON_OUTPS_14                             0x0D                         // Postcaler 1:14
#define T2CON_OUTPS_13                             0x0C                         // Postcaler 1:13
#define T2CON_OUTPS_12                             0x0B                         // Postcaler 1:12
#define T2CON_OUTPS_11                             0x0A                         // Postcaler 1:11
#define T2CON_OUTPS_10                             0x09                         // Postcaler 1:10
#define T2CON_OUTPS_9                              0x08                         // Postcaler 1:9
#define T2CON_OUTPS_8                              0x07                         // Postcaler 1:8
#define T2CON_OUTPS_7                              0x06                         // Postcaler 1:7
#define T2CON_OUTPS_6                              0x05                         // Postcaler 1:6
#define T2CON_OUTPS_5                              0x04                         // Postcaler 1:5
#define T2CON_OUTPS_4                              0x03                         // Postcaler 1:4
#define T2CON_OUTPS_3                              0x02                         // Postcaler 1:3
#define T2CON_OUTPS_2                              0x01                         // Postcaler 1:2
#define T2CON_OUTPS_1                              0x00                         // Postcaler 1:1
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR2 (T2HLT)">

#define T2HLT_PSYNC                             0x80                            // Salida de Preescaler de TM2 esta sincrnizada a FOSC/4
#define T2HLT_CPOL                              0x70                            // Flanco de bajada del clk de entrada aumenta el contador timer/preescaler
#define T2HLT_CSYNC                             0x60                            // El bit de registro ON esta sincrnizado a la entrada TMR2_clk

#define T2HLT_MODE_MONOSTABLE                   0x10                            // TMR2 en modo Monostable (Igual que One Shot, excepto que el bit ON no se limpia y el MTR puede ser reseteado por un evento de Reset externo)

    #define MS_START_RISE                       0x01
    #define MS_START_FALL                       0x02
    #define MS_START_BOTH                       0x03

#define T2HLT_MODE_ONESHOT2                     0x10                            // TMR2 en modo One Short (Una vez desbordado, TMR2 (Bit T2ON) se apaga automáticamente)

    #define OS_START_HIGH_RESET_LOW             0x06
    #define OS_START_LOW_RESET_HIGH             0x07

#define T2HLT_MODE_ONESHOT                      0x08                            // TMR2 en modo One Shot (Una vez desbordado, TMR2 (Bit T2ON) se apaga automáticamente)

    #define OS_SOFTWARE_START                   0x00
    #define OS_EDGESTART_RISE                   0x01    
    #define OS_EDGESTART_FALL                   0x02
    #define OS_EDGESTART_BOTH                   0x03
    #define OS_EDGE_STARTRESET_RISE             0x04
    #define OS_EDGE_STARTRESET_FALL             0x05
    #define OS_START_RISE_RESET_LOW             0x06
    #define OS_START_FALL_RESET_HIGH            0x07

#define T2HLT_MODE_FRP                          0x00                            // TMR2 en modo Free Running Period (Funcionamiento de TMR normal)

    #define FRP_SOFTWARE_GATE                   0x00                            // TMR2 empieza con T2ON = 1 y para con T2ON = 0
    #define FRP_HARDWARE_GATE_HIGH              0x01                            
    #define FRP_HARDWARE_GATE_LOW               0x02
    #define FRP_HARDWARE_RESET_BOTH             0x03
    #define FRP_HARDWARE_RESET_RISE             0x04
    #define FRP_HARDWARE_RESET_FALL             0x05
    #define FRP_HARDWARE_RESET_LOW              0x06
    #define FRP_HARDWARE_RESET_HIGH             0x07

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR2 (T2CLK)">
#define T2CLK_CS_ZCD                           0x08                             // Clock Source = ZCD_OUT          
#define T2CLK_CS_CLKREF                        0x07                             // Clock Source = CLKREF_OUT
#define T2CLK_CS_SOSC                          0x06                             // Clock Source = SOSC
#define T2CLK_CS_MFINTOSC                      0x05                             // Clock Source = MFINTOSC (32 kHz)
#define T2CLK_CS_LFINTOSC                      0x04                             // Clock Source = LFINTOSC
#define T2CLK_CS_HFINTOSC                      0x03                             // Clock Source = HFINTOSC
#define T2CLK_CS_FOSC                          0x02                             // Clock Source = FOSC
#define T2CLK_CS_FOSC4                         0x01                             // Clock Source = FOSC/4
#define T2CLK_CS_T2INPPS                       0x00                             // Clock Source = Pin seleccionado por T2INPPS

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR4 (T4CON0)">

#define T4CON_T4ON                                 0x80                         // Habilita TMR2

#define T4CON_CKPS_128                             0x70                         // Preescaler 1:128
#define T4CON_CKPS_64                              0x60                         // Preescaler 1: 64
#define T4CON_CKPS_32                              0x50                         // Preescaler 1: 32
#define T4CON_CKPS_16                              0x40                         // Preescaler 1: 16
#define T4CON_CKPS_8                               0x30                         // Preescaler 1:  8
#define T4CON_CKPS_4                               0x20                         // Preescaler 1:  4
#define T4CON_CKPS_2                               0x10                         // Preescaler 1:  2
#define T4CON_CKPS_1                               0x00                         // Preescaler 1:  1

#define T4CON_OUTPS_16                             0x0F                         // Postcaler 1:16
#define T4CON_OUTPS_15                             0x0E                         // Postcaler 1:15
#define T4CON_OUTPS_14                             0x0D                         // Postcaler 1:14
#define T4CON_OUTPS_13                             0x0C                         // Postcaler 1:13
#define T4CON_OUTPS_12                             0x0B                         // Postcaler 1:12
#define T4CON_OUTPS_11                             0x0A                         // Postcaler 1:11
#define T4CON_OUTPS_10                             0x09                         // Postcaler 1:10
#define T4CON_OUTPS_9                              0x08                         // Postcaler 1:9
#define T4CON_OUTPS_8                              0x07                         // Postcaler 1:8
#define T4CON_OUTPS_7                              0x06                         // Postcaler 1:7
#define T4CON_OUTPS_6                              0x05                         // Postcaler 1:6
#define T4CON_OUTPS_5                              0x04                         // Postcaler 1:5
#define T4CON_OUTPS_4                              0x03                         // Postcaler 1:4
#define T4CON_OUTPS_3                              0x02                         // Postcaler 1:3
#define T4CON_OUTPS_2                              0x01                         // Postcaler 1:2
#define T4CON_OUTPS_1                              0x00                         // Postcaler 1:1
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR4 (T4HLT)">

#define T4HLT_PSYNC                             0x80                            // Salida de Preescaler de TM2 esta sincrnizada a FOSC/4
#define T4HLT_CPOL                              0x70                            // Flanco de bajada del clk de entrada aumenta el contador timer/preescaler
#define T4HLT_CSYNC                             0x60                            // El bit de registro ON esta sincrnizado a la entrada TMR2_clk

#define T4HLT_MODE_MONOSTABLE                   0x10                            // TMR2 en modo Monostable (Igual que One Shot, excepto que el bit ON no se limpia y el MTR puede ser reseteado por un evento de Reset externo)

    #define MS_START_RISE                       0x01
    #define MS_START_FALL                       0x02
    #define MS_START_BOTH                       0x03

#define T4HLT_MODE_ONESHOT2                     0x10                            // TMR2 en modo One Short (Una vez desbordado, TMR2 (Bit T2ON) se apaga automáticamente)

    #define OS_START_HIGH_RESET_LOW             0x06
    #define OS_START_LOW_RESET_HIGH             0x07

#define T4HLT_MODE_ONESHOT                      0x08                            // TMR2 en modo One Shot (Una vez desbordado, TMR2 (Bit T2ON) se apaga automáticamente)

    #define OS_SOFTWARE_START                   0x00
    #define OS_EDGESTART_RISE                   0x01    
    #define OS_EDGESTART_FALL                   0x02
    #define OS_EDGESTART_BOTH                   0x03
    #define OS_EDGE_STARTRESET_RISE             0x04
    #define OS_EDGE_STARTRESET_FALL             0x05
    #define OS_START_RISE_RESET_LOW             0x06
    #define OS_START_FALL_RESET_HIGH            0x07

#define T4HLT_MODE_FRP                          0x00                            // TMR2 en modo Free Running Period (Funcionamiento de TMR normal)

    #define FRP_SOFTWARE_GATE                   0x00                            // TMR2 empieza con T2ON = 1 y para con T2ON = 0
    #define FRP_HARDWARE_GATE_HIGH              0x01                            
    #define FRP_HARDWARE_GATE_LOW               0x02
    #define FRP_HARDWARE_RESET_BOTH             0x03
    #define FRP_HARDWARE_RESET_RISE             0x04
    #define FRP_HARDWARE_RESET_FALL             0x05
    #define FRP_HARDWARE_RESET_LOW              0x06
    #define FRP_HARDWARE_RESET_HIGH             0x07

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR4 (T4CLK)">
#define T4CLK_CS_ZCD                           0x08                             // Clock Source = ZCD_OUT          
#define T4CLK_CS_CLKREF                        0x07                             // Clock Source = CLKREF_OUT
#define T4CLK_CS_SOSC                          0x06                             // Clock Source = SOSC
#define T4CLK_CS_MFINTOSC                      0x05                             // Clock Source = MFINTOSC (32 kHz)
#define T4CLK_CS_LFINTOSC                      0x04                             // Clock Source = LFINTOSC
#define T4CLK_CS_HFINTOSC                      0x03                             // Clock Source = HFINTOSC
#define T4CLK_CS_FOSC                          0x02                             // Clock Source = FOSC
#define T4CLK_CS_FOSC4                         0x01                             // Clock Source = FOSC/4
#define T4CLK_CS_T2INPPS                       0x00                             // Clock Source = Pin seleccionado por T2INPPS

// </editor-fold>    
    
// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR6 (T6CON0)">

#define T6CON_T6ON                                 0x80                         // Habilita TMR2

#define T6CON_CKPS_128                             0x70                         // Preescaler 1:128
#define T6CON_CKPS_64                              0x60                         // Preescaler 1: 64
#define T6CON_CKPS_32                              0x50                         // Preescaler 1: 32
#define T6CON_CKPS_16                              0x40                         // Preescaler 1: 16
#define T6CON_CKPS_8                               0x30                         // Preescaler 1:  8
#define T6CON_CKPS_4                               0x20                         // Preescaler 1:  4
#define T6CON_CKPS_2                               0x10                         // Preescaler 1:  2
#define T6CON_CKPS_1                               0x00                         // Preescaler 1:  1

#define T6CON_OUTPS_16                             0x0F                         // Postcaler 1:16
#define T6CON_OUTPS_15                             0x0E                         // Postcaler 1:15
#define T6CON_OUTPS_14                             0x0D                         // Postcaler 1:14
#define T6CON_OUTPS_13                             0x0C                         // Postcaler 1:13
#define T6CON_OUTPS_12                             0x0B                         // Postcaler 1:12
#define T6CON_OUTPS_11                             0x0A                         // Postcaler 1:11
#define T6CON_OUTPS_10                             0x09                         // Postcaler 1:10
#define T6CON_OUTPS_9                              0x08                         // Postcaler 1:9
#define T6CON_OUTPS_8                              0x07                         // Postcaler 1:8
#define T6CON_OUTPS_7                              0x06                         // Postcaler 1:7
#define T6CON_OUTPS_6                              0x05                         // Postcaler 1:6
#define T6CON_OUTPS_5                              0x04                         // Postcaler 1:5
#define T6CON_OUTPS_4                              0x03                         // Postcaler 1:4
#define T6CON_OUTPS_3                              0x02                         // Postcaler 1:3
#define T6CON_OUTPS_2                              0x01                         // Postcaler 1:2
#define T6CON_OUTPS_1                              0x00                         // Postcaler 1:1
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR6 (T6HLT)">

#define T6HLT_PSYNC                             0x80                            // Salida de Preescaler de TM2 esta sincrnizada a FOSC/4
#define T6HLT_CPOL                              0x70                            // Flanco de bajada del clk de entrada aumenta el contador timer/preescaler
#define T6HLT_CSYNC                             0x60                            // El bit de registro ON esta sincrnizado a la entrada TMR2_clk

#define T6HLT_MODE_MONOSTABLE                   0x10                            // TMR2 en modo Monostable (Igual que One Shot, excepto que el bit ON no se limpia y el MTR puede ser reseteado por un evento de Reset externo)

    #define MS_START_RISE                       0x01
    #define MS_START_FALL                       0x02
    #define MS_START_BOTH                       0x03

#define T6HLT_MODE_ONESHOT2                     0x10                            // TMR2 en modo One Short (Una vez desbordado, TMR2 (Bit T2ON) se apaga automáticamente)

    #define OS_START_HIGH_RESET_LOW             0x06
    #define OS_START_LOW_RESET_HIGH             0x07

#define T6HLT_MODE_ONESHOT                      0x08                            // TMR2 en modo One Shot (Una vez desbordado, TMR2 (Bit T2ON) se apaga automáticamente)

    #define OS_SOFTWARE_START                   0x00
    #define OS_EDGESTART_RISE                   0x01    
    #define OS_EDGESTART_FALL                   0x02
    #define OS_EDGESTART_BOTH                   0x03
    #define OS_EDGE_STARTRESET_RISE             0x04
    #define OS_EDGE_STARTRESET_FALL             0x05
    #define OS_START_RISE_RESET_LOW             0x06
    #define OS_START_FALL_RESET_HIGH            0x07

#define T6HLT_MODE_FRP                          0x00                            // TMR2 en modo Free Running Period (Funcionamiento de TMR normal)

    #define FRP_SOFTWARE_GATE                   0x00                            // TMR2 empieza con T2ON = 1 y para con T2ON = 0
    #define FRP_HARDWARE_GATE_HIGH              0x01                            
    #define FRP_HARDWARE_GATE_LOW               0x02
    #define FRP_HARDWARE_RESET_BOTH             0x03
    #define FRP_HARDWARE_RESET_RISE             0x04
    #define FRP_HARDWARE_RESET_FALL             0x05
    #define FRP_HARDWARE_RESET_LOW              0x06
    #define FRP_HARDWARE_RESET_HIGH             0x07

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de configuración para TMR6 (T6CLK)">
#define T6CLK_CS_ZCD                           0x08                             // Clock Source = ZCD_OUT          
#define T6CLK_CS_CLKREF                        0x07                             // Clock Source = CLKREF_OUT
#define T6CLK_CS_SOSC                          0x06                             // Clock Source = SOSC
#define T6CLK_CS_MFINTOSC                      0x05                             // Clock Source = MFINTOSC (32 kHz)
#define T6CLK_CS_LFINTOSC                      0x04                             // Clock Source = LFINTOSC
#define T6CLK_CS_HFINTOSC                      0x03                             // Clock Source = HFINTOSC
#define T6CLK_CS_FOSC                          0x02                             // Clock Source = FOSC
#define T6CLK_CS_FOSC4                         0x01                             // Clock Source = FOSC/4
#define T6CLK_CS_T2INPPS                       0x00                             // Clock Source = Pin seleccionado por T2INPPS

// </editor-fold>        
    
// <editor-fold defaultstate="collapsed" desc="Macros de activación/desactivación de TMRs">
//Macros para habilitar los timers
#define enableTMR0()                           T0CON0 |= T0CON0_T0EN
#define disableTMR0()                          T0CON0 &= (~T0CON0_T0EN); \
                                               TMR0H = 0x00; \
                                               TMR0L = 0x00

#define enableTMR2()                           T2CON  |= T2CON_T2ON
#define disableTMR2()                          T2CON  &= (~T2CON_T2ON); \
                                               PR2 = 0x00
    
#define enableTMR4()                           T4CON  |= T4CON_T4ON
#define disableTMR4()                          T4CON  &= (~T4CON_T4ON); \
                                               PR4 = 0x00
    
#define enableTMR6()                           T6CON  |= T6CON_T6ON
#define disableTMR6()                          T6CON  &= (~T6CON_T6ON); \
                                               PR6 = 0x00
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de activación/desactivación de ISR de TMRs">
//Macros para habilitar las interrupciones
#define enableTMR0Interrupt()                  PIE0bits.TMR0IE = 1
#define enableTMR1Interrupt()                  PIE4bits.TMR1IE = 1
#define enableTMR2Interrupt()                  PIE4bits.TMR2IE = 1
#define enableTMR3Interrupt()                  PIE4bits.TMR3IE = 1
#define enableTMR4Interrupt()                  PIE4bits.TMR4IE = 1
#define enableTMR5Interrupt()                  PIE4bits.TMR5IE = 1
#define enableTMR6Interrupt()                  PIE4bits.TMR6IE = 1
    
//Macros para desabilitar las interrupciones
#define disableTMR0Interrupt()                 PIE0bits.TMR0IE = 0
#define disableTMR1Interrupt()                 PIE4bits.TMR1IE = 0
#define disableTMR2Interrupt()                 PIE4bits.TMR2IE = 0
#define disableTMR3Interrupt()                 PIE4bits.TMR3IE = 0
#define disableTMR4Interrupt()                 PIE4bits.TMR4IE = 0
#define disableTMR5Interrupt()                 PIE4bits.TMR5IE = 0
#define disableTMR6Interrupt()                 PIE4bits.TMR6IE = 0
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de uso general">
// Macros para recargas de timer
#define TMR0_RELOAD                            0xB1DF
#define TMR1_RELOAD                            0x00
#define TMR2_RELOAD                            0xFA
#define TMR3_RELOAD                            0x00
#define TMR4_RELOAD                            0x00
#define TMR5_RELOAD                            0x00
#define TMR6_RELOAD                            0x00

// </editor-fold>

/******************************************************************************/
    
/*****************************ENUMS/STRUCTS/UNIONS*****************************/

// NA
    
/******************************************************************************/

/*********************************FUNCIONES************************************/

// <editor-fold defaultstate="collapsed" desc="Funciones públicas (Uso general)">
/* ************************************************************************
 * tmr0_config
 * @Description
 * Función que inicializa TMR0
 * @Preconditions
 * NA
 * @Param
 * t0con0           - Configuraciones deseadas para registro T0CON0 (Ver macros)
 * t0con1           - Configuraciones deseadas para registro T0CON1 (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr0_config(T0CON0_T016bit | T0CON0_T0OUTPS_1,
               T0CON1_T0CS_HFINTOSC | T0CON1_T0CKPS_16);
 * ************************************************************************/
void tmr0_config(uint8_t t0con0, uint8_t t0con1);

/* ************************************************************************
 * tmr2_config
 * @Description
 * Función que inicializa TMR2
 * @Preconditions
 * NA
 * @Param
 * t2con            - Configuraciones deseadas para registro T2CON (Ver macros)
 * t2hlt            - Configuraciones deseadas para registro T2HLT (Ver macros)
 * t2clk            - Configuraciones deseadas para registro T2CLK (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr2_config(T2CON_CKPS_32 | T2CON_OUTPS_1,
               T2HLT_MODE_FRP | FRP_SOFTWARE_GATE,
               T2CLK_CS_FOSC4);
 * ************************************************************************/
void tmr2_config(uint8_t t2con, uint8_t t2hlt, uint8_t t2clk);

/* ************************************************************************
 * tmr4_config
 * @Description
 * Función que inicializa TMR4
 * @Preconditions
 * NA
 * @Param
 * t4con            - Configuraciones deseadas para registro T4CON (Ver macros)
 * t4hlt            - Configuraciones deseadas para registro T4HLT (Ver macros)
 * t4clk            - Configuraciones deseadas para registro T4CLK (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr4_config(T4CON_CKPS_32 | T2CON_OUTPS_1,
               T4HLT_MODE_FRP | FRP_SOFTWARE_GATE,
               T4CLK_CS_FOSC4);
 * ************************************************************************/
void tmr4_config(uint8_t t4con, uint8_t t4hlt, uint8_t t4clk);

/* ************************************************************************
 * tmr6_config
 * @Description
 * Función que inicializa TMR6
 * @Preconditions
 * NA
 * @Param
 * t6con            - Configuraciones deseadas para registro T6CON (Ver macros)
 * t6hlt            - Configuraciones deseadas para registro T6HLT (Ver macros)
 * t6clk            - Configuraciones deseadas para registro T6CLK (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr6_config(T6CON_CKPS_32 | T6CON_OUTPS_1,
               T6HLT_MODE_FRP | FRP_SOFTWARE_GATE,
               T6CLK_CS_FOSC4);
 * ************************************************************************/
void tmr6_config(uint8_t t6con, uint8_t t6hlt, uint8_t t6clk);

/* ************************************************************************
 * tmr0_reload
 * @Description
 * Función que recarga registros de tiempo de TMR0
 * @Preconditions
 * Configurar TMR0
 * @Param
 * reload           - Tiempo de recarga deseado (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr0_reload(TMR0_RELOAD);
 * ************************************************************************/
inline void tmr0_reload(uint16_t reload);

/* ************************************************************************
 * tmr2_reload
 * @Description
 * Función que recarga registros de tiempo de TMR2
 * @Preconditions
 * Configurar TMR2
 * @Param
 * reload           - Tiempo de recarga deseado (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr2_reload(TMR2_RELOAD);
 * ************************************************************************/
inline void tmr2_reload(uint16_t reload);

/* ************************************************************************
 * tmr4_reload
 * @Description
 * Función que recarga registros de tiempo de TMR4
 * @Preconditions
 * Configurar TMR4
 * @Param
 * reload           - Tiempo de recarga deseado (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr4_reload(TMR4_RELOAD);
 * ************************************************************************/
inline void tmr4_reload(uint16_t reload);

/* ************************************************************************
 * tmr6_reload
 * @Description
 * Función que recarga registros de tiempo de TMR6
 * @Preconditions
 * Configurar TMR6
 * @Param
 * reload           - Tiempo de recarga deseado (Ver macros)
 * @Returns
 * NA
 * @Example
 * tmr6_reload(TMR6_RELOAD);
 * ************************************************************************/
inline void tmr6_reload(uint16_t reload);
 // </editor-fold>

/******************************************************************************/
#ifdef	__cplusplus
}
#endif

#endif	/* TIMERS_H */
