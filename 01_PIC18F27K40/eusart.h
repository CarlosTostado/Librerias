#ifndef _EUSART_H
#define _EUSART_H

#include <xc.h> 
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#define EUSART1_TX_BUFFER_SIZE              0x10                                // Tama�o de Buffer para TX
#define EUSART2_TX_BUFFER_SIZE              0x10                                // Tama�o de Buffer para TX
    
#define EUSART1_SELECT                      0x01                                // Macro de selecci�n para EUSART1
#define EUSART2_SELECT                      0x02                                // Macro de selecci�n para EUSART2
#define BAUD_2400                           0xD04                               // Macro para valor necesario para registro SPBRG (3332) a 2400 baudios
#define BAUD_9600                           0x340                               // Macro para valor necesario para registro SPBRG (832) a 9600 baudios
#define BAUD_19200                          0x1A0                               // Macro para valor necesario para registro SPBRG (416) a 19200 baudios
#define BAUD_57600                          0x8A                                // Macro para valor necesario para registro SPBRG (138) a 57600 baudios
#define BAUD_115200                         0x44                                // Macro para valor necesario para registro SPBRG (68) a 115200 baudios
    
#define TXxSTA_CSRC_MASTER                  0x80                                // Modo maestro (CLK generado internamente de BRG) - Solo en modo Asincrono
#define TXxSTA_CSRC_SLAVE                   0x00                                // Modo esclavo (CLK de fuente externa) - Solo en modo Sincrono
#define TXxSTA_TX9_9BIT                     0x40                                // Selecciona transmisi�n de 9 bits
#define TXxSTA_TX9_8BIT                     0x00                                // Selecciona transmisi�n de 8 bits
#define TXxSTA_TXEN_ON                      0x20                                // Habilita transmisi�n
#define TXxSTA_TXEN_OFF                     0x00                                // Deshabilita transmisi�n
#define TXxSTA_SYNC_SYNC                    0x10                                // EUSART modo Sincrono
#define TXxSTA_SYNC_ASYNC                   0x00                                // EUSART modo Asincrono
#define TXxSTA_SENDB_ON                     0x08                                // Env�o de Syn Break en la siguiente transmisi�n
#define TXxSTA_SENDB_OFF                    0x00                                // Transmisi�n de sync break deshabilitada
#define TXxSTA_BRGH_HSPEED                  0x04                                // BRGH en velocidad alta
#define TXxSTA_BRGH_LSPEED                  0x00                                // BRGH en velocidad baja
    
#define RCxSTA_SPEN_ON                      0x80                                // Habilita puerto serial
#define RCxSTA_SPEN_OFF                     0x00                                // Deshabilita puerto serial
#define RCxSTA_RX9_9BIT                     0x40                                // Recepci�n de 9 bits
#define RCxSTA_RX9_8BIT                     0x00                                // Recepci�n de 8 bits
#define RCxSTA_SREN_ON                      0x20                                // Habilita receic�n singular
#define RCxSTA_SREN_OFF                     0x00                                // Deshabilita recepci�n singular
#define RCxSTA_CREN_ON                      0x10                                // Habilita recepci�n continua
#define RCxSTA_CREN_OFF                     0x00                                // DFeshabilita recepci�n continua
#define RCxSTA_ADDEN_ON                     0x08                                // Habilita detecci�n de direcci�n
#define RCxSTA_ADDEN_OFF                    0x00                                // Deshabilita detecci�n de direcci�n, todos los bytes son recibidos y el 9no es el de paridad
    
#define BAUDxCON_SCKP_TXINV                 0x10                                // Transmisi�n es invertida
#define BAUDxCON_SCKP_TXNINV                0x00                                // Transmisi�n es no invertida
#define BAUDxCON_BRG16_16BIT                0x08                                // Generador de 16 bits (baudios)
#define BAUDxCON_BRG16_8BIT                 0x00                                // Generador de 8 bits (baudios)
#define BAUDxCON_WUE_FEDGE                  0x02                                // El recepto esta esperando un flanco de bajada. 
#define BAUDxCON_WUE_NORMAL                 0x00                                // El receptor opera normalmente
#define BAUDxCON_ABDEN_ON                   0x01                                // Modo de detecci�n autom�tica de baudios habilitado (Se limpia cuando la detecci�n se completa))
#define BAUDxCON_ABDEN_OFF                  0x00                                // Modo de detecci�n autom�tica de baudios deshabilitado
    
#define enableEUSART1()                     TX1STAbits.TXEN |= TXxSTA_TXEN_ON; \
                                            RC1STAbits.CREN |= RCxSTA_CREN_ON
#define enableEUSART2()                     TX2STAbits.TXEN |= TXxSTA_TXEN_ON; \
                                            RC2STAbits.CREN |= RCxSTA_CREN_ON

#define disableEUSART1()                    TX1STAbits.TXEN &= (~TXxSTA_TXEN_OFF); \
                                            RC1STAbits.CREN &= (~RCxSTA_CREN_OFF)
#define disableEUSART2()                    TX2STAbits.TXEN &= (~TXxSTA_TXEN_OFF); \
                                            RC2STAbits.CREN &= (~RCxSTA_CREN_OFF)    
    
#define enableEUSART1TXInterrupt()          PIE3bits.TX1IE = 1
#define disableEUSART1TXInterrupt()         PIE3bits.TX1IE = 0
#define enableEUSART1RXInterrupt()          PIE3bits.RC1IE = 1
#define disableEUSART1RXInterrupt()         PIE3bits.RC1IE = 0
    
#define enableEUSART2TXInterrupt()          PIE3bits.TX2IE = 1         
#define disableEUSART2TXInterrupt()         PIE3bits.TX2IE = 0
#define enableEUSART2RXInterrupt()          PIE3bits.RC2IE = 1
#define disableEUSART2RXInterrupt()         PIE3bits.RC2IE = 0
    
void eusart_config(uint8_t eusart_select, uint8_t txxsta, uint8_t rcxsta,
                   uint8_t baudxcon);    
void eusart_baud(uint8_t eusart_select, uint32_t baudios);
void EUSART1_WriteInterrupt(uint8_t txData);
void inline EUSART1_Transmit_ISR(void);
uint8_t EUSART1_Read();
void EUSART2_WriteInterrupt(uint8_t txData);
void inline EUSART2_Transmit_ISR(void);
uint8_t EUSART2_Read();

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

