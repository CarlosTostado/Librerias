/*
 * File:        adc.c
 * Author:      Ing. Carlos Tostado
 * Version:     1.0
 * Fecha:       25 de mayo de 2017
 * Libreria para manejo de adc en pic18f24K40
 * 
 * File:        adc.c
 * Author:      Ing. Rodrigo Alvarado
 * Versi�n:     2.0
 * Fecha:       19 de Diciembre de 2017
 * Librer�a para manejo de ADC adaptado para microcontrolador PIC18F27K40
 */
 
#include "adc.h"
 
#define _XTAL_FREQ 32000000
 
void adc_config (uint8_t adcon0, uint8_t adcclk, uint8_t adcref, uint8_t adcacq)
{
    ADCON0 = 0x00 | adcon0;
    ADCON1 = 0x00;
    ADCON2 = 0x00;
    ADCON3 = 0x00;

    ADCLK  = 0x00 | adcclk;                        
    ADREF  = 0x00 | adcref;     
    ADPRE  = 0x00;                                                              
    ADACQ  = 0x00 | adcacq;                                                              
    ADCAP  = 0x00;                                                             
}
 
void adc_enable (uint8_t canal)
{
    enableADC();
    ADPCH = ADPCH_AVSS;                                                            //Recomendacion datasheet seccion 31.1.2
    __delay_us(10);                                                             //Delay para el cambio de canal
    ADPCH = canal;
    __delay_us(10);                                                             //Delay para el cambio de canal
}

void adc_disable ()
{
    disableADC();
}
 
uint16_t adc_read_promedio(void)
{
    uint16_t dato_prom = 0;
    uint16_t dato_adc  = 0;
    uint8_t promedio   = 0;
    
    do
    {
        ADCON0bits.GO = 1;
        while(ADCON0bits.GO);

        if (!ADCON0bits.ADFM)
        {     
            dato_prom = ADRESH << 8;
            dato_prom |= ADRESL;
            dato_prom >>= 6;
        }
        else
        {
            dato_prom = ADRESH << 8;
            dato_prom |= ADRESL;
        }
        dato_adc = dato_adc + dato_prom;
        promedio++;
    }while(promedio < PROMEDIO_ADC);
    
    promedio = 0;
    dato_adc /= PROMEDIO_ADC;
    return dato_adc;
}

uint16_t adc_read(void)
{
    uint16_t dato = 0;
    ADCON0bits.GO = 1;
    while(ADCON0bits.GO);
    
    if(!ADCON0bits.ADFM)
    {
        dato = ADRESH << 8;
        dato |= ADRESL;
        dato >>= 6;
    }
    else
    {
        dato = ADRESH << 8;
        dato |= ADRESL;
    }
    
    return dato;
}
