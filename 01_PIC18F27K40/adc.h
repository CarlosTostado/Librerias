/*
 * File:        adc.c
 * Author:      Ing. Carlos Tostado
 * Version:     1.0
 * Fecha:       25 de mayo de 2017
 * Libreria para manejo de adc en pic18f24K40
 * 
 * File:        adc.c
 * Author:      Ing. Rodrigo Alvarado
 * Versi�n:     2.0
 * Fecha:       19 de Diciembre de 2017
 * Librer�a para manejo de ADC adaptado para microcontrolador PIC18F27K40
 */
 
#ifndef ADC_H
#define ADC_H

#ifdef  __cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>
    
/********************************* MACROS *************************************/

// <editor-fold defaultstate="collapsed" desc="Macros para configuraciones ADCON0">
#define ADCON0_ADON                               0x80                          // Perif�rico ADC habilitado
#define ADCON0_ADCONT                             0x40                          // El bit ADGO se vuelve a levantar autom�ticamente despu�s de completar una conversi�n
#define ADCON0_ADCS                               0x10                          // El clock se obtiene de un oscilador FRC (Clock se obtiene de FOSC dividido por el registro ADCLK si no se activa este bit)
#define ADCON0_ADFM_RIGHT                         0x04                          // La conversi�n esta justificada a la derecha (ADRESL 0:7, ADRESH 8:9)
#define ADCON0_ADFM_LEFT                          0x00                          // La conversi�n esta justificada a la izquierda (ADRESL 0:1, ADRESH 2:9)
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros para configuraciones ADCLK">
#define ADCLK_FOSC128                             0x3F                          // Divisor de 128 para FOSC
#define ADCLK_FOSC126                             0x3E                          // Divisor de 126 para FOSC
#define ADCLK_FOSC124                             0x3D                          // Divisor de 124 para FOSC
#define ADCLK_FOSC122                             0x3C                          // Divisor de 122 para FOSC
#define ADCLK_FOSC120                             0x3B                          // Divisor de 120 para FOSC
#define ADCLK_FOSC118                             0x3A                          // Divisor de 118 para FOSC
#define ADCLK_FOSC116                             0x39                          // Divisor de 116 para FOSC
#define ADCLK_FOSC114                             0x38                          // Divisor de 114 para FOSC
#define ADCLK_FOSC112                             0x37                          // Divisor de 112 para FOSC
#define ADCLK_FOSC110                             0x36                          // Divisor de 110 para FOSC
#define ADCLK_FOSC108                             0x35                          // Divisor de 108 para FOSC
#define ADCLK_FOSC106                             0x34                          // Divisor de 106 para FOSC
#define ADCLK_FOSC104                             0x33                          // Divisor de 104 para FOSC
#define ADCLK_FOSC102                             0x32                          // Divisor de 102 para FOSC
#define ADCLK_FOSC100                             0x31                          // Divisor de 100 para FOSC
#define ADCLK_FOSC98                              0x30                          // Divisor de 98 para FOSC
#define ADCLK_FOSC96                              0x2F                          // Divisor de 96 para FOSC
#define ADCLK_FOSC94                              0x2E                          // Divisor de 94 para FOSC
#define ADCLK_FOSC92                              0x2D                          // Divisor de 92 para FOSC
#define ADCLK_FOSC90                              0x2C                          // Divisor de 90 para FOSC
#define ADCLK_FOSC88                              0x2B                          // Divisor de 88 para FOSC
#define ADCLK_FOSC86                              0x2A                          // Divisor de 86 para FOSC
#define ADCLK_FOSC84                              0x29                          // Divisor de 84 para FOSC
#define ADCLK_FOSC82                              0x28                          // Divisor de 82 para FOSC
#define ADCLK_FOSC80                              0x27                          // Divisor de 80 para FOSC
#define ADCLK_FOSC78                              0x26                          // Divisor de 78 para FOSC
#define ADCLK_FOSC76                              0x25                          // Divisor de 76 para FOSC
#define ADCLK_FOSC74                              0x24                          // Divisor de 74 para FOSC
#define ADCLK_FOSC72                              0x23                          // Divisor de 72 para FOSC
#define ADCLK_FOSC70                              0x22                          // Divisor de 70 para FOSC
#define ADCLK_FOSC68                              0x21                          // Divisor de 68 para FOSC
#define ADCLK_FOSC66                              0x20                          // Divisor de 66 para FOSC
#define ADCLK_FOSC64                              0x1F                          // Divisor de 64 para FOSC
#define ADCLK_FOSC62                              0x1E                          // Divisor de 62 para FOSC
#define ADCLK_FOSC60                              0x1D                          // Divisor de 60 para FOSC
#define ADCLK_FOSC58                              0x1C                          // Divisor de 58 para FOSC
#define ADCLK_FOSC56                              0x1B                          // Divisor de 56 para FOSC
#define ADCLK_FOSC54                              0x1A                          // Divisor de 54 para FOSC
#define ADCLK_FOSC52                              0x19                          // Divisor de 52 para FOSC
#define ADCLK_FOSC50                              0x18                          // Divisor de 50 para FOSC
#define ADCLK_FOSC48                              0x17                          // Divisor de 48 para FOSC
#define ADCLK_FOSC46                              0x16                          // Divisor de 46 para FOSC
#define ADCLK_FOSC44                              0x15                          // Divisor de 44 para FOSC
#define ADCLK_FOSC42                              0x14                          // Divisor de 42 para FOSC
#define ADCLK_FOSC40                              0x13                          // Divisor de 40 para FOSC
#define ADCLK_FOSC38                              0x12                          // Divisor de 38 para FOSC
#define ADCLK_FOSC36                              0x11                          // Divisor de 36 para FOSC
#define ADCLK_FOSC34                              0x10                          // Divisor de 34 para FOSC
#define ADCLK_FOSC32                              0x0F                          // Divisor de 32 para FOSC
#define ADCLK_FOSC30                              0x0E                          // Divisor de 30 para FOSC
#define ADCLK_FOSC28                              0x0D                          // Divisor de 28 para FOSC
#define ADCLK_FOSC26                              0x0C                          // Divisor de 26 para FOSC
#define ADCLK_FOSC24                              0x0B                          // Divisor de 24 para FOSC
#define ADCLK_FOSC22                              0x0A                          // Divisor de 22 para FOSC
#define ADCLK_FOSC20                              0x09                          // Divisor de 20 para FOSC
#define ADCLK_FOSC18                              0x08                          // Divisor de 18 para FOSC
#define ADCLK_FOSC16                              0x07                          // Divisor de 16 para FOSC
#define ADCLK_FOSC14                              0x06                          // Divisor de 14 para FOSC
#define ADCLK_FOSC12                              0x05                          // Divisor de 12 para FOSC
#define ADCLK_FOSC10                              0x04                          // Divisor de 10 para FOSC
#define ADCLK_FOSC8                               0x03                          // Divisor de 8 para FOSC
#define ADCLK_FOSC6                               0x02                          // Divisor de 6 para FOSC
#define ADCLK_FOSC4                               0x01                          // Divisor de 4 para FOSC
#define ADCLK_FOSC2                               0x00                          // Divisor de 2 para FOSC
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros para configuraciones ADREF">
#define ADREF_ADNREF_VSS                          0x00                          // Voltaje de referencia negativo a Vss
#define ADREF_ADNREF_VREF                         0x10                          // Voltaje de referencia negativo a Vref-
#define ADREF_ADPREF_FVR                          0x03                          // Voltaje de referencia positivo a FVR
#define ADREF_ADPREF_VREF                         0x02                          // Voltaje de referencia positivo a Vref+
#define ADREF_ADPREF_VDD                          0x00                          // Voltaje de referencia positivo a Vdd
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros para configuraciones de ADACQ">
#define ADACQ_ACQ0                                0x00                          // Tiempo de adquisici�n no incluido
#define ADACQ_ACQ1                                0x01                          // Tiempo de adquisici�n de 1 TAD
#define ADACQ_ACQ2                                0x02                          // Tiempo de adquisici�n de 2 TADs
#define ADACQ_ACQ3                                0x03                          // Tiempo de adquisici�n de 3 TADs
#define ADACQ_ACQ4                                0x04                          // Tiempo de adquisici�n de 4 TADs
#define ADACQ_ACQ5                                0x05                          // Tiempo de adquisici�n de 5 TADs
#define ADACQ_ACQ6                                0x06                          // Tiempo de adquisici�n de 6 TADs
#define ADACQ_ACQ7                                0x07                          // Tiempo de adquisici�n de 7 TADs
#define ADACQ_ACQ8                                0x08                          // Tiempo de adquisici�n de 8 TADs
#define ADACQ_ACQ9                                0x09                          // Tiempo de adquisici�n de 9 TADs
#define ADACQ_ACQ10                               0x0A                          // Tiempo de adquisici�n de 10 TADs
#define ADACQ_ACQ11                               0x0B                          // Tiempo de adquisici�n de 11 TADs
#define ADACQ_ACQ12                               0x0C                          // Tiempo de adquisici�n de 12 TADs
#define ADACQ_ACQ13                               0x0D                          // Tiempo de adquisici�n de 13 TADs
#define ADACQ_ACQ14                               0x0E                          // Tiempo de adquisici�n de 14 TADs
#define ADACQ_ACQ15                               0x0F                          // Tiempo de adquisici�n de 15 TADs
#define ADACQ_ACQ16                               0x10                          // Tiempo de adquisici�n de 16 TADs
#define ADACQ_ACQ17                               0x11                          // Tiempo de adquisici�n de 17 TADs
#define ADACQ_ACQ18                               0x12                          // Tiempo de adquisici�n de 18 TADs
#define ADACQ_ACQ19                               0x13                          // Tiempo de adquisici�n de 19 TADs
#define ADACQ_ACQ20                               0x14                          // Tiempo de adquisici�n de 20 TADs
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros para configuraciones ADPCH">
#define ADPCH_FVR                                  0b111111                     // Convierte voltaje en FVR
#define ADPCH_DAC1                                 0b111110                     // Salida DAC1
#define ADPCH_TEMP                                 0b111101                     // Indicador de temperatura
#define ADPCH_AVSS                                 0b111100                     // AVss
#define ADPCH_ANC7                                 0b010111                     // ANAC7
#define ADPCH_ANC6                                 0b010110                     // ANAC6
#define ADPCH_ANC5                                 0b010101                     // ANAC5
#define ADPCH_ANC4                                 0b010100                     // ANAC4
#define ADPCH_ANC3                                 0b010011                     // ANAC3
#define ADPCH_ANC2                                 0b010010                     // ANAC2
#define ADPCH_ANC1                                 0b010001                     // ANAC1
#define ADPCH_ANC0                                 0b010000                     // ANAC0
#define ADPCH_ANB7                                 0b001111                     // ANAB7
#define ADPCH_ANB6                                 0b001110                     // ANAB6
#define ADPCH_ANB5                                 0b001101                     // ANAB5
#define ADPCH_ANB4                                 0b001100                     // ANAB4
#define ADPCH_ANB3                                 0b001011                     // ANAB3
#define ADPCH_ANB2                                 0b001010                     // ANAB2
#define ADPCH_ANB1                                 0b001001                     // ANAB1
#define ADPCH_ANB0                                 0b001000                     // ANAB0
#define ADPCH_ANA7                                 0b000111                     // ANAA7
#define ADPCH_ANA6                                 0b000110                     // ANAA6
#define ADPCH_ANA5                                 0b000101                     // ANAA5
#define ADPCH_ANA4                                 0b000100                     // ANAA4
#define ADPCH_ANA3                                 0b000011                     // ANAA3
#define ADPCH_ANA2                                 0b000010                     // ANAA2
#define ADPCH_ANA1                                 0b000001                     // ANAA1
#define ADPCH_ANA0                                 0b000000                     // ANAA0
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros para habilitar/deshabilitar ADC">
#define enableADC()                             ADCON0 |= ADCON0_ADON
#define disableADC()                            ADCON0 &= (~ADCON0_ADON)
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Macros de uso general">
// Macro para promedio de lectura ADC
#define PROMEDIO_ADC                            10
// </editor-fold>
    
/******************************************************************************/
    
/*****************************ENUMS/STRUCTS/UNIONS*****************************/

// NA
    
/******************************************************************************/

/*********************************FUNCIONES************************************/
    
// <editor-fold defaultstate="collapsed" desc="Funciones p�blicas (Uso general)">
/* ************************************************************************
 * adc_config
 * @Description
 * Funci�n que inicializa el perif�rico ADC
 * @Preconditions
 * NA
 * @Param
 * adcon0        - Configuraciones deseadas para registro ADCON0 (Ver macros)
 * adcclk        - Configuraciones deseadas para registro ADCCLK (Ver macros)
 * adcref        - Configuraciones deseadas para registro ADCREF (Ver macros)
 * adcacq        - Configuraciones deseadas para registro ADCACQ (Ver macros)
 * @Returns
 * NA
 * @Example
 * adc_config(ADCON0_ADFM_RIGHT, ADCLK_FOSC16M
             ADREF_ADPREF_VDD | ADREF_ADNREF_VSSM
             ADACQ_ACQ10);
 * ************************************************************************/
void adc_config(uint8_t adcon0, uint8_t adcclk, uint8_t adcref, uint8_t adcacq);

/* ************************************************************************
 * adc_enable
 * @Description
 * Funci�n que habilita el perif�rico ADC
 * @Preconditions
 * Configurar perif�rico ADC
 * @Param
 * canal         - Canal deseado de conversi�n ADC (Ver macros)
 * @Returns
 * NA
 * @Example
 * adc_enable(ADPCH_FVR);
 * ************************************************************************/
void adc_enable(uint8_t canal);

/* ************************************************************************
 * adc_disable
 * @Description
 * Funci�n que deshabilita el m�dulo ADC
 * @Preconditions
 * NA
 * @Param
 * NA
 * @Returns
 * NA
 * @Example
 * adc_enable();
 * ************************************************************************/
void adc_disable(void);

/* ************************************************************************
 * adc_read_promedio
 * @Description
 * Funci�n que hace lecturas ADC en base a un promedio
 * @Preconditions
 * Configurar y habilitar perif�rico ADC y definir macro PROMEDIO_ADC
 * @Param
 * NA
 * @Returns
 * Valor d�gital de conversi�n ADC en promedio seleccionable (Ver macro)
 * @Example
 * #define PROMEDIO_ADC 5
 * x = adc_read_promedio();
 * ************************************************************************/
uint16_t adc_read_promedio(void);

/* ************************************************************************
 * adc_read
 * @Description
 * Funci�n que hace una lectura ADC �nica 
 * @Preconditions
 * Configurar y habilitar perif�rico ADC
 * @Param
 * NA
 * @Returns
 * Valor d�gital de conversi�n ADC
 * @Example
 * #define PROMEDIO_ADC 5
 * x = adc_read_promedio();
 * ************************************************************************/
uint16_t adc_read(void); 
// </editor-fold>

/******************************************************************************/ 

#ifdef  __cplusplus
}
#endif
 
#endif  /* ADC_H */