/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     timers.hh
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Mayo 22, 2017
 * Version:     1.0
 * 
 * Librer�a para manejo de TMRs en PIC18F24K40
 * 
 * Archivo:     timers.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptaci�n a est�ndar de librer�as y adaptaci�n a PIC18F27K40
 */

#include "timers.h"

/* ****************** Formula para TMR0 de 8 bits ******************************
 *
 * Formula: Tiempo = 4/CLOCKSOURCE x Prescaler x (255 - TMR) x 1/POSTCALER
 * 
 * ****************************************************************************/

/* ****************** Formula para TMR0 de 16 bits *****************************
 *
 * Formula: Tiempo = 4/CLOCKSOURCE x Prescaler x (65535 - TMR) x 1/POSTCALER
 * 
 * ****************************************************************************/

inline void tmr0_reload(uint16_t reload)
{
    TMR0H = (uint8_t) ((0xFF00 & reload) >> 8);
    TMR0L = (uint8_t) (reload & 0x00FF); 
}   

void tmr0_config(uint8_t t0con0, uint8_t t0con1)
{    
    T0CON0 = 0x00 | t0con0;
    T0CON1 = 0x00 | t0con1;
}

inline void tmr2_reload(uint16_t reload)
{
    PR2 = reload;
    TMR2 = 0x00;
}

void tmr2_config(uint8_t t2con, uint8_t t2hlt, uint8_t t2clk)
{
    T2CON = 0x00 | t2con;
    T2HLT = 0x00 | t2hlt;
    T2CLK = 0x00 | t2clk;
}