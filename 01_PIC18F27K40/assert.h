/* 
 * Empresa:     Departamento i+I+D
 * Archivo:     assert.h
 * Autor:       Ing. Carlos Tostado
 * Fecha:       Marzo 24, 2017
 * Version:     1.0
 * 
 * Archivo:     assert.h
 * Autor:       Ing. Rodrigo Alvarado
 * Fecha:       Diciembre 20, 2017
 * Version:     2.0
 * 
 * Adaptaci�n a est�ndar de librer�as
 */

#ifndef ASSERT_H
#define	ASSERT_H

#ifdef __cplusplus
extern "C" {
#endif  

#include <xc.h>
#include <stdint.h>
    
/*********************************FUNCIONES************************************/

// <editor-fold defaultstate="collapsed" desc="Funciones locales (Uso prohibido)">
void aFailed(uint16_t File_Num, uint16_t line);
// </editor-fold>

/******************************************************************************/    
    
/*********************************MACROS***************************************/

// <editor-fold defaultstate="collapsed" desc="Macros de uso general">
#define RESET()	asm("reset")                                                    // Reset

#define FILENUM(num) \                                                          // Macro para guardar el n�mero de archivo de un archivo de c�digo
    enum { F_NUM = num }; \
    void _dummy##num(void){}

#ifndef NO_ASSERTION                                                            // Algoritmo para generar el error de ASSERT
#define ASSERT(expr) \
        if (expr) \
        {}  \
        else \
            aFailed(F_NUM, __LINE__)
#else                                                                          
#define ASSERT(expr)
#endif

/* NOTA: Cambiar valor de estos macros en caso de usar EEPROM interna de uC.
 * La localidad de error es el primer espacio de memoria en EEPROM en caso de 
 * no usarla. En este caso, se usan 6 espacios de memoria (0, 1, 2, 3, 4, 5), 
 * por lo tanto, el inicial es el espacio 6). */

#define LOCALIDAD_ERROR             0x06                                        // Bandera que indica que ocurrio un assert
#define LOCALIDAD_NUMERO_ERRORES    0x07                                        // Canttidad de veces que ocurrio el assrt
#define LOCALIDAD_POSICION_MEMORIA  0x08                                        // Indica en que posicion de memoria te encuentras
#define LOCALIDAD_SOBRECARGA        0xFF                                        // Indica sobrecarga de memoria (Cambia dependiendo del uC)
#define VERSION_FW                  0x02
// </editor-fold>
 
/******************************************************************************/
    
/*****************************ENUMS/STRUCTS/UNIONS*****************************/

// NA
    
/******************************************************************************/
         
#ifdef __cplusplus
}
#endif

#endif	/* ASSERT_H */
