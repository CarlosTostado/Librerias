/*
 * File:   eusart.c
 * Author: Mi Pc
 *
 * Created on 15 de enero de 2018, 15:12
 */


#include <xc.h>
#include "eusart.h"

static uint8_t eusart1TxHead = 0;
static uint8_t eusart1TxTail = 0;
static uint8_t eusart1TxBuffer[EUSART1_TX_BUFFER_SIZE];
volatile uint8_t eusart1TxBufferRemaining;

static uint8_t eusart2TxHead = 0;
static uint8_t eusart2TxTail = 0;
static uint8_t eusart2TxBuffer[EUSART2_TX_BUFFER_SIZE];
volatile uint8_t eusart2TxBufferRemaining;

static uint8_t eusart1RxHead = 0;
static uint8_t eusart1RxTail = 0;
static uint8_t eusart1RxBuffer[EUSART1_TX_BUFFER_SIZE];
uint8_t eusart1RxCount = 0;

static uint8_t eusart2RxHead = 0;
static uint8_t eusart2RxTail = 0;
static uint8_t eusart2RxBuffer[EUSART1_TX_BUFFER_SIZE];
uint8_t eusart2RxCount = 0;

void eusart_config(uint8_t eusart_select, uint8_t txxsta, 
                   uint8_t rcxsta, uint8_t baudxcon) {
    
    if(eusart_select & 1){
        // Configuración de los pines
        TRISCbits.TRISC6 = 0;
        TRISCbits.TRISC7 = 1;

        // Guardar estado de interrupciones
        uint8_t state = GIE;

        // Apagado de interrupciones
        GIE = 0;

        // Secuencia de desbloqueo de periférico
        PPSLOCK = 0x55;
        PPSLOCK = 0xAA;
        PPSLOCKbits.PPSLOCKED = 0x00;

        // Asigna los recursos de EUSART1 a pines por default
        RX1PPSbits.RXPPS = 0x17;
        TX1PPSbits.TXPPS = 0x16;

        // Redirección de pines EUSART
        RC7PPS = 0x0A;
        RC6PPS = 0x09;
        
        PPSLOCK = 0x55;
        PPSLOCK = 0xAA;
        
        PPSLOCKbits.PPSLOCKED = 0x01;

        // Regresa el estado de las interrupciones
        GIE = state;
        
        // Configura registro TX1STA
        TX1STA = 0x02 | txxsta;
        
        // Configura registro RC1STA
        RC1STA = 0x00 | rcxsta;
        
        // Configura registro BAUD1CON
        BAUD1CON = 0x00 | baudxcon;
        
        eusart1TxHead = 0;
        eusart1TxTail = 0;
        eusart1TxBufferRemaining = sizeof(eusart1TxBuffer);   
    } else {
        // Configuración de los pines
        TRISCbits.TRISC0 = 1;
        TRISCbits.TRISC1 = 0;

        // Guardar estado de interrupciones
        uint8_t state = GIE;

        // Apagado de interrupciones
        GIE = 0;

        // Secuencia de desbloqueo de periférico
        PPSLOCK = 0x55;
        PPSLOCK = 0xAA;
        PPSLOCKbits.PPSLOCKED = 0x00;

        // Asigna los recursos de EUSART1 a pines por default
        RX2PPSbits.RXPPS = 0x10;
        TX2PPSbits.TXPPS = 0x11;

        // Redirección de pines EUSART
        RC0PPS = 0x0C;
        RC1PPS = 0x0B;
        
        PPSLOCK = 0x55;
        PPSLOCK = 0xAA;
        
        PPSLOCKbits.PPSLOCKED = 0x01;

        // Regresa el estado de las interrupciones
        GIE = state;
        
        // Configura registro TX1STA
        TX2STA = 0x02 | txxsta;
        
        // Configura registro RC1STA
        RC2STA = 0x00 | rcxsta;
        
        // Configura registro BAUD1CON
        BAUD2CON = 0x00 | baudxcon;
        
        eusart2TxHead = 0;
        eusart2TxTail = 0;
        eusart2TxBufferRemaining = sizeof(eusart2TxBuffer);           
    }
}

void eusart_baud(uint8_t eusart_select, uint32_t valor_spbrg){
    if (eusart_select & 0x01){
        SP1BRGH = (valor_spbrg >> 8) & 0xFF;
        SP1BRGL = valor_spbrg & 0xFF;
    } else {
        SP2BRGH = (valor_spbrg >> 8) & 0xFF;
        SP2BRGL = valor_spbrg & 0xFF;
    }
}

void EUSART1_WriteInterrupt(uint8_t txData)
{
    while(0 == eusart1TxBufferRemaining)                                        //eusart1TxBufferRemaining = 16 al inicio.
    {}

    if(0 == PIE3bits.TX1IE)
    {
        TXREG1 = txData;
    }
    else
    {
        PIE3bits.TX1IE = 0;
        eusart1TxBuffer[eusart1TxHead++] = txData;
        if(sizeof(eusart1TxBuffer) <= eusart1TxHead)
        {
            eusart1TxHead = 0;
        }
        eusart1TxBufferRemaining--;
    }
    PIE3bits.TX1IE = 1;
}

void inline EUSART1_Transmit_ISR(void)
{
    // add your EUSART2 interrupt custom code
    if(sizeof(eusart1TxBuffer) > eusart1TxBufferRemaining)
    {
        TXREG1 = eusart1TxBuffer[eusart1TxTail++];
        if(sizeof(eusart1TxBuffer) <= eusart1TxTail)
        {
            eusart1TxTail = 0;
        }
        eusart1TxBufferRemaining++;
    }
    else
    {
        PIE3bits.TX1IE = 0;
    }
}

uint8_t EUSART1_Read()//Funcion para reiniciar la recepcion continua en caso de overrun(Sobreescritura o perdida de datos).
{
    if (0 == PIE3bits.RC1IE)
    {
        while(!PIR3bits.RC1IF){}                                                    //Comentar si se maneja por interrupcion
    }
    
    if(1 == RC1STAbits.OERR)
    {
        // EUSART1 error - restart

        RC1STAbits.CREN = 0; 
        RC1STAbits.CREN = 1; 
        
    }
    return RCREG1;
}

void inline EUSART1_Receive_ISR(void) //Funcion habilitada por interrupcion para recepcion de datos.
{
    // buffer overruns are ignored
    eusart1RxBuffer[eusart1RxHead++] = RCREG1;
    if (sizeof (eusart1RxBuffer) <= eusart1RxHead) 
    {
        eusart1RxHead = 0;
    }
    eusart1RxCount++;
}

void EUSART2_WriteInterrupt(uint8_t txData)
{
    while(0 == eusart2TxBufferRemaining)                                        //eusart1TxBufferRemaining = 16 al inicio.
    {}

    if(0 == PIE3bits.TX2IE)
    {
        TXREG2 = txData;
    }
    else
    {
        PIE3bits.TX2IE = 0;
        eusart2TxBuffer[eusart2TxHead++] = txData;
        if(sizeof(eusart2TxBuffer) <= eusart2TxHead)
        {
            eusart2TxHead = 0;
        }
        eusart2TxBufferRemaining--;
    }
    PIE3bits.TX2IE = 1;
}

void inline EUSART2_Transmit_ISR(void)
{
    // add your EUSART2 interrupt custom code
    if(sizeof(eusart2TxBuffer) > eusart2TxBufferRemaining)
    {
        TXREG2 = eusart2TxBuffer[eusart2TxTail++];
        if(sizeof(eusart2TxBuffer) <= eusart2TxTail)
        {
            eusart2TxTail = 0;
        }
        eusart2TxBufferRemaining++;
    }
    else
    {
        PIE3bits.TX2IE = 0;
    }
}

uint8_t EUSART2_Read()//Funcion para reiniciar la recepcion continua en caso de overrun(Sobreescritura o perdida de datos).
{
    if (0 == PIE3bits.RC2IE)
    {
        while(!PIR3bits.RC2IF){}                                                    //Comentar si se maneja por interrupcion
    }
    
    if(1 == RC2STAbits.OERR)
    {
        // EUSART1 error - restart

        RC2STAbits.CREN = 0; 
        RC2STAbits.CREN = 1; 
        
    }
    return RCREG2;
}

void inline EUSART2_Receive_ISR(void) //Funcion habilitada por interrupcion para recepcion de datos.
{
    // buffer overruns are ignored
    eusart2RxBuffer[eusart2RxHead++] = RCREG2;
    if (sizeof (eusart2RxBuffer) <= eusart2RxHead) 
    {
        eusart2RxHead = 0;
    }
    eusart1RxCount++;
}
