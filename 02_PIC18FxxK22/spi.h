/* 
 * File:   SPI.h
 * Author: V�ctor
 *
 * Created on 17 de noviembre de 2016, 03:13 PM
 */

#ifndef SPI_H
#define	SPI_H

#include <xc.h> 
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

// Maestro y CLK es igual a FOSC/64
#define M64                      64

// Maestro y CLK es igual a FOSC/16
#define M16                      16

// Maestro y CLK es igual a FOSC/4
#define M4                       4

// Maestro y CLK es igual a la salida de TMR2 entre 2
#define MTMR2                    2

//Modos de operaci�n SPI
#define MODO11                   3
#define MODO10                   2
#define MODO01                   1
#define MODO00                   0
                      
//Configuracion de los Pines
#define CS                       PORTDbits.RD2                                  // Escribir puerto de esclavo a utilizar
#define CLK                      PORTCbits.RC3
#define SDI                      PORTCbits.RC4
#define SDO                      PORTCbits.RC5
    
#define config_CS()              TRISDbits.RD2 = 0
#define config_CLK()             TRISCbits.RC3 = 0
#define config_SDI()             TRISCbits.RC4 = 1
#define config_SDO()             TRISCbits.RC5 = 0

#define enableSPI1()             SSP1CON1bits.SSPEN1 = 1
#define enableSPI_Interrupt()    PIE1bits.SSP1IE = 1
#define disableSPI_Interrupt()   PIE1bits.SSP1IE = 0
    
    
//Portotipo de funciones
    void init_SPI(uint8_t modo, uint8_t type);
    uint8_t readSPI_Buffer();
    void writeSPI_Buffer (uint8_t buffer);
    
unsigned char lect;
unsigned char READSPI();
void WRITESPIISR(unsigned char send_data);
void WRITESPIPOL(unsigned char send_data);
void SPI_ISR();


#ifdef	__cplusplus
}
#endif

#endif	/* SPI_H */

