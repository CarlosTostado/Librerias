#include "uart_sw.h"

// Funcion que inicializa los puertos a utilizar
void init_usart_sw(void)
{
    // Configura el pin Rx
    TRISRx1 = 1;
    PORTRx1 = 0;
    // Configura el Pin TX
    TRISTx1 = 0;
    PORTRx1 = 1;
 }

//Formula para delay en .h
void writeByteSW1(uint8_t byte)  
{
    uint8_t i=0;

    LATTx1 = 0; 
    _delay(833);
    for(i = 0; i < 8; i++)
    {
        LATTx1 = byte & 0x01;
        _delay(833);
        byte = byte >> 1;
    }

    LATTx1 = 1;
    _delay(833);

}

//Formula para delay en .h
void writeByteInvertSW1(uint8_t byte)  
{
    uint8_t i = 0;

    byte = ~byte;
    
    LATTx1 = 1; 
    _delay(833);
    for(i = 0; i < 8; i++)
    {
        LATTx1 = byte & 0x01;
        _delay(833);
        byte = byte >> 1;
    }

    LATTx1 = 0;
    _delay(833);

}

//Funcion que envia una cadena de datos
void writeStringSW1( unsigned char* buffer)
{
    // envia datos hasta que el caracter es nulo
    while(*buffer)                   
    {
        writeByteSW1(*buffer);
        // Increment buffer
        buffer++;               
    }  
 }
 
void writeStringInvertSW1(unsigned char* buffer)
{
    // envia datos hasta que el caracter es nulo
    while(*buffer)                   
    {
        writeByteInvertSW1(*buffer);
        // Increment buffer
        buffer++;               
    }  
 }

// Queda pendiente revisar 
unsigned char ReadSw1(void)
{
    unsigned char bitcount=8;
    unsigned char uartdata;
    unsigned long x=0;

    while (PORTRx1 && x<15000)
    {
        DelayRXHalfBitUART1();
        x=x+1;
    }

    while (bitcount--)
    {    
       DelayRXBitUART1();
       uartdata=uartdata>>1 | PORTRx1<<7;
    }
    return (uartdata);
}

void DelayRXHalfBitUART1(void)
{
    _delay(264);
	return;
}

void DelayRXBitUART1(void)
{
	_delay(517);
	return;
}

void DelayTX1(void)
{
   _delay(517); //para 20M//_delay(833);//para 32M   
}

///////////////////////////////////  SERIAL_SW (2)  ////////////////////////////
//

void DelayRXHalfBitUART2(void)
{
    _delay(260);
	return;
}

void DelayRXBitUART2(void)
{
	_delay(800);
	return;
}

void DelayTX2(void)
 {
   _delay(520); //para 20M//_delay(833);//para 32M
   
}
void OpenSw2(void)
 {
   
    TRISRx2=1;  // RX
   // PORTRx2=1;

    TRISTx2=0;// TX
    LATTx2=1;

 }

 void WriteSw2(unsigned char byte2) //  = (((2*OSC_FREQ)/(4*BAUDRATE))+1)/2  Tcy  
 {
    int i=0;

    OpenSw2(); 

    LATTx2=0;//LATEbits.LATE1=0;
    DelayTX2();

    for(i=0;i<8;i++)
        {
            LATTx2=byte2&0x01;
            DelayTX2();
            byte2=byte2>>1;
        }

    LATTx2=1;
    DelayTX2();
    //LATBbits.LATB4=0;// agregada para que sea igual que el USART

 }
 
 void putsSw2(unsigned char* buffer2)
 {
 
   while(*buffer2)              // envia datos hasta que el caracter es nulo
    {
        WriteSw2(*buffer2);
        buffer2++;               // Increment buffer
    }  
     
 
 }
 
 unsigned char ReadSw2(void)
{
    unsigned char bitcount=8;
    unsigned char uartdata;
    unsigned char dat=0;
    unsigned long x=0;

    while (PORTRx2)
    {
    DelayRXHalfBitUART2();

    }   


    while (bitcount--)
    {    
        DelayRXBitUART2();
         uartdata=uartdata>>1 | PORTRx2<<7;
     }
     //LED1=0;
     return (uartdata);
}