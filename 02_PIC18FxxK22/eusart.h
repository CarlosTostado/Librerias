/* 
 * File:   USART_lib.h
 * Author: V�ctor
 *
 * Created on 10 de noviembre de 2016, 12:32 PM
 */

#ifndef USART_LIB_H
#define	USART_LIB_H

#include <xc.h>
#include <stdint.h>

//Define el USART que sera utilizado
#define EUSART1                                 0x01
#define EUSART2                                 0x02

//Tama�os para Buffer 2    
#define EUSART2_TX_BUFFER_SIZE                   8
#define EUSART2_RX_BUFFER_SIZE                   8

//Tama�os para Buffer 1   
#define EUSART1_TX_BUFFER_SIZE                   8
#define EUSART1_RX_BUFFER_SIZE                   8
    
#define enableTX1Interrupt()                     PIE1bits.TX1IE = 1
#define enableRX1Interrupt()                     PIE1bits.RC1IE = 1
    
#define enableTX2Interrupt()                     PIE3bits.TX2IE = 1
#define enableRX2Interrupt()                     PIE3bits.RC2IE = 1
    

//Funciones para ambos perifericos    
void init_EUSARTx (uint8_t const EUSARTx_SELECT);
void USARTx_Disable(uint8_t const EUSARTx_SELECT);
void USARTx_Enable(uint8_t const EUSARTx_SELECT);

//Funciones para EUSART 1
uint8_t EUSART_Read1();
void EUSART1_WriteByte(uint8_t txData);
void EUSART1_WriteInterrupt(uint8_t txData);
void EUSART1_Transmit_ISR(void);

//Funciones para EUSART 2
uint8_t EUSART_Read2();
void EUSART2_WriteByte(uint8_t txData);
void EUSART2_WriteInterrupt(uint8_t txData);
void EUSART2_Transmit_ISR(void);

// Funcion para ecribir una cadena
void puts1(const unsigned char *buffer);

void SendString(const char *ptrBytes);



void EUSART2_WriteString(const uint8_t *TXdata);

#endif	/* USART_LIB_H */

