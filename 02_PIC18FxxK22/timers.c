/*
* File:            timers.c
* Author:          Ing. Carlos Tostado
* Date:            11 de enero 2017
* PIC:             PIC18F4xK22
* 
* Libreria que contiene las funciones para el uso de temporizadores 
* 
*/

//#include <pic18f46k22.h>

#include "timers.h"

static volatile uint16_t TickCount = 0;

/********************************** TMR0 **************************************/

void init_TMR0(void)
{ 
//    // Stop TMR0 - 16 bit - Internal CLK - Prescaler assigned - Prescaler 1:2  
//    T0CON = 0x00;
//    TMR0_Reload(0x00);
//    INTCONbits.TMR0IF = 0;                                                      // Clear Interrupt flag before enabling the interrupt
     T0CONbits.T08BIT = 0;                                                       //timer 16bits
    T0CONbits.T0CS = 0;                                                         // reloj interno 
    T0CONbits.PSA = 0;                                                          // Asignar preescaler a TIMER0
    T0CONbits.T0PS = 0b110;                                                     //preescaler 1:128
    TMR0_Reload(3036);
    T0CONbits.TMR0ON = 0;                                                       // timer on
    INTCONbits.TMR0IF = 0;                                                      // Clear Interrupt flag before enabling the interrupt

} 

void TMR0_Reload(uint16_t reload)
{
    TMR0H = (unsigned char) ((0xFF00 & reload) >> 8);
    TMR0L = (unsigned char)(reload & 0x00FF); 
}

void TMR0_StartTimer(void)
{
    // Start the Timer by writing to TMR0ON bit
    T0CONbits.TMR0ON = 1;
}

void TMR0_StopTimer(void)
{
    // Stop the Timer by writing to TMR0ON bit
    T0CONbits.TMR0ON = 0;
}

/* ****************************** TMR2/4/6 ************************************/

void init_TMRx(uint8_t const TMRx, uint8_t config)
{
    switch (TMRx)
    {
        case INIT_TMR2:
            T2CON = 0x00 | config;
            TMRx_Reload(INIT_TMR2,249);
        break;
        case INIT_TMR4:
            T4CON = 0x00 | config;           
            TMRx_Reload(INIT_TMR4,255);
        break;
        case INIT_TMR6:
            T6CON = 0x00 | config;
            TMRx_Reload(INIT_TMR6,255);
        break;
        default:
            T2CON = 0x00 | config;
            TMRx_Reload(INIT_TMR2,249);
        break;
            
    }
}

void TMRx_Reload(uint8_t const TMRx, uint8_t reload)
{
    switch (TMRx)
    {
        case INIT_TMR2:
                PR2 = reload;
                TMR2 = 0x00;
        break;
        case INIT_TMR4:
                PR4 = reload;
                TMR4 =0x00;
        break;
        case INIT_TMR6:
                PR6 = reload;
                TMR4 = 0x00;
        break;
        default:
                PR2 = reload;
                TMR2 = 0x00;
        break;
    }

}


void TMRx_StartTimer(uint8_t const TMRx)
{
    switch (TMRx)
    {
        case INIT_TMR2:
            T2CONbits.TMR2ON = 1;
        break;
        case INIT_TMR4:
            T4CONbits.TMR4ON = 1;
            
        break;
        case INIT_TMR6:
            T6CONbits.TMR6ON = 1;
        break;
        default:
            T2CONbits.TMR2ON = 1;
        break;
    }
}

void TMRx_StopTimer(uint8_t const TMRx)
{
    switch (TMRx)
    {
        case INIT_TMR2:
            T2CONbits.TMR2ON = 0;
        break;
        case INIT_TMR4:
         
            T4CONbits.TMR4ON = 0;TMR4=0;
        break;
        case INIT_TMR6:
            T6CONbits.TMR6ON = 0;
        break;
        default:
            T2CONbits.TMR2ON = 0;
        break;
    }
}


/* ****************************** TMR 1/3/5 ************************************/

void init_TMR(uint8_t const TMRx, uint8_t config)
{
    switch (TMRx)
    {
        case INIT_TMR1:
            T1CON = 0x00 | config;
//            T1CONbits.TMR1CS = 0b00;                                          //Timer1/3/5 clock source is system clock (FOSC)
//            T1CONbits.T1CKPS = 0b11;                                          //1:2 Prescale value           
//            T1CONbits.T1RD16 = 1;                                             //Enable 16 bits
//            T1CONbits.TMR1ON = 1;                                             //Enables Timer1/3/5
//            IPR1bits.TMR1IP = 0;                                              //Baja prioridad

            //Tentativamente 0.05 segundos, 50 milisegundos(15535_pre8)
            TMR_Reload(INIT_TMR1,15536);                                        //Valor inicial 25536
            break;
    }                                                       
}

void TMR_Reload(uint8_t const TMRx, uint16_t reload)
{
    switch (TMRx)
    {
        case INIT_TMR1:
            TMR1H = (uint8_t) ((0xFF00 & reload) >> 8);
            TMR1L = (uint8_t) (reload & 0x00FF); 
            break;
    }
}

void TMR_StartTimer(uint8_t const TMRx)
{
    switch (TMRx)
    {
        case INIT_TMR1:
            T1CONbits.TMR1ON = 1;
        break;
    }
}

void TMR_StopTimer(uint8_t const TMRx)
{
    switch (TMRx)
    {
        case INIT_TMR1:
            T1CONbits.TMR1ON = 0;
        break;
    }
}



/* ****************************** Funciones delay******************************/

void Wait1ms(unsigned int NumMilliseconds)
{
    StartTimer(NumMilliseconds);         //Start software timer 2
    while(!isTimerDone())                   //and wait for it to count down
    { CLRWDT();}
    
    TMRx_StopTimer(INIT_TMR2);
}

static unsigned char isTimerDone()
{
    if(TickCount == 0)               //Check if counted down to zero
        return 1;                        //then return true
    return 0;                           //else return false
}

static void StartTimer(unsigned int Count)
{
    TickCount = Count;             
    TMRx_StartTimer(INIT_TMR2);
}

void TMR2_ISR()
{
  if(TickCount != 0)                   //If software timer 0 has not reached zero then
    TickCount--;  

    TMRx_Reload(INIT_TMR2, 249);
    PIR1bits.TMR2IF = 0; 
  
  
}
