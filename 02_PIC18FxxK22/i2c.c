/*
* File:            i2c.c
* Author:          Ing. Carlos Tostado
* Date:            27 de enero 2017
* PIC:             PIC18F4xK22
* 
* Libreria que contiene las funciones para el uso de los puertos i2c 1
* con el que cuenta el microcontrolador 
* 
*/

//#include <pic18f45k22.h>
//#include <pic18f46k22.h>

#include "i2c.h"

void init_I2C(void)
{
    // Pines como entradas
    TRISCbits.RC3 = 1;//CLK
    TRISCbits.RC4 = 1;//SDA
    
    // SMP Standar Speed (100KHz) ; CKE enabled; 
    SSP1STAT = 0xC0;
    // SSPEN enabled; WCOL no_collision; CKP Idle:Low, Active:High; SSPM FOSC/4_SSPxADD_I2C; SSPOV no_overflow; 
    SSP1CON1 = 0x28;
    // SBCDE disabled; BOEN disabled; SCIE disabled; PCIE disabled; DHEN disabled; SDAHT 100ns; AHEN disabled; 
    SSP1CON3 = 0x00;
    // Baud Rate Generator Value: SSP1ADD 3;   
    SSP1ADD = 0x27;
    
    //SSP1ADD se calcula = ( FOSC / (FCLOCK x 4) ) - 1

}

void Write_I2CBuffer(uint8_t *ptrBuffer, uint8_t bufferSize, uint8_t addr)
{   
    uint8_t bufferCounter = 0;
    
    if (SSP1CON1bits.WCOL)
    {
        SSP1CON1bits.WCOL  = 0;
        // The user generates a Start condition by setting the SEN bit of the SSPxCON2 register.  
        SSP1CON2bits.SEN = 1;
    }
    else
    {
        // The user generates a Start condition by setting the SEN bit of the SSPxCON2 register.  
        SSP1CON2bits.SEN = 1;  
    }
     
    // The MSSPx module will wait the required start time before any other operation takes place.
    while (SSP1CON2bits.SEN);
    
    // The user loads the SSPxBUF with the slave address to transmit.
    SSP1BUF = ( addr << 1) & 0xFE;                                                             // This culd be fixed or variable
    while(SSP1STATbits.RW);
//    while(SSP1CON2bits.ACKSTAT || SSP1STATbits.RW);
//    while(SSP1CON2bits.ACKSTAT);////////////////////////////
    // Address is shifted out the SDAx pin until all eight bits are transmitted. 
    // Transmission begins as soon as SSPxBUF is written to.
    
    // The MSSPx module shifts in the ACK bit from the slave device and writes 
    // its value into the ACKSTAT bit of the SSPxCON2 register.
 
    for (bufferCounter = 0; bufferCounter < bufferSize; bufferCounter++)
    {
        // The ACK must 0 to send the data if not we stop
        if(SSP1CON2bits.ACKSTAT)
        {
            
            // Transmission was not acknowledged
            // Send a stop flag and go back to idle
            SSP1CON2bits.PEN = 1;
            
            // Reset the Ack flag
//            SSP1CON2bits.ACKSTAT = 0;
            
            break;
        }
        else
        {
            //If the ACk is correct we check the buffer to verify if exist data to send
            SSP1BUF = *ptrBuffer++; 
            while(SSP1STATbits.RW); 
//             while(SSP1CON2bits.ACKSTAT || SSP1STATbits.RW);//////////////////////////
        } 
    }

    // Transmission was not acknowledged
    // Send a stop flag and go back to idle
//    SSP1CON2bits.PEN = 1;
//    
//    //Espera que la condicion de stop termine
//    while(SSP1CON2bits.PEN);
//
//    // Reset the Ack flag
//    SSP1CON2bits.ACKSTAT = 0;
}

void Write_I2CData (uint8_t data)
{   
    if (SSP1CON1bits.WCOL)
    {
        SSP1CON1bits.WCOL  = 0;
        // The user generates a Start condition by setting the SEN bit of the SSPxCON2 register.  
        SSP1CON2bits.SEN = 1;
    }
    else
    {
        // The user generates a Start condition by setting the SEN bit of the SSPxCON2 register.  
        SSP1CON2bits.SEN = 1;  
    }
    
    // The MSSPx module will wait the required start time before any other operation takes place.
    while (SSP1CON2bits.SEN);
    
    // The user loads the SSPxBUF with the slave address to transmit.
    SSP1BUF = 0xA0;                                                             // This culd be fixed or variable
    while(SSP1STATbits.RW);

    if(!SSP1CON2bits.ACKSTAT)
    {
        SSP1BUF = data; 
        while(SSP1STATbits.RW);
    }
}

void RestartI2C( void )
{
  SSP1CON2bits.RSEN = 1;           // initiate bus restart condition
  while (SSP2CON2bits.RSEN);
}

void StopI2C( void )
{
    // Send a stop flag and go back to idle
    SSP1CON2bits.PEN = 1;
    
    //Espera que la condicion de stop termine
    while(SSP1CON2bits.PEN);
}

uint8_t ReadI2C(uint8_t address)
{
    uint8_t valor = 0;
    if (SSP1CON1bits.WCOL)
    {
        SSP1CON1bits.WCOL  = 0;
        // The user generates a Start condition by setting the SEN bit of the SSPxCON2 register.  
        SSP1CON2bits.SEN = 1;
    }
    else
    {
        // The user generates a Start condition by setting the SEN bit of the SSPxCON2 register.  
        SSP1CON2bits.SEN = 1;  
    }
    
    // The MSSPx module will wait the required start time before any other operation takes place.
    while (SSP1CON2bits.SEN);
    SSP1IF = 0;
    
    // The user loads the SSPxBUF with the slave address to transmit.
    SSP1BUF = ( address << 1) | 0x01;                                                             // This culd be fixed or variable
    while(!SSP1IF);
    SSP1IF = 0;
    
    // User sets the RCEN bit of the SSPxCON2 register and the Master clocks in a byte from the slave.
    SSP1CON2bits.RCEN = 1;
    
    //After the 8th falling edge of SCLx, SSPxIF and BF are set.
    while(!SSP1STATbits.BF);
    
    //Master reads the received byte from SSPxBF, clears BF.
    valor = SSP1BUF;
    
    //Master sets ACK value sent to slave in ACKDT bit of the SSPxCON2 register 
    //and initiates the ACK by setting the ACKEN bit.
    SSP1CON2bits.ACKDT = 1;
    SSP1CON2bits.ACKEN = 1;
    
    while (SSP1CON2bits.ACKEN);
    
    return (valor);
}