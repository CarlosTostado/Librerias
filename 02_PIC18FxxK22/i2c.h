/* 
 * File:   i2c.h
 * Author: Desarrollo
 *
 * Created on January 27, 2017, 8:08 AM
 */

#ifndef I2C_H
#define	I2C_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>
    
void init_I2C(void);
void Write_I2CBuffer(uint8_t *ptrBuffer, uint8_t bufferSize, uint8_t addr);
void Write_I2CData (uint8_t data);

void RestartI2C( void );
void StopI2C( void );

uint8_t ReadI2C(uint8_t address);


#ifdef	__cplusplus
}
#endif

#endif	/* I2C_H */

