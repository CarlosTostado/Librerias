/*******************************************************************************
 * 
 * Archivo:     EEPROM.h
 * Autor:       Ing. Carlos Tostado
 * Fecha:       16 de enero 2017
 * 
 ******************************************************************************/

#ifndef EEPROM_H
#define	EEPROM_H

#include <xc.h>
#include <stdint.h>
#include "ds1302.h"
#include "lcd_7segmentos.h"
// Constantes con las marcas por localidad
#define MARCA_SISTEMA_INICIADO                   0x0A
#define LOCALIDAD_INICIAL                        0x00

//*********************** LOCALIDADES DE MEMORIA EXTERNA 24LC256 (32kBytes)

#define LOCALIDAD_ENSENDIDO_INICIAL              0x00

#define LOCALIDAD_TOTALIZADOR_SIN_AD_HH          0x01
#define LOCALIDAD_TOTALIZADOR_SIN_AD_H           0x02
#define LOCALIDAD_TOTALIZADOR_SIN_AD_M           0x03
#define LOCALIDAD_TOTALIZADOR_SIN_AD_L           0x04


#define LOCALIDAD_PRECIOH                        0x05
#define LOCALIDAD_PRECIOL                        0x06

#define LOCALIDAD_ADITIVO                        0x07
#define LOCALIDAD_DESABILITADA                   0x08                           // DESABILITADO

#define LOCALIDAD_FOLIO_BYTE_H                   0x09
#define LOCALIDAD_FOLIO_BYTE_MED                 0x0A
#define LOCALIDAD_FOLIO_BYTE_L                   0x0B

#define LOCALIDAD_NUM_SERVICIO_H                 0x0C
#define LOCALIDAD_NUM_SERVICIO_L                 0x0D

#define LOCALIDAD_NODRIZA                        0x0E
#define LOCALIDAD_TIMEOUT                        0x0F

#define LOCALIDAD_KCALIBRACIONH_ANFORITA         0x10
#define LOCALIDAD_KCALIBRACIONL_ANFORITA         0x11

#define LOCALIDAD_KCALIBRACIONH_CARBURACION      0x12            
#define LOCALIDAD_KCALIBRACIONL_CARBURACION      0x13 

#define LOCALIDAD_PASSWORDH                      0x14
#define LOCALIDAD_PASSWORDL                      0x15

#define LOCALIDAD_PASSWORD2_H                    0x16
#define LOCALIDAD_PASSWORD2_L                    0x17

#define LOCALIDAD_POSICION_DE_MEMORIA_H          0x18
#define LOCALIDAD_POSICION_DE_MEMORIA_L          0x19

#define LOCALIDAD_RECIRCULACION_H                0x1A                           //
#define LOCALIDAD_RECIRCULACION_M                0x1B                           //  ANTES ERAN LITROS SUERTIENDOSE RAM
#define LOCALIDAD_RECIRCULACION_L                0x1C                           //

#define LOCALIDAD_LITROS_ACUMULADOS_H            0x1D
#define LOCALIDAD_LITROS_ACUMULADOS_M            0x1E
#define LOCALIDAD_LITROS_ACUMULADOS_L            0x1F

#define LOCALIDAD_LITROS_INTERNOS_H              0x20
#define LOCALIDAD_LITROS_INTERNOS_M              0x21
#define LOCALIDAD_LITROS_INTERNOS_L              0x22

#define LOCALIDAD_PESOS_CREDITO_H                0x23
#define LOCALIDAD_PESOS_CREDITO_M                0x24
#define LOCALIDAD_PESOS_CREDITO_L                0x25
//***

#define LOCALIDAD_PESOS_CONTADO_H                0x26
#define LOCALIDAD_PESOS_CONTADO_M                0x27
#define LOCALIDAD_PESOS_CONTADO_L                0x28


#define LOCALIDAD_DIA_OPERATIVO                  0x29
#define LOCALIDAD_MES_OPERATIVO                  0x2A
#define LOCALIDAD_ANIO_OPERATIVO                 0x2B

#define LOCALIDAD_TRATADO1                       0x2C
#define LOCALIDAD_TRATADO2                       0x2D


#define LOCALIDAD_PREFIJO1                       0x2E
#define LOCALIDAD_PREFIJO2                       0x2F
#define LOCALIDAD_PREFIJO3                       0x30
#define LOCALIDAD_PREFIJO4                       0x31
#define LOCALIDAD_PREFIJO5                       0x32

#define LOCALIDAD_TANQUE                         0x33

#define LOCALIDAD_TOTALIZADOR_INICIAL_HH         0x34
#define LOCALIDAD_TOTALIZADOR_INICIAL_H          0x35  
#define LOCALIDAD_TOTALIZADOR_INICIAL_M          0x36  
#define LOCALIDAD_TOTALIZADOR_INICIAL_L          0x37  

#define LOCALIDAD_PORCENTAJE_INICIAL_SAD         0x38

#define LOCALIDAD_COSTO_PRECIO_H                 0x39
#define LOCALIDAD_COSTO_PRECIO_M                 0x3A
#define LOCALIDAD_COSTO_PRECIO_L                 0x3B

#define LOCALIDAD_DESCUENTO_CONTADO_H            0x3C
#define LOCALIDAD_DESCUENTO_CONTADO_M            0x3D
#define LOCALIDAD_DESCUENTO_CONTADO_L            0x3E

#define LOCALIDAD_FINANCIAMIENTO_CONTADO_H       0x3F
#define LOCALIDAD_FINANCIAMIENTO_CONTADO_M       0x40
#define LOCALIDAD_FINANCIAMIENTO_CONTADO_L       0x41

#define LOCALIDAD_DESCUENTO_CREDITO_H            0x42
#define LOCALIDAD_DESCUENTO_CREDITO_M            0x43
#define LOCALIDAD_DESCUENTO_CREDITO_L            0x44

#define LOCALIDAD_FINANCIAMIENTO_CREDITO_H       0x45
#define LOCALIDAD_FINANCIAMIENTO_CREDITO_M       0x46
#define LOCALIDAD_FINANCIAMIENTO_CREDITO_L       0x47

#define LOCALIDAD_CREDITO_NETO_H                 0x48
#define LOCALIDAD_CREDITO_NETO_M                 0x49
#define LOCALIDAD_CREDITO_NETO_L                 0x4A

#define LOCALIDAD_CONTADO_NETO_H                 0x4B
#define LOCALIDAD_CONTADO_NETO_M                 0x4C
#define LOCALIDAD_CONTADO_NETO_L                 0x4D

#define LOCALIDAD_CORTE_FLUJO                    0x4E

#define LOCALIDAD_SURTIDOS_INCORRECTOS           0x4F


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define LOCALIDAD_TOTALIZADOR_CON_AD_H           0x03                           // DESABILITADO            //
#define LOCALIDAD_TOTALIZADOR_CON_AD_M           0x04                           // DESABILITADO            //
#define LOCALIDAD_TOTALIZADOR_CON_AD_L           0x05                           // DESABILITADO            //
                                                                                                           //

                                                                                                           //
//#define LOCALIDAD_PORCENTAJE_INICIAL_SAD         0x25                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_PORCENTAJE_FINAL_SAD           0X26                           // PENDIENTE DE REVISAR ***//

// se borro la funcion desde el enter
#define LOCALIDAD_PORCENTAJE_INICIAL_CAD         0x27                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_PORCENTAJE_FINAL_CAD           0X28                           // PENDIENTE DE REVISAR ***//
                                                                                                           //
#define LOCALIDAD_CLAVE_H                        0X29                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_CLAVE_M                        0X2A                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_CLAVE_L                        0X2B                           // PENDIENTE DE REVISAR ***//
                                                                                                           //
#define LOCALIDAD_PORCENTAJE_FINAL_ANTERIOR_SAD  0x2C                           // PENDIENTE DE REVISAR ***//
                                                                                                           //
#define LOCALIDAD_PULSOS_NO_DESEADOS_H           0x2D                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_PULSOS_NO_DESEADOS_M           0x2E                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_PULSOS_NO_DESEADOS_L           0x2F                           // PENDIENTE DE REVISAR ***//
                                                                                                           //
#define LOCALIDAD_PESOS_INTERNOS_H               0x32                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_PESOS_INTERNOS_M               0x33                           // PENDIENTE DE REVISAR ***//
#define LOCALIDAD_PESOS_INTERNOS_L               0x34                           // PENDIENTE DE REVISAR ***//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////
#define PAQUETE_SURTIDO                          17
#define INIT_POSICION                            0x60//
#define INIT_LITROS_SURTIDOS                     0x01//
#define INIT_PESOS_SURTIDOS                      0x04//
#define INIT_HORA_INICIAL                        0x07//
#define INIT_TIEMPO_RESTANTE                     0x09//
#define INIT_DIA_MES                             0x0B//
#define INIT_ALARMA                              0x0D//
#define INIT_TIPO_PAGO                           0x0E//
#define INIT_NUM_SERVICIO                        0x0F//
#define INIT_LINEA_CAPTURA                       0x11//
///////////////////////////////////////////////////////



//typedef struct
//{
    
    
//}MEMORIA;



//*************************************** FUNCIONES GENERICAS DE MEMORIA EXTERNA

//void initObject_Epp(void);

uint32_t getVariable_3Bytes_EEPROM(uint8_t LOCALIDAD_H,uint8_t LOCALIDAD_M,uint8_t LOCALIDAD_L);
void setVariable_3Bytes_EEPROM(uint32_t valor,uint8_t LOCALIDAD_H,uint8_t LOCALIDAD_M,uint8_t LOCALIDAD_L);


uint16_t getVariable_2Bytes_EEPROM(uint8_t LOCALIDAD_H,uint8_t LOCALIDAD_L);
void setVariable_2Bytes_EEPROM(uint16_t valor,uint8_t LOCALIDAD_H,uint8_t LOCALIDAD_L);


uint8_t getVariable_1Byte_EEPROM(uint8_t LOCALIDAD_H);
void setVariable_1Byte_EEPROM(uint8_t valor,uint8_t LOCALIDAD_H);





//************************************ FUNCIONES DE MEMORIA EXTERNA INDIVIDUALES

uint32_t getClaveEEPROM(void);
void setClaveEEPROM(uint32_t numeroTecleado);

void setTotalizadorInicial_EEPROM(uint32_t numeroTecleado);
uint32_t getTotalizadorInicial_EEPROM(void);

void EEPROM_Write(uint8_t add, uint8_t datain);
uint8_t EEPROM_Read(uint8_t address);

void setPorcentajeInicialSinAD_EEPROM(uint8_t numeroTeclado);
uint8_t getPorcentajeInicialSinAD_EEPROM(void);

uint8_t getPorcentajeFinalSinAD_EEPROM(void);
void setPorcentajeFinalSinAD_EEPROM(uint8_t numeroTeclado);

void setPorcentajeInicialConAD_EEPROM(uint8_t numeroTeclado);
uint8_t getPorcentajeInicialConAD_EEPROM(void);

void setPorcentajeFinalConAD_EEPROM(uint8_t numeroTeclado);
uint8_t getPorcentajeFinalConAD_EEPROM(void);


uint16_t getKCalibracionCarburacion_EEPROM(void);
void setKCalibracionCarburacion_EEPROM(uint16_t numeroTecleado);

uint16_t getKCalibracionAnforita_EEPROM(void);
void setKCalibracion_Anforita_EEPROM(uint16_t numeroTecleado);
//uint16_t getKCalibracion(void);
//void setKCalibracionEEPROM(uint16_t numeroTecleado);


uint16_t getPasswordEEPROM(void);
void setPasswordEEPROM(uint16_t numeroTecleado);


uint16_t getPassword_SUPERVISOR_EEPROM(void);
void setPassword_SUPERVISOR_EEPROM(uint16_t numeroTecleado);


uint16_t getPrecioEEPROM(void);
void setPrecioEEPROM(uint16_t numeroTecleado);


uint8_t getNodrizaEEPROM(void);
void setNodrizaEEPROM(uint8_t numeroTecleado);


void setTimeOutEEPROM(uint8_t numeroTeclado);
uint8_t getTimeOutEEPROM(void);


uint32_t getFolioEEPROM(void);
void setFolioEEPROM(uint32_t numeroTecleado);


void setAditivoEEPROM(uint8_t numeroTeclado);
uint8_t getAditivoEEPROM(void);


uint16_t getNumServicioEEPROM(void);
void setNumServicioEEPROM(uint16_t numeroTecleado);


uint32_t getTotalizadorSinAD_EEPROM(void);
void setTotalizadorSinAD_EEPROM(uint32_t numeroTecleado);


uint32_t getTotalizadorConAD_EEPROM(void);
void setTotalizadorConAD_EEPROM(uint32_t numeroTecleado);


uint32_t get_LitrosAcumulados_EEPROM(void);
void set_LitrosAcumulados_EEPROM(uint32_t numeroTecleado);

uint32_t get_LitrosInternos_EEPROM(void);
void set_LitrosInternos_EEPROM(uint32_t numeroTecleado);



uint32_t get_PesosCredito_EEPROM(void);
void set_PesosCredito_EEPROM(uint32_t numeroTecleado);


void set_PesosInternos_EEPROM(uint32_t numeroTecleado);
uint32_t get_PesosInternos_EEPROM(void);

uint32_t get_PesosDeContado_Internos_EEPROM(void);
void set_PesosDeContado_EEPROM(uint32_t numeroTecleado);



uint32_t get_PulsosNoDeseados_EEPROM(void);
void set_PulsosNoDeseados_EEPROM(uint32_t numeroTecleado);


void set_fechaOperativa_EEPROM(uint8_t* fecha);
void get_fechaOperativa_EEPROM(uint8_t* fecha);


void setTratado1EEPROM(uint8_t numeroTeclado);
uint8_t getTratado1EEPROM(void);

void setTratado2EEPROM(uint8_t numeroTeclado);
uint8_t getTratado2EEPROM(void);

void setTanqueEEPROM(uint8_t numeroTeclado);
uint8_t getTanqueEEPROM(void);

void setPrefijos_EEPROM(uint8_t* prefijo);
void getPrefijos_EEPROM(uint8_t* prefijo);

uint32_t getRecirculacion_EEPROM(void);
void setRecirculacion_EEPROM(uint32_t litros);

void setPrecioCosto_EEPROM(uint32_t numeroTeclado);
uint32_t getPrecioCosto_EEPROM(void);

void setDescuentoContado_EEPROM(uint32_t numeroTeclado);
uint32_t getDescuentoContado_EEPROM(void);

void setFinanciamientoContado_EEPROM(uint32_t numeroTeclado);
uint32_t getFinanciamientoContado_EEPROM(void);

void setFinanciamientoCredito_EEPROM(uint32_t numeroTeclado);
uint32_t getFinanciamientoCredito_EEPROM(void);

void setDescuentoCredito_EEPROM(uint32_t numeroTeclado);
uint32_t getDescuentoCredito_EEPROM(void) ;

void setCreditoNeto_EEPROM(uint32_t numeroTeclado);
uint32_t getCreditoNeto_EEPROM(void);

void setContadoNeto_EEPROM(uint32_t numeroTeclado);
uint32_t getContadoNeto_EEPROM(void);

uint8_t getCorteFujoEEPROM(void);
void setCorteFlujoEEPROM(uint8_t numeroTecleado);

void setSurtidosIncorrectosEEPROM(uint8_t numeroTeclado);
uint8_t getSurtidosIncorrectosEEPROM(void);
//______________________________________________________________________________


uint16_t getPosicionDeMemoriaEEPROM(void);
void setPosicionDeMemoriaEEPROM(uint16_t numeroTecleado);


void set_SurtidoX_Bandera3G_EEPROM(uint16_t DireccionX, uint8_t Alarma);
uint8_t get_SurtidoX_Bandera3G_EEPROM(uint16_t servicio);


void set_SurtidoX_LitrosSurtidos_EEPROM(uint16_t DireccionX,uint32_t numeroTecleado);
uint32_t get_SurtidoX_LitrosSurtidos_EEPROM(uint16_t servicio);


uint32_t get_SurtidoX_PesosSurtidos_EEPROM(uint16_t servicio);
void set_SurtidoX_PesosSurtidos_EEPROM(uint16_t DireccionX,uint32_t numeroTecleado);


void set_SurtidoX_HoraInicial_EEPROM(uint16_t DireccionX,Calendario* tiempo);
Calendario get_SurtidoX_HoraInicial_EEPROM(uint16_t servicio);


void set_SurtidoX_TiempoRestante_EEPROM(uint16_t DireccionX,uint16_t numeroTecleado);
uint16_t get_SurtidoX_TiempoRestante_EEPROM(uint16_t servicio);


void set_SurtidoX_Dia_EEPROM(uint16_t DireccionX, Calendario* tiempo);
Calendario get_SurtidoX_Dia_EEPROM(uint16_t servicio);


void set_SurtidoX_Alarma_EEPROM(uint16_t Direccion, uint8_t Alarma);
uint8_t get_SurtidoX_Alarma_EEPROM(uint16_t servicio);


void set_SurtidoX_TipoDePago_EEPROM(uint16_t Direccion, uint8_t pago);
uint8_t get_SurtidoX_tipoDePago_EEPROM(uint16_t servicio);


void set_SurtidoX_NumServicio_EEPROM(uint16_t DireccionX,uint16_t numeroTecleado);
uint16_t get_SurtidoX_NumServicio_EEPROM(uint16_t servicio);


void set_SurtidoX_LineaDeCaptura_EEPROM(uint16_t DireccionX,unsigned char* numeroTecleado);
void get_SurtidoX_LineaDeCaptura_EEPROM(uint16_t servicio,unsigned char*captura);

//uint32_t get_LitrosSurtidos_RAM(void);
//void set_LitrosSurtidos_RAM(uint32_t numeroTecleado);



//******************************************************************************



#endif	/* EEPROM_H */

