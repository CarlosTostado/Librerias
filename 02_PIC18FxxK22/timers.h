/*
* File:            timers.c
* Author:          Ing. Carlos Tostado
* Date:            11 de enero 2017
* PIC:             PIC18F4xK22
* version:         1.0
* 
* Libreria que contiene las funciones para el uso de temporizadores 
* 
* Actualizaci�n
* Autor:            Ing. XXX
* Fecha:            mes dia, a�o
* Versi�n:          x.x.x
* Descripcion:      ...
*  
*/

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef Timers_K22_H
#define	Timers_K22_H

#include <xc.h>
#include <stdint.h>

#define INIT_TMR1                   0x01 
#define INIT_TMR2                   0x02       
#define INIT_TMR4                   0x04 
#define INIT_TMR6                   0x06 

#define TMRx_PRESCALE1              0x00
#define TMRx_PRESCALE4              0x01
#define TMRx_PRESCALE16             0x02
#define TMRx_ON                     0x04
#define TMRx_POSTCALE2              0x08
#define TMRx_POSTCALE3              0x10
#define TMRx_POSTCALE4              0x18
#define TMRx_POSTCALE5              0x20
#define TMRx_POSTCALE6              0x28
#define TMRx_POSTCALE7              0x30
#define TMRx_POSTCALE8              0x38
#define TMRx_POSTCALE9              0x40
#define TMRx_POSTCALE10             0x48
#define TMRx_POSTCALE11             0x50
#define TMRx_POSTCALE12             0x58
#define TMRx_POSTCALE13             0x60
#define TMRx_POSTCALE14             0x68
#define TMRx_POSTCALE15             0x70
#define TMRx_POSTCALE16             0x78


//Macros para utilizar en TMRS 1 / 3 / 5
#define TMR_ON                      0x01
#define TMR_16BIT                   0x02
#define TMR_PRESCALE1               0x00
#define TMR_PRESCALE2               0x10
#define TMR_PRESCALE4               0x20
#define TMR_PRESCALE8               0x30
#define TMR_FOSC4                   0x00
#define TMR_FOSC                    0x40
////////////////////////////////////////

#define enableTMR0Interrupt()    INTCONbits.TMR0IE  = 1                         // Enabling TMR4 interrupt.
#define enableTMR1Interrupt()    PIE1bits.TMR1IE    = 1                         // Enabling TMR1 interrupt.
#define enableTMR2Interrupt()    PIE1bits.TMR2IE    = 1                         // Enabling TMR2 interrupt.
#define enableTMR4Interrupt()    PIE5bits.TMR4IE    = 1                         // Enabling TMR4 interrupt.
#define disableTMR0Interrupt()   INTCONbits.TMR0IE  = 0                         // Disable TMR0 interrupt.
#define disbleTMR2Interrupt()    PIE1bits.TMR2IE    = 0                         // Disable TMR2 interrupt.
#define disbleTMR4Interrupt()    PIE5bits.TMR4IE    = 0                         // Disable TMR4 interrupt.
#define enableTMR6Interrupt()    PIE5bits.TMR6IE    = 1
#define disableTMR6Interrupt()    PIE5bits.TMR6IE   = 0
//En esta funcion haces las modificaciones para el Timer
void init_TMR0(void);
void TMR0_Reload(uint16_t reload);
void TMR0_StartTimer(void);
void TMR0_StopTimer(void);

//Funciones para manejo de timers 2 / 4 / 6
void init_TMRx(uint8_t const TMRx, uint8_t config);
void TMRx_Reload(uint8_t const TMRx, uint8_t reload);
void TMRx_StartTimer(uint8_t const TMRx);
void TMRx_StopTimer(uint8_t const TMRx);

void Wait1ms(unsigned int NumMilliseconds);
static unsigned char isTimerDone();
static void StartTimer(unsigned int Count);

void TMR2_ISR();


//Funciones para manejo de timers 1 / 3 / 5
void init_TMR(uint8_t const TMRx, uint8_t config);
void TMR_Reload(uint8_t const TMRx, uint16_t reload);
void TMR_StartTimer(uint8_t const TMRx);
void TMR_StopTimer(uint8_t const TMRx);


#endif	/* XC_HEADER_TEMPLATE_H */

