#include "spi.h"

void init_SPI(unsigned char mode, unsigned char type)
{
    // Asignación de los puertos SPI.
    config_CS();                                                                //OUTPUT
    config_CLK();                                                               //OUTPUT
    config_SDI();                                                               //INPUT                                  
    config_SDO();                                                               //OUTPUT
    
    //Deshabilita el dispositivo conectado
    CS = 1;
    
    //Habilitar SPI
    enableSPI1();                                            
    
    //Interrupcion
    disableSPI_Interrupt();
    
    // <editor-fold defaultstate="collapsed" desc="Selección modo/tipo">
    switch (mode)
    {
        case MODO11:
            SSP1CON1bits.CKP = 1;                                               //Idle state for clock is a high level
            SSP1STATbits.CKE = 0;                                               //Transmit occurs on transition from active to Idle clock state
        break;
        
        case MODO10:
            SSP1CON1bits.CKP = 1;                                               //Idle state for clock is a high level
            SSP1STATbits.CKE = 1;                                               //Transmit occurs on transition from Idle to active clock state
        break;
        
        case MODO01:
            SSP1CON1bits.CKP = 0;                                               // Idle state for clock is a low level
            SSP1STATbits.CKE = 1;                                               //Transmit occurs on transition from active to Idle clock state
            
        break;
        
        case MODO00:
            SSP1CON1bits.CKP = 0;                                               // Idle state for clock is a low level
            SSP1STATbits.CKE = 0;                                               //Transmit occurs on transition from Idle to active clock state
        break;
        
        default:
            SSP1CON1bits.CKP = 0;                                               // Idle state for clock is a low level
            SSP1STATbits.CKE = 0;                                               //Transmit occurs on transition from Idle to active clock state
        break;
    }
    
    switch (type)
    {
        case MTMR2:
            SSP1CON1bits.SSPM31 = 0;
            SSP1CON1bits.SSPM21 = 0;
            SSP1CON1bits.SSPM11 = 1;
            SSP1CON1bits.SSPM01 = 1;
        break;
        
        case M64:
            SSP1CON1bits.SSPM31 = 0;
            SSP1CON1bits.SSPM21 = 0;
            SSP1CON1bits.SSPM11 = 1;
            SSP1CON1bits.SSPM01 = 0;
        break;
        
        case M16:
            SSP1CON1bits.SSPM31 = 0;
            SSP1CON1bits.SSPM21 = 0;
            SSP1CON1bits.SSPM11 = 0;
            SSP1CON1bits.SSPM01 = 1;
        break;
        
        case M4:
            SSP1CON1bits.SSPM31 = 0;
            SSP1CON1bits.SSPM21 = 0;
            SSP1CON1bits.SSPM11 = 0;
            SSP1CON1bits.SSPM01 = 0;
        break;
        
        default:
        break;
    }
    // </editor-fold> 
}

uint8_t readSPI_Buffer()
{
    
    if (1 == PIE1bits.SSP1IE)
    {
       return SSP1BUF; 
    }
    else 
    {
        PIR1bits.SSP1IF = 0;
        SSP1BUF=0xFF;
        while(!PIR1bits.SSP1IF);

        return SSP1BUF; 
    }
    
}

void writeSPI_Buffer (uint8_t buffer)
{
    if (1 == PIE1bits.SSP1IE)
    {
        SSP1BUF = buffer;
    }
    else
    {
        PIR1bits.SSP1IF = 0;
        SSP1BUF = buffer;
        while(!PIR1bits.SSP1IF);
    }
}

#if DISABLE_CODE

unsigned char READSPI()
{
    unsigned char read;
    read = SSP1BUF;
//    SSP1BUF=0x00;
//    while(!SSP1STATbits.BF)
//    return SSP1BUF;
    return read;
}

void WRITESPIISR(unsigned char send_data)                                      
{
    PIE1bits.SSP1IE = 1;                                                        // Activa interrupción por SPI y limpia bandera
    PIR1bits.SSP1IF = 0;
    SSP1BUF = send_data;
}

void WRITESPIPOL(unsigned char send_data)
{
    PIE1bits.SSP1IE = 0;                                                        // Desactiva interrupción por SPI
    SSP1BUF = send_data;
    while(!SSP1STATbits.BF);
}

void SPI_ISR()
{
    lect = READSPI();
    PIR1bits.SSPIF = 0;
    PORTD = lect; 
}

#endif