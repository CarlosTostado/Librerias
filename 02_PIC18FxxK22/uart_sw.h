/* 
 * File:   UART_Sw.h
 * Author: Desarrollo
 *
 * Created on 2 de mayo de 2016, 12:08 PM
 */

#ifndef UART_SW_H
#define	UART_SW_H

#include <xc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define INIT_PORTA              0x01
#define INIT_PORTB              0x02
#define INIT_PORTC              0x04

#define PORTNULL                0x00
#define PORT0                   0x01
#define PORT1                   0x02
#define PORT2                   0x04
#define PORT3                   0x08
#define PORT4                   0x10
#define PORT5                   0x20
#define PORT6                   0x40
#define PORT7                   0x80


#define TRISRx1                 TRISBbits.TRISB3
#define PORTRx1                 PORTBbits.RB3

#define TRISTx1                 TRISCbits.TRISC1
#define LATTx1                  LATCbits.LATC1

#define TRISRx2                 TRISDbits.TRISD3
#define PORTRx2                 PORTDbits.RD3

#define TRISTx2                 TRISCbits.TRISC0
#define LATTx2                  LATCbits.LATC0

#define _XTAL_FREQ 32000000

/*
 * DelayTX =  -              -
 *           |    OSC_FREQ    |
 *           | -------------- |
 *           |  4 x Baudrate  |
 *            -              -
 */

void init_usart_sw(void);

void writeByteSW1(uint8_t byte);
void writeByteInvertSW1(uint8_t byte);
void writeStringSW1(unsigned char *buffer);
void writeStringInvertSW1(unsigned char* buffer);

unsigned char ReadSw1(void);

void OpenSw2(void);
void WriteSw2(unsigned char byte2);
void putsSw2(unsigned char* buffer2);
unsigned char ReadSw2(void);

void DelayTX1(void);
void DelayRXHalfBitUART1(void);
void DelayRXBitUART1(void);

void DelayTX2(void);
void DelayRXHalfBitUART2(void);
void DelayRXBitUART2(void);
#endif	/* UART_SW_H */

