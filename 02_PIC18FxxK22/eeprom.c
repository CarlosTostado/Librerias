
#include "eeprom.h"
#include "ee_24LC256.h"
#include "ds1302.h"

static uint32_t HHSB=0,HSB=0,MSB=0,LSB=0,restultado=0; 


////    void     (*eepWrite)(uint16_t,uint8_t);
////    uint8_t  (*eepRead)(uint8_t);
//    
//    void     (*set_3bytes)(uint32_t,uint8_t,uint8_t,uint8_t);
//    uint32_t (*get_3bytes)(uint8_t,uint8_t,uint8_t);
//    
//    void     (*set_2bytes)(uint16_t,uint8_t,uint8_t);
//    uint32_t (*get_2bytes)(uint8_t,uint8_t);
//    
//    void     (*set_1byte)(uint8_t,uint8_t);
//    uint8_t  (*get_1byte)(uint8_t);
////    
////
//////MEMORIA Epp;
////
//void initObject_Epp(void)
//{
////    eepWrite   = EEPROMWrite;
////    eepRead    = EEPROMRead;
//    
//    set_1byte  = setVariable_1Byte_EEPROM;
//    get_1byte  = getVariable_1Byte_EEPROM;
//    
//    set_2bytes  = setVariable_2Bytes_EEPROM;
//    get_2bytes  = getVariable_2Bytes_EEPROM;
//    
//    set_3bytes  = setVariable_3Bytes_EEPROM;
//    get_3bytes  = getVariable_3Bytes_EEPROM;
//    
//
//}

//Funci�n de escritura en EEPROM
void EEPROM_Write(uint8_t add, uint8_t datain)
{
    uint8_t GIEBitValue = INTCONbits.GIE;
    
    EEADR = add;                                                                //Direccion de memoria
    EEDATA = datain;                                                            //Valor a escribir
    EECON1bits.EEPGD = 0;
    EECON1bits.CFGS  = 0;
    EECON1bits.WREN = 1;
    INTCONbits.GIE = 0;
    EECON2 = 0x55;
    EECON2 = 0xAA;
    EECON1bits.WR = 1;                                                          //Se limpia por HW
    while(EECON1bits.WR)
    {
    }
    
    EECON1bits.WREN = 0;
    INTCONbits.GIE = GIEBitValue;   // Restore interrupt enable
}

//Funci�n de lectura en EEPROM
uint8_t EEPROM_Read(uint8_t address)
{
    
    EEADR = address;
    EECON1bits.EEPGD = 0;
    EECON1bits.CFGS  = 0;
    EECON1bits.RD = 1;  
   
    NOP();  // NOPs may be required for latency at high frequencies
    NOP();  //10101111
    return EEDATA;
}


// ******************* FUNCIONES GENERRICAS DE EEPROM EXTERNA

// <editor-fold defaultstate="collapsed" desc="FUNCIONES PARA GUARDAR 3 BYTES">

uint32_t getVariable_3Bytes_EEPROM(uint8_t LOCALIDAD_H, uint8_t LOCALIDAD_M, uint8_t LOCALIDAD_L) {

            HSB = EEPROMRead(LOCALIDAD_H);
            MSB = EEPROMRead(LOCALIDAD_M);
            LSB = EEPROMRead(LOCALIDAD_L);
//        HSB = ejemplo(LOCALIDAD_H);
//        MSB = ejemplo(LOCALIDAD_M);
//        LSB = ejemplo(LOCALIDAD_L);
    restultado = (HSB << 16) | (MSB << 8) | (LSB);

    return restultado;
}

void setVariable_3Bytes_EEPROM(uint32_t valor, uint8_t LOCALIDAD_H, uint8_t LOCALIDAD_M, uint8_t LOCALIDAD_L) {
//
    EEPROMWrite(LOCALIDAD_H, ((valor & 0x00FF0000) >> 16) & 0xFF);
    EEPROMWrite(LOCALIDAD_M, ((valor & 0x0000FF00) >> 8) & 0xFF);
    EEPROMWrite(LOCALIDAD_L, (valor & 0x000000FF) & 0xFF);    
//    (*eepWrite)(LOCALIDAD_H, ((valor & 0x00FF0000) >> 16) & 0xFF);
//    (*eepWrite)(LOCALIDAD_M, ((valor & 0x0000FF00) >> 8) & 0xFF);
//    (*eepWrite)(LOCALIDAD_L, (valor & 0x000000FF) & 0xFF);

}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FUNCIONES PARA GUARDAR 2 BYTES">

uint16_t getVariable_2Bytes_EEPROM(uint8_t LOCALIDAD_H, uint8_t LOCALIDAD_L) {
    return ( (EEPROMRead(LOCALIDAD_H) << 8) | EEPROMRead(LOCALIDAD_L));
    
      
      return 0;
}

void setVariable_2Bytes_EEPROM(uint16_t valor, uint8_t LOCALIDAD_H, uint8_t LOCALIDAD_L) {

    EEPROMWrite(LOCALIDAD_H, (valor & 0xFF00) >> 8);
    EEPROMWrite(LOCALIDAD_L, (valor & 0x00FF));
//    (*eepWrite)(LOCALIDAD_H, (valor & 0xFF00) >> 8);
//    (*eepWrite)(LOCALIDAD_L, (valor & 0x00FF));
//    
    
    
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FUNCIONES PARA GUARDAR 1 BYTE">

uint8_t getVariable_1Byte_EEPROM(uint8_t LOCALIDAD_H) {
    return EEPROMRead(LOCALIDAD_H);
//        return (*eepRead)(LOCALIDAD_H);
}

void setVariable_1Byte_EEPROM(uint8_t valor, uint8_t LOCALIDAD_H) {
    EEPROMWrite(LOCALIDAD_H, valor);
//    (*eepWrite)(LOCALIDAD_H, valor);
    
}
// </editor-fold>



// <editor-fold defaultstate="collapsed" desc="TOTALIZADOR INICIAL(3 BYTES)">

uint32_t getTotalizadorInicial_EEPROM(void) 
{
   
    HHSB = EEPROMRead(LOCALIDAD_TOTALIZADOR_INICIAL_HH);
    HSB  = EEPROMRead(LOCALIDAD_TOTALIZADOR_INICIAL_H);
    MSB  = EEPROMRead(LOCALIDAD_TOTALIZADOR_INICIAL_M);
    LSB  = EEPROMRead(LOCALIDAD_TOTALIZADOR_INICIAL_L);
//    HHSB = (*eepRead)(LOCALIDAD_TOTALIZADOR_INICIAL_HH);
//    HSB  = (*eepRead)(LOCALIDAD_TOTALIZADOR_INICIAL_H);
//    MSB  = (*eepRead)(LOCALIDAD_TOTALIZADOR_INICIAL_M);
//    LSB  = (*eepRead)(LOCALIDAD_TOTALIZADOR_INICIAL_L);
    restultado = (HHSB << 24) | (HSB << 16) | (MSB << 8) | (LSB);
    
    return restultado;
}

void setTotalizadorInicial_EEPROM(uint32_t numeroTecleado)
{
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_INICIAL_HH,((numeroTecleado & 0xFF000000) >> 24) & 0xFF);
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_INICIAL_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_INICIAL_M, ((numeroTecleado & 0x0000FF00) >>  8) & 0xFF);
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_INICIAL_L, (numeroTecleado  & 0x000000FF)        & 0xFF);
//    (*eepWrite)(LOCALIDAD_TOTALIZADOR_INICIAL_HH,((numeroTecleado & 0xFF000000) >> 24) & 0xFF);
//    (*eepWrite)(LOCALIDAD_TOTALIZADOR_INICIAL_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
//    (*eepWrite)(LOCALIDAD_TOTALIZADOR_INICIAL_M, ((numeroTecleado & 0x0000FF00) >>  8) & 0xFF);
//    (*eepWrite)(LOCALIDAD_TOTALIZADOR_INICIAL_L, (numeroTecleado  & 0x000000FF)        & 0xFF);
}// </editor-fold>




// <editor-fold defaultstate="collapsed" desc="CTE. DE CALIBRACION  DE CARBURACION(2 BYTES)">

uint16_t getKCalibracionCarburacion_EEPROM(void) {
    return getVariable_2Bytes_EEPROM(LOCALIDAD_KCALIBRACIONH_CARBURACION,LOCALIDAD_KCALIBRACIONL_CARBURACION);
//    return (*get_2bytes)(LOCALIDAD_KCALIBRACIONH_CARBURACION,LOCALIDAD_KCALIBRACIONL_CARBURACION);
}

void setKCalibracionCarburacion_EEPROM(uint16_t numeroTecleado) {
    setVariable_2Bytes_EEPROM(numeroTecleado,LOCALIDAD_KCALIBRACIONH_CARBURACION,LOCALIDAD_KCALIBRACIONL_CARBURACION);    
//   (*set_2bytes)(numeroTecleado,LOCALIDAD_KCALIBRACIONH_CARBURACION,LOCALIDAD_KCALIBRACIONL_CARBURACION);    
    
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="CTE. DE CALIBRACION ANFORITA(2 BYTES)">

uint16_t getKCalibracionAnforita_EEPROM(void) {
    return getVariable_2Bytes_EEPROM(LOCALIDAD_KCALIBRACIONH_ANFORITA,LOCALIDAD_KCALIBRACIONL_ANFORITA);
//    return ( (EEPROMRead(LOCALIDAD_KCALIBRACIONH) << 8) | EEPROMRead(LOCALIDAD_KCALIBRACIONL));
}

void setKCalibracion_Anforita_EEPROM(uint16_t numeroTecleado) {
    setVariable_2Bytes_EEPROM(numeroTecleado,LOCALIDAD_KCALIBRACIONH_ANFORITA,LOCALIDAD_KCALIBRACIONL_ANFORITA);    
//    EEPROMWrite(LOCALIDAD_KCALIBRACIONH, (numeroTecleado & 0xFF00) >> 8);
//    EEPROMWrite(LOCALIDAD_KCALIBRACIONL, (numeroTecleado & 0x00FF));
}// </editor-fold>



// <editor-fold defaultstate="collapsed" desc="PASSWORD TECNICO(2 BYTES)">

uint16_t getPasswordEEPROM(void) {
    return getVariable_2Bytes_EEPROM(LOCALIDAD_PASSWORDH,LOCALIDAD_PASSWORDL);
//    return ((EEPROMRead(LOCALIDAD_PASSWORDH) << 8) | EEPROMRead(LOCALIDAD_PASSWORDL));
    
}

void setPasswordEEPROM(uint16_t numeroTecleado) {
    setVariable_2Bytes_EEPROM(numeroTecleado,LOCALIDAD_PASSWORDH,LOCALIDAD_PASSWORDL);
//    EEPROMWrite(LOCALIDAD_PASSWORDH, (numeroTecleado & 0xFF00) >> 8);
//    EEPROMWrite(LOCALIDAD_PASSWORDL, (numeroTecleado & 0x00FF));
}// </editor-fold>




// <editor-fold defaultstate="collapsed" desc="PASSWORD SUPERVISOR(2 BYTES)">

uint16_t getPassword_SUPERVISOR_EEPROM(void) {
    return getVariable_2Bytes_EEPROM(LOCALIDAD_PASSWORD2_H,LOCALIDAD_PASSWORD2_L);
//    return ((EEPROMRead(LOCALIDAD_PASSWORD2_H) << 8) | EEPROMRead(LOCALIDAD_PASSWORD2_L));
}

void setPassword_SUPERVISOR_EEPROM(uint16_t numeroTecleado) {
    setVariable_2Bytes_EEPROM(numeroTecleado,LOCALIDAD_PASSWORD2_H,LOCALIDAD_PASSWORD2_L);
//    EEPROMWrite(LOCALIDAD_PASSWORD2_H, (numeroTecleado & 0xFF00) >> 8);
//    EEPROMWrite(LOCALIDAD_PASSWORD2_L, (numeroTecleado & 0x00FF));
}// </editor-fold>




// <editor-fold defaultstate="collapsed" desc="PREFIJOS BLUETOOTH">

void setPrefijos_EEPROM(uint8_t* prefijo)
{

    setVariable_1Byte_EEPROM(*prefijo,   LOCALIDAD_PREFIJO1);
    setVariable_1Byte_EEPROM(*++prefijo, LOCALIDAD_PREFIJO2);
    setVariable_1Byte_EEPROM(*++prefijo, LOCALIDAD_PREFIJO3);
    setVariable_1Byte_EEPROM(*++prefijo, LOCALIDAD_PREFIJO4);
    setVariable_1Byte_EEPROM(*++prefijo, LOCALIDAD_PREFIJO5);
 
}

void getPrefijos_EEPROM(uint8_t* prefijo) 
{
    *prefijo   = getVariable_1Byte_EEPROM(LOCALIDAD_PREFIJO1);
    *++prefijo = getVariable_1Byte_EEPROM(LOCALIDAD_PREFIJO2);
    *++prefijo = getVariable_1Byte_EEPROM(LOCALIDAD_PREFIJO3);
    *++prefijo = getVariable_1Byte_EEPROM(LOCALIDAD_PREFIJO4);
    *++prefijo = getVariable_1Byte_EEPROM(LOCALIDAD_PREFIJO5);
    *++prefijo = 0;
}

// </editor-fold>




// <editor-fold defaultstate="collapsed" desc="NUM DE NODRIZA(1 BYTE)">

uint8_t getNodrizaEEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_NODRIZA);
//    return EEPROMRead(LOCALIDAD_NODRIZA);
}

void setNodrizaEEPROM(uint8_t numeroTecleado) {
    setVariable_1Byte_EEPROM(numeroTecleado,LOCALIDAD_NODRIZA);
//    EEPROMWrite(LOCALIDAD_NODRIZA, numeroTecleado);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="CORTE POR FLUJO(1 BYTE)">

uint8_t getCorteFujoEEPROM(void) 
{
    return getVariable_1Byte_EEPROM(LOCALIDAD_CORTE_FLUJO);
}

void setCorteFlujoEEPROM(uint8_t numeroTecleado) 
{
    setVariable_1Byte_EEPROM(numeroTecleado,LOCALIDAD_CORTE_FLUJO);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="PRECIO SIN ADITIVO(2 BYTES)">

uint16_t getPrecioEEPROM(void) {
    return getVariable_2Bytes_EEPROM(LOCALIDAD_PRECIOH,LOCALIDAD_PRECIOL);
//    return ((EEPROMRead(LOCALIDAD_PRECIOH) << 8) | EEPROMRead(LOCALIDAD_PRECIOL));
}

void setPrecioEEPROM(uint16_t numeroTecleado) {
    setVariable_2Bytes_EEPROM(numeroTecleado,LOCALIDAD_PRECIOH,LOCALIDAD_PRECIOL);
//    EEPROMWrite(LOCALIDAD_PRECIOH, (numeroTecleado & 0xFF00) >> 8);
//    EEPROMWrite(LOCALIDAD_PRECIOL, (numeroTecleado & 0x00FF));
}// </editor-fold>






// <editor-fold defaultstate="collapsed" desc="NUM DE FOLIO(3 BYTES)">

uint32_t getFolioEEPROM(void) {
 
    return getVariable_3Bytes_EEPROM(LOCALIDAD_FOLIO_BYTE_H,LOCALIDAD_FOLIO_BYTE_MED,LOCALIDAD_FOLIO_BYTE_L);
//    HSB = EEPROMRead(LOCALIDAD_FOLIO_BYTE_H);
//    MSB = EEPROMRead(LOCALIDAD_FOLIO_BYTE_MED);
//    LSB = EEPROMRead(LOCALIDAD_FOLIO_BYTE_L);
//    restultado = (HSB << 16) | (MSB << 8) | (LSB);
//    return restultado;
}

void setFolioEEPROM(uint32_t numeroTecleado) {
    
    setVariable_3Bytes_EEPROM(numeroTecleado,LOCALIDAD_FOLIO_BYTE_H,LOCALIDAD_FOLIO_BYTE_MED,LOCALIDAD_FOLIO_BYTE_L);
//    EEPROMWrite(LOCALIDAD_FOLIO_BYTE_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
//    EEPROMWrite(LOCALIDAD_FOLIO_BYTE_MED, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
//    EEPROMWrite(LOCALIDAD_FOLIO_BYTE_L, (numeroTecleado & 0x000000FF) & 0xFF);
    
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="CLAVE DE NODRIZA(3 BYTES)">

uint32_t getClaveEEPROM(void) {

    return getVariable_3Bytes_EEPROM(LOCALIDAD_CLAVE_H, LOCALIDAD_CLAVE_M, LOCALIDAD_CLAVE_L);
    //    HSB = EEPROMRead(LOCALIDAD_FOLIO_BYTE_H);
    //    MSB = EEPROMRead(LOCALIDAD_FOLIO_BYTE_MED);
    //    LSB = EEPROMRead(LOCALIDAD_FOLIO_BYTE_L);
    //    restultado = (HSB << 16) | (MSB << 8) | (LSB);
    //    return restultado;
}

void setClaveEEPROM(uint32_t numeroTecleado) {

    setVariable_3Bytes_EEPROM(numeroTecleado, LOCALIDAD_CLAVE_H, LOCALIDAD_CLAVE_M, LOCALIDAD_CLAVE_L);
    //    EEPROMWrite(LOCALIDAD_FOLIO_BYTE_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
    //    EEPROMWrite(LOCALIDAD_FOLIO_BYTE_MED, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
    //    EEPROMWrite(LOCALIDAD_FOLIO_BYTE_L, (numeroTecleado & 0x000000FF) & 0xFF);

}// </editor-fold>






// <editor-fold defaultstate="collapsed" desc="PORCENTAJE INICIAL SIN AD(1 BYTE)">

void setPorcentajeInicialSinAD_EEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado,LOCALIDAD_PORCENTAJE_INICIAL_SAD);
    

}

uint8_t getPorcentajeInicialSinAD_EEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_PORCENTAJE_INICIAL_SAD);
//    return EEPROMRead(LOCALIDAD_PORCENTAJE_INICIAL_SAD);
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="PORCENTAJE FINAL SIN AD(1 BYTE)">

void setPorcentajeFinalSinAD_EEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado,LOCALIDAD_PORCENTAJE_FINAL_SAD);
//    EEPROMWrite(LOCALIDAD_PORCENTAJE_FINAL_SAD, numeroTeclado);
}

uint8_t getPorcentajeFinalSinAD_EEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_PORCENTAJE_FINAL_SAD);
//    return EEPROMRead(LOCALIDAD_PORCENTAJE_FINAL_SAD);
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="PORCENTAJE INICIAL CON AD(1 BYTE)">

void setPorcentajeInicialConAD_EEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado,LOCALIDAD_PORCENTAJE_INICIAL_CAD);

}

uint8_t getPorcentajeInicialConAD_EEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_PORCENTAJE_INICIAL_CAD);
//    return EEPROMRead(LOCALIDAD_PORCENTAJE_INICIAL_CAD);
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="PORCENTAJE FINAL CON AD(1 BYTE)">

void setPorcentajeFinalConAD_EEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado,LOCALIDAD_PORCENTAJE_FINAL_CAD);
//    EEPROMWrite(LOCALIDAD_PORCENTAJE_FINAL_CAD, numeroTeclado);
}

uint8_t getPorcentajeFinalConAD_EEPROM(void) {
    
    return getVariable_1Byte_EEPROM(LOCALIDAD_PORCENTAJE_FINAL_CAD);
//    return EEPROMRead(LOCALIDAD_PORCENTAJE_FINAL_CAD);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="PORCENTAJE FINAL DE AYER CON AD(1 BYTE)">
//
//void setPorcentajeFinalDeAyerSinnAD_EEPROM(uint8_t numeroTeclado) {
//    setVariable_1Byte_EEPROM(numeroTeclado,LOCALIDAD_PORCENTAJE_FINAL_ANTERIOR_SAD);
////    EEPROMWrite(LOCALIDAD_PORCENTAJE_FINAL_CAD, numeroTeclado);
//}
//
//uint8_t getPorcentajeDeAyerSinAD_EEPROM(void) {
//    
//    return getVariable_1Byte_EEPROM(LOCALIDAD_PORCENTAJE_FINAL_ANTERIOR_SAD);
////    return EEPROMRead(LOCALIDAD_PORCENTAJE_FINAL_CAD);
//}
// </editor-fold>







// <editor-fold defaultstate="collapsed" desc="NUM DE TIMEOUT(1 BYTE)">

void setTimeOutEEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado,LOCALIDAD_TIMEOUT);
//    EEPROMWrite(LOCALIDAD_TIMEOUT, numeroTeclado);
}

uint8_t getTimeOutEEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_TIMEOUT);
//    return EEPROMRead(LOCALIDAD_TIMEOUT);
}// </editor-fold>






// <editor-fold defaultstate="collapsed" desc="PRECIO DE ADITIVO(1 BYTE)">

void setAditivoEEPROM(uint8_t numeroTeclado) {
//    EEPROMWrite(LOCALIDAD_ADITIVO, numeroTeclado);
    setVariable_1Byte_EEPROM(numeroTeclado,LOCALIDAD_ADITIVO);
}

uint8_t getAditivoEEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_ADITIVO);
//    return EEPROMRead(LOCALIDAD_ADITIVO);
}
// </editor-fold>






// <editor-fold defaultstate="collapsed" desc=" CONTADOR TOTALIZADOR SIN AD(3 BYTES)">

uint32_t getTotalizadorSinAD_EEPROM(void) 
{
    
//    return getVariable_3Bytes_EEPROM(LOCALIDAD_TOTALIZADOR_SIN_AD_H,LOCALIDAD_TOTALIZADOR_SIN_AD_M,LOCALIDAD_TOTALIZADOR_SIN_AD_L);
    HHSB = EEPROMRead(LOCALIDAD_TOTALIZADOR_SIN_AD_HH);
    HSB  = EEPROMRead(LOCALIDAD_TOTALIZADOR_SIN_AD_H);
    MSB  = EEPROMRead(LOCALIDAD_TOTALIZADOR_SIN_AD_M);
    LSB  = EEPROMRead(LOCALIDAD_TOTALIZADOR_SIN_AD_L);
    restultado = (HHSB << 24) | (HSB << 16) | (MSB << 8) | (LSB);
    
    return restultado;

}

void setTotalizadorSinAD_EEPROM(uint32_t numeroTecleado) 
{
//    setVariable_3Bytes_EEPROM(numeroTecleado,LOCALIDAD_TOTALIZADOR_SIN_AD_H,LOCALIDAD_TOTALIZADOR_SIN_AD_M,LOCALIDAD_TOTALIZADOR_SIN_AD_L);
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_SIN_AD_HH,((numeroTecleado & 0xFF000000) >> 24) & 0xFF);
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_SIN_AD_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_SIN_AD_M, ((numeroTecleado & 0x0000FF00) >>  8) & 0xFF);
    EEPROMWrite(LOCALIDAD_TOTALIZADOR_SIN_AD_L, (numeroTecleado  & 0x000000FF)        & 0xFF);
    
    
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="CONTADOR TOTALIZADOR CON ADITIVO(3 BYTES)">

uint32_t getTotalizadorConAD_EEPROM(void) 
{
    return getVariable_3Bytes_EEPROM(LOCALIDAD_TOTALIZADOR_CON_AD_H,LOCALIDAD_TOTALIZADOR_CON_AD_M,LOCALIDAD_TOTALIZADOR_CON_AD_L);
}

void setTotalizadorConAD_EEPROM(uint32_t numeroTecleado)
{
    setVariable_3Bytes_EEPROM(numeroTecleado,LOCALIDAD_TOTALIZADOR_CON_AD_H,LOCALIDAD_TOTALIZADOR_CON_AD_M,LOCALIDAD_TOTALIZADOR_CON_AD_L);
}
// </editor-fold>






// <editor-fold defaultstate="collapsed" desc="NUM DE SERVICIO(2 BYTES)">

uint16_t getNumServicioEEPROM(void) {
    return getVariable_2Bytes_EEPROM(LOCALIDAD_NUM_SERVICIO_H,LOCALIDAD_NUM_SERVICIO_L);
//    return ((EEPROMRead(LOCALIDAD_NUM_SERVICIO_H) << 8) | EEPROMRead(LOCALIDAD_NUM_SERVICIO_L));
}

void setNumServicioEEPROM(uint16_t numeroTecleado) {
    
    setVariable_2Bytes_EEPROM(numeroTecleado,LOCALIDAD_NUM_SERVICIO_H,LOCALIDAD_NUM_SERVICIO_L);
//    EEPROMWrite(LOCALIDAD_NUM_SERVICIO_H, (numeroTecleado & 0xFF00) >> 8);
//    EEPROMWrite(LOCALIDAD_NUM_SERVICIO_L, (numeroTecleado & 0x00FF));
}
// </editor-fold>



////uint32_t get_LitrosSurtidos_RAM(void)
////{
////    
////    HSB =   EEPROMRead(LOCALIDAD_LITROS_SURTIENDOSE_H);
////    MSB =   EEPROMRead(LOCALIDAD_LITROS_SURTIENDOSE_M);
////    LSB =   EEPROMRead(LOCALIDAD_LITROS_SURTIENDOSE_L); 
////    restultado =   (HSB<<16) | (MSB<<8) | (LSB);    
////
////    return restultado; 
////}
////void set_LitrosSurtidos_RAM(uint32_t numeroTecleado){ 
////    EEPROMWrite(LOCALIDAD_LITROS_SURTIENDOSE_H,   ( (numeroTecleado   &  0x00FF0000)  >> 16 ) & 0xFF    );      
////    EEPROMWrite(LOCALIDAD_LITROS_SURTIENDOSE_M,   ( (numeroTecleado   &  0x0000FF00)  >>  8 ) & 0xFF    );    
////    EEPROMWrite(LOCALIDAD_LITROS_SURTIENDOSE_L,   (  numeroTecleado   &  0x000000FF)          & 0xFF    );
////}

// <editor-fold defaultstate="collapsed" desc="LITROS DE RECIRCULACION">

uint32_t getRecirculacion_EEPROM(void) 
{
    return getVariable_3Bytes_EEPROM(LOCALIDAD_RECIRCULACION_H, LOCALIDAD_RECIRCULACION_M, LOCALIDAD_RECIRCULACION_L);
}

void setRecirculacion_EEPROM(uint32_t litros) 
{
    setVariable_3Bytes_EEPROM(litros, LOCALIDAD_RECIRCULACION_H, LOCALIDAD_RECIRCULACION_M, LOCALIDAD_RECIRCULACION_L);
}// </editor-fold>



// <editor-fold defaultstate="collapsed" desc="PULSOS NO DESEADOS">

uint32_t get_PulsosNoDeseados_EEPROM(void) {
    //    uint32_t HSB=0,MSB=0,LSB=0,restultado=0;    

    HSB = EEPROMRead(LOCALIDAD_PULSOS_NO_DESEADOS_H);
    MSB = EEPROMRead(LOCALIDAD_PULSOS_NO_DESEADOS_M);
    LSB = EEPROMRead(LOCALIDAD_PULSOS_NO_DESEADOS_L);
    restultado = (HSB << 16) | (MSB << 8) | (LSB);

    return restultado;
}

void set_PulsosNoDeseados_EEPROM(uint32_t numeroTecleado) {
    EEPROMWrite(LOCALIDAD_PULSOS_NO_DESEADOS_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
    EEPROMWrite(LOCALIDAD_PULSOS_NO_DESEADOS_M, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
    EEPROMWrite(LOCALIDAD_PULSOS_NO_DESEADOS_L, (numeroTecleado & 0x000000FF) & 0xFF);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="LITROS ACUMULADOS(3 BYTES)">

uint32_t get_LitrosAcumulados_EEPROM(void) {
   
    return getVariable_3Bytes_EEPROM(LOCALIDAD_LITROS_ACUMULADOS_H,LOCALIDAD_LITROS_ACUMULADOS_M,LOCALIDAD_LITROS_ACUMULADOS_L);
//    HSB = EEPROMRead(LOCALIDAD_LITROS_ACUMULADOS_H);
//    MSB = EEPROMRead(LOCALIDAD_LITROS_ACUMULADOS_M);
//    LSB = EEPROMRead(LOCALIDAD_LITROS_ACUMULADOS_L);
//    restultado = (HSB << 16) | (MSB << 8) | (LSB);
//    return restultado;
}

void set_LitrosAcumulados_EEPROM(uint32_t numeroTecleado) {
    setVariable_3Bytes_EEPROM(numeroTecleado,LOCALIDAD_LITROS_ACUMULADOS_H,LOCALIDAD_LITROS_ACUMULADOS_M,LOCALIDAD_LITROS_ACUMULADOS_L);
//    EEPROMWrite(LOCALIDAD_LITROS_ACUMULADOS_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
//    EEPROMWrite(LOCALIDAD_LITROS_ACUMULADOS_M, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
//    EEPROMWrite(LOCALIDAD_LITROS_ACUMULADOS_L, (numeroTecleado & 0x000000FF) & 0xFF);
} // </editor-fold>


//////
// <editor-fold defaultstate="collapsed" desc="LITROS INTERNOS(3 BYTES)">

uint32_t get_LitrosInternos_EEPROM(void) {
   
    return getVariable_3Bytes_EEPROM(LOCALIDAD_LITROS_INTERNOS_H,LOCALIDAD_LITROS_INTERNOS_M,LOCALIDAD_LITROS_INTERNOS_L);
//    HSB = EEPROMRead(LOCALIDAD_LITROS_ACUMULADOS_H);
//    MSB = EEPROMRead(LOCALIDAD_LITROS_ACUMULADOS_M);
//    LSB = EEPROMRead(LOCALIDAD_LITROS_ACUMULADOS_L);
//    restultado = (HSB << 16) | (MSB << 8) | (LSB);
//    return restultado;
}

void set_LitrosInternos_EEPROM(uint32_t numeroTecleado) {
    setVariable_3Bytes_EEPROM(numeroTecleado,LOCALIDAD_LITROS_INTERNOS_H,LOCALIDAD_LITROS_INTERNOS_M,LOCALIDAD_LITROS_INTERNOS_L);
//    EEPROMWrite(LOCALIDAD_LITROS_ACUMULADOS_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
//    EEPROMWrite(LOCALIDAD_LITROS_ACUMULADOS_M, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
//    EEPROMWrite(LOCALIDAD_LITROS_ACUMULADOS_L, (numeroTecleado & 0x000000FF) & 0xFF);
} // </editor-fold>

////////


   
// <editor-fold defaultstate="collapsed" desc="PESOS CREDITO(3 BYTES)">

uint32_t get_PesosCredito_EEPROM(void) {

 return getVariable_3Bytes_EEPROM(LOCALIDAD_PESOS_CREDITO_H,LOCALIDAD_PESOS_CREDITO_M,LOCALIDAD_PESOS_CREDITO_L);
//    HSB = EEPROMRead(LOCALIDAD_PESOS_ACUMULADOS_H);
//    MSB = EEPROMRead(LOCALIDAD_PESOS_ACUMULADOS_M);
//    LSB = EEPROMRead(LOCALIDAD_PESOS_ACUMULADOS_L);
//    restultado = (HSB << 16) | (MSB << 8) | (LSB);
//    return restultado;
}

void set_PesosCredito_EEPROM(uint32_t numeroTecleado) {
     
     setVariable_3Bytes_EEPROM(numeroTecleado,LOCALIDAD_PESOS_CREDITO_H,LOCALIDAD_PESOS_CREDITO_M,LOCALIDAD_PESOS_CREDITO_L);
//    EEPROMWrite(LOCALIDAD_PESOS_ACUMULADOS_H, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
//    EEPROMWrite(LOCALIDAD_PESOS_ACUMULADOS_M, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
//    EEPROMWrite(LOCALIDAD_PESOS_ACUMULADOS_L, (numeroTecleado & 0x000000FF) & 0xFF);
} // </editor-fold>


// <editor-fold defaultstate="collapsed" desc="PESOS INTERNOS">

void set_PesosInternos_EEPROM(uint32_t numeroTecleado) {
    setVariable_3Bytes_EEPROM(numeroTecleado, LOCALIDAD_PESOS_INTERNOS_H, LOCALIDAD_PESOS_INTERNOS_M, LOCALIDAD_PESOS_INTERNOS_L);
}

uint32_t get_PesosInternos_EEPROM(void) {
    return getVariable_3Bytes_EEPROM(LOCALIDAD_PESOS_INTERNOS_H, LOCALIDAD_PESOS_INTERNOS_M, LOCALIDAD_PESOS_INTERNOS_L);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="PESOS DE CONTADO">

void set_PesosDeContado_EEPROM(uint32_t numeroTecleado) {
    setVariable_3Bytes_EEPROM(numeroTecleado, LOCALIDAD_PESOS_CONTADO_H, LOCALIDAD_PESOS_CONTADO_M, LOCALIDAD_PESOS_CONTADO_L);
}

uint32_t get_PesosDeContado_EEPROM(void) {
    return getVariable_3Bytes_EEPROM(LOCALIDAD_PESOS_CONTADO_H, LOCALIDAD_PESOS_CONTADO_M, LOCALIDAD_PESOS_CONTADO_L);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="FECHA OPRATIVA">

void set_fechaOperativa_EEPROM(uint8_t* fecha)
{
    setVariable_1Byte_EEPROM(*fecha,LOCALIDAD_DIA_OPERATIVO);
    setVariable_1Byte_EEPROM(*++fecha,LOCALIDAD_MES_OPERATIVO);
    setVariable_1Byte_EEPROM(*++fecha,LOCALIDAD_ANIO_OPERATIVO);
}

void get_fechaOperativa_EEPROM(uint8_t* fecha) 
{
    *fecha   = getVariable_1Byte_EEPROM(LOCALIDAD_DIA_OPERATIVO);
    *++fecha = getVariable_1Byte_EEPROM(LOCALIDAD_MES_OPERATIVO);
    *++fecha = getVariable_1Byte_EEPROM(LOCALIDAD_ANIO_OPERATIVO);
    
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="TRATADO 1">

void setTratado1EEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado, LOCALIDAD_TRATADO1);
}

uint8_t getTratado1EEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_TRATADO1);
}// </editor-fold>



// <editor-fold defaultstate="collapsed" desc="TRATADO 2">

void setTratado2EEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado, LOCALIDAD_TRATADO2);
}

    uint8_t getTratado2EEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_TRATADO2);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="TANQUE">

void setTanqueEEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado, LOCALIDAD_TANQUE);
}

uint8_t getTanqueEEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_TANQUE);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="PRECIO COSTO">

void setPrecioCosto_EEPROM(uint32_t numeroTeclado) {
    setVariable_3Bytes_EEPROM(numeroTeclado, LOCALIDAD_COSTO_PRECIO_H, LOCALIDAD_COSTO_PRECIO_M, LOCALIDAD_COSTO_PRECIO_L);
}

uint32_t getPrecioCosto_EEPROM(void) {
    return getVariable_3Bytes_EEPROM(LOCALIDAD_COSTO_PRECIO_H, LOCALIDAD_COSTO_PRECIO_M, LOCALIDAD_COSTO_PRECIO_L);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="DESCUENTO_CONTADO">

void setDescuentoContado_EEPROM(uint32_t numeroTeclado) 
{
setVariable_3Bytes_EEPROM(numeroTeclado,LOCALIDAD_DESCUENTO_CONTADO_H,LOCALIDAD_DESCUENTO_CONTADO_M,LOCALIDAD_DESCUENTO_CONTADO_L);
}

uint32_t getDescuentoContado_EEPROM(void) 
{
return getVariable_3Bytes_EEPROM(LOCALIDAD_DESCUENTO_CONTADO_H,LOCALIDAD_DESCUENTO_CONTADO_M,LOCALIDAD_DESCUENTO_CONTADO_L);
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FINANCIAMIENTO_CONTADO">

void setFinanciamientoContado_EEPROM(uint32_t numeroTeclado) 
{
setVariable_3Bytes_EEPROM(numeroTeclado,LOCALIDAD_FINANCIAMIENTO_CONTADO_H,LOCALIDAD_FINANCIAMIENTO_CONTADO_M,LOCALIDAD_FINANCIAMIENTO_CONTADO_L);
}

uint32_t getFinanciamientoContado_EEPROM(void) 
{
return getVariable_3Bytes_EEPROM(LOCALIDAD_FINANCIAMIENTO_CONTADO_H,LOCALIDAD_FINANCIAMIENTO_CONTADO_M,LOCALIDAD_FINANCIAMIENTO_CONTADO_L);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="FINANCIAMIENTO_CREDITO">

void setFinanciamientoCredito_EEPROM(uint32_t numeroTeclado) 
{
setVariable_3Bytes_EEPROM(numeroTeclado,LOCALIDAD_FINANCIAMIENTO_CREDITO_H,LOCALIDAD_FINANCIAMIENTO_CREDITO_M,LOCALIDAD_FINANCIAMIENTO_CREDITO_L);
}

uint32_t getFinanciamientoCredito_EEPROM(void) 
{
return getVariable_3Bytes_EEPROM(LOCALIDAD_FINANCIAMIENTO_CREDITO_H,LOCALIDAD_FINANCIAMIENTO_CREDITO_M,LOCALIDAD_FINANCIAMIENTO_CREDITO_L);
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="DESCUENTO_CREDITO">

void setDescuentoCredito_EEPROM(uint32_t numeroTeclado) 
{
setVariable_3Bytes_EEPROM(numeroTeclado,LOCALIDAD_DESCUENTO_CREDITO_H,LOCALIDAD_DESCUENTO_CREDITO_M,LOCALIDAD_DESCUENTO_CREDITO_L);
}

uint32_t getDescuentoCredito_EEPROM(void) 
{
return getVariable_3Bytes_EEPROM(LOCALIDAD_DESCUENTO_CREDITO_H,LOCALIDAD_DESCUENTO_CREDITO_M,LOCALIDAD_DESCUENTO_CREDITO_L);
}// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="TOTAL_NETO_CREDITO">

void setCreditoNeto_EEPROM(uint32_t numeroTeclado) 
{
setVariable_3Bytes_EEPROM(numeroTeclado,LOCALIDAD_CREDITO_NETO_H,LOCALIDAD_CREDITO_NETO_M,LOCALIDAD_CREDITO_NETO_L);
}

uint32_t getCreditoNeto_EEPROM(void) 
{
return getVariable_3Bytes_EEPROM(LOCALIDAD_CREDITO_NETO_H,LOCALIDAD_CREDITO_NETO_M,LOCALIDAD_CREDITO_NETO_L);
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="TOTAL_NETO_CONTADO">

void setContadoNeto_EEPROM(uint32_t numeroTeclado) 
{
setVariable_3Bytes_EEPROM(numeroTeclado,LOCALIDAD_CONTADO_NETO_H,LOCALIDAD_CONTADO_NETO_M,LOCALIDAD_CONTADO_NETO_L);
}

uint32_t getContadoNeto_EEPROM(void) 
{
return getVariable_3Bytes_EEPROM(LOCALIDAD_CONTADO_NETO_H,LOCALIDAD_CONTADO_NETO_M,LOCALIDAD_CONTADO_NETO_L);
}// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="TANQUE">

void setSurtidosIncorrectosEEPROM(uint8_t numeroTeclado) {
    setVariable_1Byte_EEPROM(numeroTeclado, LOCALIDAD_SURTIDOS_INCORRECTOS);
}

uint8_t getSurtidosIncorrectosEEPROM(void) {
    return getVariable_1Byte_EEPROM(LOCALIDAD_SURTIDOS_INCORRECTOS);
}// </editor-fold>



// <editor-fold defaultstate="collapsed" desc="POSICION DE MEMORIA(2 BYTES)">

uint16_t getPosicionDeMemoriaEEPROM(void) {
    return getVariable_2Bytes_EEPROM(LOCALIDAD_POSICION_DE_MEMORIA_H,LOCALIDAD_POSICION_DE_MEMORIA_L);
//    return ((EEPROMRead(LOCALIDAD_POSICION_DE_MEMORIA_H) << 8) | EEPROMRead(LOCALIDAD_POSICION_DE_MEMORIA_L));
}

void setPosicionDeMemoriaEEPROM(uint16_t numeroTecleado) {
    
      setVariable_2Bytes_EEPROM(numeroTecleado,LOCALIDAD_POSICION_DE_MEMORIA_H,LOCALIDAD_POSICION_DE_MEMORIA_L);
//    EEPROMWrite(LOCALIDAD_POSICION_DE_MEMORIA_H, (numeroTecleado & 0xFF00) >> 8);
//    EEPROMWrite(LOCALIDAD_POSICION_DE_MEMORIA_L, (numeroTecleado & 0x00FF));
}
// </editor-fold>


//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________
//______________________________________________________________________________________________________________________








//****** GUARDA INFORMACION DE CADA SURTIDO *******


// <editor-fold defaultstate="collapsed" desc="BANDERA 3G">

void set_SurtidoX_Bandera3G_EEPROM(uint16_t DireccionX, uint8_t Alarma) {

    EEPROMWrite(DireccionX, Alarma);
}

uint8_t get_SurtidoX_Bandera3G_EEPROM(uint16_t servicio) {

    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION;
    return EEPROMRead((servicio));
}// </editor-fold>




// <editor-fold defaultstate="collapsed" desc="LITROS SURTIDOS">

uint32_t get_SurtidoX_LitrosSurtidos_EEPROM(uint16_t servicio) {

    // uint32_t HSB=0,MSB=0,LSB=0,restultado=0;    

    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_LITROS_SURTIDOS;


    HSB = EEPROMRead(servicio);
    MSB = EEPROMRead(++servicio);
    LSB = EEPROMRead(++servicio);
    restultado = (HSB << 16) | (MSB << 8) | (LSB);

    return restultado;

}

void set_SurtidoX_LitrosSurtidos_EEPROM(uint16_t DireccionX, uint32_t numeroTecleado) {

    EEPROMWrite(DireccionX, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
    EEPROMWrite(++DireccionX, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
    EEPROMWrite(++DireccionX, (numeroTecleado & 0x000000FF) & 0xFF);


}// </editor-fold>






// <editor-fold defaultstate="collapsed" desc="PESOS SURTIDOS">

uint32_t get_SurtidoX_PesosSurtidos_EEPROM(uint16_t servicio) {

    // uint32_t HSB=0,MSB=0,LSB=0,restultado=0;    

    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_PESOS_SURTIDOS;

    HSB = EEPROMRead(servicio);
    MSB = EEPROMRead(++servicio);
    LSB = EEPROMRead(++servicio);


    restultado = (HSB << 16) | (MSB << 8) | (LSB);

    return restultado;

}

void set_SurtidoX_PesosSurtidos_EEPROM(uint16_t DireccionX, uint32_t numeroTecleado) {

    EEPROMWrite(DireccionX, ((numeroTecleado & 0x00FF0000) >> 16) & 0xFF);
    EEPROMWrite(++DireccionX, ((numeroTecleado & 0x0000FF00) >> 8) & 0xFF);
    EEPROMWrite(++DireccionX, (numeroTecleado & 0x000000FF) & 0xFF);


}// </editor-fold>




// <editor-fold defaultstate="collapsed" desc="HORA INICIAL">

void set_SurtidoX_HoraInicial_EEPROM(uint16_t DireccionX, Calendario* tiempo) {

    EEPROMWrite(DireccionX, tiempo->hour);
    EEPROMWrite(++DireccionX, tiempo->minute);


}

Calendario get_SurtidoX_HoraInicial_EEPROM(uint16_t servicio) {

    Calendario horaInicial;

    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_HORA_INICIAL;

    horaInicial.hour = EEPROMRead(servicio);
    horaInicial.minute = EEPROMRead(++servicio);


    return horaInicial;
}// </editor-fold>






// <editor-fold defaultstate="collapsed" desc="TIEMPO RESTANTE">

uint16_t get_SurtidoX_TiempoRestante_EEPROM(uint16_t servicio) {
    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_TIEMPO_RESTANTE;

    return ((EEPROMRead(servicio) << 8) | EEPROMRead(++servicio));

}

void set_SurtidoX_TiempoRestante_EEPROM(uint16_t DireccionX, uint16_t numeroTecleado) {
    EEPROMWrite(DireccionX, (numeroTecleado & 0xFF00) >> 8);
    EEPROMWrite(++DireccionX, (numeroTecleado & 0x00FF));
}// </editor-fold>





// <editor-fold defaultstate="collapsed" desc="FECHA">

void set_SurtidoX_Dia_EEPROM(uint16_t DireccionX, Calendario* tiempo) {

    EEPROMWrite(DireccionX, tiempo->day);
    EEPROMWrite(++DireccionX, tiempo->month);
}

Calendario get_SurtidoX_Dia_EEPROM(uint16_t servicio) {

    Calendario horaInicial;

    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_DIA_MES;

    horaInicial.day = EEPROMRead(servicio);
    horaInicial.month = EEPROMRead(++servicio);

    return horaInicial;
}// </editor-fold>







// <editor-fold defaultstate="collapsed" desc="ALARMA">

void set_SurtidoX_Alarma_EEPROM(uint16_t Direccion, uint8_t Alarma) {

    EEPROMWrite(Direccion, Alarma);
}

uint8_t get_SurtidoX_Alarma_EEPROM(uint16_t servicio) {
    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_ALARMA;
    return EEPROMRead(servicio);
}// </editor-fold>







// <editor-fold defaultstate="collapsed" desc="TIPO DE PAGO">

void set_SurtidoX_TipoDePago_EEPROM(uint16_t Direccion, uint8_t pago) {

    EEPROMWrite(Direccion, pago);
}

uint8_t get_SurtidoX_tipoDePago_EEPROM(uint16_t servicio) {
    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_TIPO_PAGO;
    return EEPROMRead(servicio);
}// </editor-fold>








// <editor-fold defaultstate="collapsed" desc="NUMERO DE SERVICIO">

uint16_t get_SurtidoX_NumServicio_EEPROM(uint16_t servicio) {
    
    uint16_t valor=0;
    
    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_NUM_SERVICIO;

    valor = ((EEPROMRead(servicio) << 8) | EEPROMRead(++servicio));
    
    return valor;

}

void set_SurtidoX_NumServicio_EEPROM(uint16_t DireccionX, uint16_t numeroTecleado) {
    EEPROMWrite(DireccionX, (numeroTecleado & 0xFF00) >> 8);
    EEPROMWrite(++DireccionX, (numeroTecleado & 0x00FF));
}// </editor-fold>




// <editor-fold defaultstate="collapsed" desc="LINEA DE  CAPTURA">

void get_SurtidoX_LineaDeCaptura_EEPROM(uint16_t servicio, unsigned char*captura) {
    char i = 0;

    servicio = ((servicio - 1) * PAQUETE_SURTIDO) + INIT_POSICION + INIT_LINEA_CAPTURA;

    do {
        //*captura++ = convertBCDToDec(EEPROMRead((servicio + i)));
        *captura++ = EEPROMRead(servicio + i);
        i++;
    } while (i < 11);



}

void set_SurtidoX_LineaDeCaptura_EEPROM(uint16_t DireccionX, unsigned char* numeroTecleado) {
    char i = 0;

    do {
        EEPROMWrite((DireccionX + i), *numeroTecleado++);
        i++;
    } while (i < 11);


    //    
    //    EEPROMWrite(  DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );   
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );   
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    EEPROMWrite(++DireccionX,*numeroTecleado++ );    
    //    

}// </editor-fold>

