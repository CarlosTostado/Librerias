/*
* File:            eusart.c
* Author:          Ing. Carlos Tostado
* Date:            11 de enero 2017
* PIC:             PIC18F4xK22
* 
* Libreria que contiene las funciones para el uso de los puertos eusart
* con los que cuenta el microcontrolador 
* 
*/


//#include <pic18f45k22.h>

//#include <pic18f46k22.h>

#include "eusart.h"
#include "timers.h"

static uint8_t eusart2TxHead = 0;
static uint8_t eusart2TxTail = 0;
static uint8_t eusart2TxBuffer[EUSART2_TX_BUFFER_SIZE];
volatile uint8_t eusart2TxBufferRemaining;

static uint8_t eusart1TxHead = 0;
static uint8_t eusart1TxTail = 0;
static uint8_t eusart1TxBuffer[EUSART1_TX_BUFFER_SIZE];
volatile uint8_t eusart1TxBufferRemaining;

void init_EUSARTx (uint8_t const EUSARTx_SELECT)
{
    
    if (EUSARTx_SELECT & 1)
    {
        //Pines RX y TX como digitales
        ANSELCbits.ANSC6 = 0;  
        ANSELCbits.ANSC7 = 0;
        
        //Pines RX y TX como entradas
        TRISCbits.RC6 = 1;
        TRISCbits.RC7 = 1;
        
       
        
        // 8 bit TX -  TX En - Async - BRGH = 1
        TXSTA1 = 0b00100100;
        
        // Serial En - 8 bit RX - Continuos receive
        RCSTA1 = 0b10010000;
        
        //RX not inverted - TX IDLE High - BRG16 = 1
        BAUDCON1 = 0b00001000;
        
        //BAUDRATE 9600, Error 0.15%
        SPBRGH1 = 0x03;                                                         //832
        SPBRG1  = 0x40;
        
        //Variables para TX por interrupcion
        eusart1TxHead = 0;
        eusart1TxTail = 0;
        eusart1TxBufferRemaining = sizeof(eusart1TxBuffer);        
    }
    else
    {
        //Pines RX y TX como digitales
        ANSELDbits.ANSD6 = 0;  
        ANSELDbits.ANSD7 = 0;
        
        //Pines RX y TX como entradas
        TRISDbits.RD6 = 1;//tx
        TRISDbits.RD7 = 1;//rx   
        
        // 8 bit TX -  TX En - Async - BRGH = 1
        TXSTA2   = 0b00100100;
        
        // Serial En - 8 bit RX - Continuos receive
        RCSTA2   = 0b10010000;
        
        //RX not inverted - TX IDLE High - BRG16 = 1
        BAUDCON2 = 0b00001000;
        
        //BAUDRATE 115200
        SPBRGH2=0;       
        SPBRG2=68;
        
        //Variables para TX por interrupcion
        eusart2TxHead = 0;
        eusart2TxTail = 0;
        eusart2TxBufferRemaining = sizeof(eusart2TxBuffer);
    
    }
}

void SendString(const char *ptrBytes)
{
    while(*ptrBytes)
    {
        EUSART2_WriteByte(*ptrBytes);
        ptrBytes++;
    }
    Wait1ms(30);
    //CheckRX();
}

void USARTx_Disable(uint8_t const EUSARTx_SELECT)
{
    if (EUSARTx_SELECT & 1)
    {
        TXSTA1bits.TXEN = 0;
        RCSTA1bits.CREN = 0;
    }
    else 
    {
   
        TXSTA2bits.TXEN = 0;
        RCSTA2bits.CREN = 0;
    }
}

void USARTx_Enable(uint8_t const EUSARTx_SELECT)
{
    if (EUSARTx_SELECT & 1)
    {
        TXSTA1bits.TXEN = 1;
        RCSTA1bits.CREN = 1;
    }
    else 
    {
        TXSTA2bits.TXEN = 1;
        RCSTA2bits.CREN = 1;
    }
}

//void EUSART_WriteByte(uint8_t txData)
//{
//    while(!TXSTA2bits.TRMT);     
//    TXREG2 = txData;             // Write the data byte to the USART.
//}

uint8_t EUSART_Read1()
{
    if (0 == PIE1bits.RC1IE)
    {
        while(!PIR1bits.RC1IF){}                                                    //Comentar si se maneja por interrupcion
    }
    
    if(1 == RCSTA1bits.OERR)
    {
        // EUSART1 error - restart

        RCSTA1bits.CREN = 0; 
        RCSTA1bits.CREN = 1; 
    }
    return RCREG1;
}

void EUSART1_WriteByte(uint8_t txData)
{
//    USARTx_Enable(EUSART2);
    while(!TXSTA1bits.TRMT);     
    TXREG1 = txData;            
//    EUSART2_WriteByte(txData);
}

void EUSART1_WriteInterrupt(uint8_t txData)
{
    while(0 == eusart1TxBufferRemaining)
    {
    }

    if(0 == PIE1bits.TX1IE)
    {
        TXREG1 = txData;
    }
    else
    {
        PIE1bits.TX1IE = 0;
        eusart1TxBuffer[eusart1TxHead++] = txData;
        if(sizeof(eusart1TxBuffer) <= eusart1TxHead)
        {
            eusart1TxHead = 0;
        }
        eusart1TxBufferRemaining--;
    }
    PIE1bits.TX1IE = 1;
}

uint8_t EUSART_Read2()
{
    while(!PIR3bits.RC2IF){}                                                //Comentar si se maneja por interrupcion
    
    if(1 == RCSTA2bits.OERR)
    {
        // EUSART1 error - restart

        RCSTA2bits.CREN = 0; 
        RCSTA2bits.CREN = 1; 
    }
    return RCREG2;
}

void EUSART2_WriteByte(uint8_t txData)
{
    while(!TXSTA2bits.TRMT);     
    TXREG2 = txData;             // Write the data byte to the USART.
}

void EUSART2_WriteInterrupt(uint8_t txData)
{
    while(0 == eusart2TxBufferRemaining)
    {
    }

    if(0 == PIE3bits.TX2IE)
    {
        TXREG2 = txData;
    }
    else
    {
        PIE3bits.TX2IE = 0;
        eusart2TxBuffer[eusart2TxHead++] = txData;
        if(sizeof(eusart2TxBuffer) <= eusart2TxHead)
        {
            eusart2TxHead = 0;
        }
        eusart2TxBufferRemaining--;
    }
    PIE3bits.TX2IE = 1;
}

void EUSART2_Transmit_ISR(void)
{
    // add your EUSART2 interrupt custom code
    if(sizeof(eusart2TxBuffer) > eusart2TxBufferRemaining)
    {
        TXREG2 = eusart2TxBuffer[eusart2TxTail++];
        if(sizeof(eusart2TxBuffer) <= eusart2TxTail)
        {
            eusart2TxTail = 0;
        }
        eusart2TxBufferRemaining++;
    }
    else
    {
        PIE3bits.TX2IE = 0;
    }
}

void EUSART1_Transmit_ISR(void)
{
    // add your EUSART2 interrupt custom code
    if(sizeof(eusart1TxBuffer) > eusart1TxBufferRemaining)
    {
        TXREG1 = eusart1TxBuffer[eusart1TxTail++];
        if(sizeof(eusart1TxBuffer) <= eusart1TxTail)
        {
            eusart1TxTail = 0;
        }
        eusart1TxBufferRemaining++;
    }
    else
    {
        PIE1bits.TX1IE = 0;
    }
}

void puts1(const unsigned char *buffer) {

    while (*buffer != '\0') 
    {
 
        EUSART1_WriteByte(*buffer++);
        
    }

}


void EUSART2_WriteString(const uint8_t *TXdata)
{
    while(*TXdata)
    {
        //EUSART2_WriteInterpt(*TXdata);
        EUSART2_WriteByte(*TXdata++);
    }
}

